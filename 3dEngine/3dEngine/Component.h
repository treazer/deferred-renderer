#pragma once
#include <string>
using namespace std;

class GameObject;
class Scene;
class Component
{
public:
	Component();
	virtual ~Component();
	virtual void Initialize(GameObject* gameObject) = 0;
	virtual string GetName() = 0;
	virtual GameObject* GetGameObject() = 0;
	virtual void Update(Scene * scene) = 0;
	virtual void Render(Scene * scene) = 0;
protected:
	GameObject* m_gameObject;
	string m_name;
};