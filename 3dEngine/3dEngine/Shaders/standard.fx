#include "globals.fx"
#include "inputs.fx"

////////////////////////////////////////////////////////////////////////////////
// Domain Shader
////////////////////////////////////////////////////////////////////////////////
[domain("tri")]

PixelInputType ColorDomainShader(ConstantOutputType input, float3 uvwCoord : SV_DomainLocation, const OutputPatch<HullOutputType, 3> patch)
{
    float4 vertexPosition;
    PixelInputType output;
 
    // Determine the position of the new vertex.
    vertexPosition = uvwCoord.x * patch[0].position + uvwCoord.y * patch[1].position + uvwCoord.z * patch[2].position;
    vertexPosition.w = 1;
    output.tex = uvwCoord.x * patch[0].tex + uvwCoord.y * patch[1].tex + uvwCoord.z * patch[2].tex;
    output.tex2 = uvwCoord.x * patch[0].tex2 + uvwCoord.y * patch[1].tex2 + uvwCoord.z * patch[2].tex2;
    //output.tex2 = patch[0].tex2;
    output.normal = float4(uvwCoord.x * patch[0].normal + uvwCoord.y * patch[1].normal + uvwCoord.z * patch[2].normal,0);
    output.tangent = uvwCoord.x * patch[0].tangent + uvwCoord.y * patch[1].tangent + uvwCoord.z * patch[2].tangent;
    output.bitangent = uvwCoord.x * patch[0].bitangent + uvwCoord.y * patch[1].bitangent + uvwCoord.z * patch[2].bitangent;
    //displacement
    vertexPosition=vertexPosition + (displacementTex.SampleLevel(SampleType, output.tex,0).r*output.normal*((patch[0].dispStrength+patch[1].dispStrength+patch[2].dispStrength)/3)); //displacementStrength
    //vertexPosition+=float4(patch[0].normal*0.01f,0);
    //vertexPosition = patch[0].position;
    // Calculate the position of the new vertex against the world, view, and projection matrices.
    
    //vertexPosition.x+=output.normal.x;
    //vertexPosition.y+=output.normal.y;
    //vertexPosition.z+=output.normal.z;
    
    output.position = mul(vertexPosition, worldMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);
    if (reconstructionValuesBuffer.x==0)
        output.position -=float4(0,(1/SCREEN_RESOLUTION.y)*2.0f*output.position.w,0,0);
    output.positionNew = output.position;
    
    output.positionOld = mul(vertexPosition, worldMatrix);
    output.positionOld = mul(output.positionOld, viewMatrixOld);
    output.positionOld = mul(output.positionOld, projectionMatrixOld);
    if (reconstructionValuesBuffer.x==0)
        output.positionOld -=float4(0,(1/SCREEN_RESOLUTION.y)*2.0f*output.position.w,0,0);
    //output.position = float4(vertexPosition,1);
    // Send the input color into the pixel shader.
    output.color = patch[0].color;
    //output.wPosition = mul(vertexPosition, worldMatrix);
    //output.wPosition = mul(output.wPosition, viewMatrix); //for verifying reconstructed data
    //output.wPosition = mul(vertexPosition, projectionMatrix);
    output.normal.w = 0;
    output.normal = mul(output.normal, worldMatrix);
    output.tangent = mul(float4(output.tangent,0), worldMatrix).xyz;
    output.bitangent = mul(float4(output.bitangent,0), worldMatrix).xyz;
    //output.normal = float3(output.position.x,output.position.y,output.position.z);
    return output;
    
    
}


////////////////////////////////////////////////////////////////////////////////
// Patch Constant Function
////////////////////////////////////////////////////////////////////////////////
ConstantOutputType ColorPatchConstantFunction(InputPatch<HullInputType, 3> inputPatch, uint patchId : SV_PrimitiveID)
{    
    ConstantOutputType output;

    // Set the tessellation factors for the three edges of the triangle.
    output.edges[0] = inputPatch[0].tessellationStrength;
    output.edges[1] = inputPatch[0].tessellationStrength;
    output.edges[2] = inputPatch[0].tessellationStrength;

    // Set the tessellation factor for tessallating inside the triangle.
    output.inside = inputPatch[0].tessellationStrength;

    return output;
}

////////////////////////////////////////////////////////////////////////////////
// Hull Shader
////////////////////////////////////////////////////////////////////////////////
[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("ColorPatchConstantFunction")]

HullOutputType ColorHullShader(InputPatch<HullInputType, 3> patch, uint pointId : SV_OutputControlPointID, uint patchId : SV_PrimitiveID)
{
    HullOutputType output;


    // Set the position for this control point as the output position.
    output.position = patch[pointId].position;

    // Set the input color as the output color.
    output.color = patch[pointId].color;
    output.normal = patch[pointId].normal;
    output.tangent = patch[pointId].tangent;
    output.bitangent = patch[pointId].bitangent;
    output.tex = patch[pointId].tex;
    output.tex2 = patch[pointId].tex2;
    output.tessellationStrength = patch[pointId].tessellationStrength;
    output.dispStrength = patch[pointId].dispStrength;
    return output;
}




////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
HullInputType VSMain(VertexInputType input)
{
    //PixelInputType output;
    HullInputType output; //switch to this one
    
    // Change the position vector to be 4 units for proper matrix calculations.
    input.position.w = 1.0f;
    output.position = input.position;
    output.color = input.color;
    output.normal = input.normal;
    output.tangent = input.tangent;
    output.bitangent = input.bitangent;
    output.tex = float2(input.tex.x * uvTilingX, input.tex.y * uvTilingY);
    output.tex.y = 1-output.tex.y;
    output.tex2.xy = input.tex.zw;
    output.tex2.y = 1-output.tex2.y;
    
    //tessellation
    float4 pos = mul(input.position, worldMatrix);

    float pos2 = abs(length(eyePosition-pos));
    
    float tessellationValue = tessellationAmount-(pos2*0.1f);// / pos.w;

    if (tessellationValue > tessellationAmount) tessellationValue = tessellationAmount;
    if (tessellationValue < 1) tessellationValue = 1;
    output.tessellationStrength = tessellationAmount;
    output.dispStrength = lerp (displacementStrength*0.8f, displacementStrength, tessellationValue/tessellationAmount);
    return output;
}

//TEST

float4 testShader(float4 ScreenPos)
{
    
    ScreenPos *= float4(1280,720,1,1);
	float oddRow = int(ScreenPos.x) % 2 == 1 ? 1.0f : 0.0f;
	float oddColumn = int(ScreenPos.y) % 2 == 1 ? 1.0f : 0.0f;
	float xDerivative = ddx(oddRow);
	float yDerivative = ddy(oddColumn);
	return float4(xDerivative, yDerivative, 0.0f, 1.0f);
}

//TEST END

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
PS_OUTPUT PSMain(PixelInputType input) : SV_TARGET
{
	PS_OUTPUT output;
    float4 textureColor = float4(1, 0, 0, 1);

    // Sample the pixel color from the texture using the sampler at this texture coordinate location.
    textureColor = albedoTex.Sample(SampleType, input.tex);
    textureColor.xyz *= clamp(lightTex.Sample(SampleType, input.tex2).xyz,float3(0.43f,0.43f,0.43f),float3(1.0f,1.0f,1.0f));
    //drop transparent
    float cutoutValue = cutoutTex.Sample(SampleType, input.tex).r;
    clip(cutoutValue-0.3f);
    
    //NORMAL
    
    // Sample the pixel in the bump map.
    float4 bumpMap = normalTex.Sample(SampleType, input.tex);

    // Calculate strength of bumpMap
    //bumpMap = lerp (float4(0,1,0,0),bumpMap,1);

    // Expand the range of the normal value from (0, +1) to (-1, +1).
    bumpMap = (bumpMap * 2.0f) - 1.0f;

    // Calculate the normal from the data in the bump map.
    float3 bumpNormal = (bumpMap.x * input.tangent) + (bumpMap.y * input.bitangent) + (bumpMap.z * input.normal.xyz);

    //NORMAL END
    
    float3 finalColor;
    float2 finalNormal;

	finalColor = textureColor * baseColor;
    /*
    if (reconstructionValuesBuffer.x==0)
        finalColor = float3(0,0,1);
    else
        finalColor = float3(1,0,0);
    */
    float2 velocity = (input.positionNew.xy / input.positionNew.w) - (input.positionOld.xy / input.positionOld.w);
	float3 decodedNormal = normalize(bumpNormal);
    finalNormal = (float2(atan2(decodedNormal.y,decodedNormal.x)/PI, decodedNormal.z)+1.0)*0.5;
    
    float roughness = roughnessTex.Sample(SampleType, input.tex).r;
    float metalness = metalTex.Sample(SampleType, input.tex).r * mulMetal;
    if (metalness > 0.2f) roughness *=-1;
    output.GBuffer1 = float4(finalColor,roughness);
    output.GBuffer2 = float2(finalNormal);
    output.GBuffer3 = float2(velocity);
        
    
    return output;
}
#include "shadows.fx"