#pragma once
#include "Shader.h"
class StandardShader :
	public Shader
{
public:
	StandardShader(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName, bool usesTessellation);
	virtual ~StandardShader(void);

	void Begin(ID3D11DeviceContext* deviceContext);
	void BeginShadow(ID3D11DeviceContext* deviceContext);
	void BeginTranslucency(ID3D11DeviceContext* deviceContext);
	void End(ID3D11DeviceContext* deviceContext);

	bool SetShaderParameters(ID3D11DeviceContext* deviceContext, 
		ID3D11ShaderResourceView* albedo, 
		ID3D11ShaderResourceView* normal,
		ID3D11ShaderResourceView* displacement,
		ID3D11ShaderResourceView* roughness,
		ID3D11ShaderResourceView* metal,
		ID3D11ShaderResourceView* translucency,
		ID3D11ShaderResourceView* cutout,
		ID3D11ShaderResourceView* lightmap,
		float emission,
		int materialId);
	using Shader::SetShaderParameters;
	//bool SetShaderParameters(ID3D11DeviceContext* deviceContext, ID3D11ShaderResourceView* texture, int slot) {};
	//bool SetShaderParameters(ID3D11DeviceContext* deviceContext, XMFLOAT4X4 worldMatrix, XMFLOAT4X4 viewMatrix, XMFLOAT4X4 projectionMatrix, float tessellationAmount = 0) {};
protected:
	bool Initialize(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName, bool usesTessellation);
	bool InitializeSamplerState(ID3D11Device* device);

private:
	ID3D11SamplerState* m_samplerState;
	ID3D11SamplerState* m_samplerStateClamp;
};

