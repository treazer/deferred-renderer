#include "DeferredShader.h"
#include <iostream>

DeferredShader::DeferredShader(ID3D11Device * device, HWND hwnd, WCHAR * shaderFileName) :
	Shader(device, hwnd, shaderFileName, false)
{
	m_samplerState = 0;
	m_initialized = InitializeSamplerState(device);
}

DeferredShader::~DeferredShader()
{
	if (m_samplerState)
	{
		m_samplerState->Release();
		m_samplerState = NULL;
	}
}

void DeferredShader::Begin(ID3D11DeviceContext * deviceContext)
{
	deviceContext->PSSetSamplers(0, 1, &m_samplerState);
	deviceContext->PSSetSamplers(1, 1, &m_shadowSamplerState);
	Shader::Begin(deviceContext);
}

void DeferredShader::End(ID3D11DeviceContext * deviceContext)
{
	deviceContext->PSSetSamplers(0, 0, NULL);
	Shader::End(deviceContext);
}

bool DeferredShader::Initialize(ID3D11Device * device, HWND hwnd, WCHAR * shaderFileName)
{
	if (!Shader::Initialize(device, hwnd, shaderFileName, false))
	{
		return false;
	}

	return true;
}

bool DeferredShader::InitializeSamplerState(ID3D11Device * device)
{
	D3D11_SAMPLER_DESC samplerDesc;
	HRESULT result;

	// Create texture sampler desc
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create sampler state
	result = device->CreateSamplerState(&samplerDesc, &m_samplerState);
	if (FAILED(result))
	{
		return false;
	}

	// Create shadow sampler desc
	samplerDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_LESS_EQUAL;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create sampler state
	result = device->CreateSamplerState(&samplerDesc, &m_shadowSamplerState);
	if (FAILED(result))
	{
		return false;
	}

	D3D11_BUFFER_DESC lightBufferDesc;
	// Setup the buffer description
	lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc.ByteWidth = sizeof(DeferredBufferType);
	lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc.MiscFlags = 0;
	lightBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer
	result = device->CreateBuffer(&lightBufferDesc, NULL, &m_lightBuffer);
	if (FAILED(result))
	{
		std::cout << "!! failed to create deferred buffer desc" << std::endl;
		return false;
	}

	return true;
}

/*

	Fills the light-computations shaders with the necessary information
	The light-parameters refer to the directional sun light

*/

bool DeferredShader::SetShaderParameters(ID3D11DeviceContext* deviceContext,
											XMFLOAT4 lightDirection,
											XMFLOAT4 lightPosition,
											XMFLOAT4 color,
											float intensity,
											XMFLOAT4 eye,
											XMFLOAT4X4 lightView,
											XMFLOAT4X4 lightOrtho,
											XMFLOAT4X4 worldMatrix,
											XMFLOAT4X4 viewMatrix,
											XMFLOAT4X4 projectionMatrix,
											XMFLOAT4X4 projectionMatrixScene,
											XMFLOAT4X4 viewMatrixScene,
											int currentFrame,
											int frameSteps)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	DeferredBufferType* dataPtr;

	unsigned int bufferNumber;
	
	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_lightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(result))
	{
		return false;
	}
	// Get a pointer to the data in the constant buffer.
	dataPtr = (DeferredBufferType*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	dataPtr->color = color;
	dataPtr->direction = lightDirection;
	dataPtr->position = lightPosition;
	dataPtr->eye = eye;
	dataPtr->lightViewMatrix = lightView;
	dataPtr->lightOrthoMatrix = lightOrtho;
	dataPtr->worldMatrixDeferred = XMMatrixTranspose(XMLoadFloat4x4(&worldMatrix));
	dataPtr->viewMatrixDeferred = XMMatrixTranspose(XMLoadFloat4x4(&viewMatrix));
	dataPtr->projectionMatrixDeferred = XMMatrixTranspose(XMLoadFloat4x4(&projectionMatrix));
	dataPtr->inverseProjection = XMMatrixInverse(NULL, XMMatrixTranspose(XMLoadFloat4x4(&projectionMatrixScene)));
	dataPtr->inverseView = XMMatrixInverse(NULL, XMMatrixTranspose(XMLoadFloat4x4(&viewMatrixScene)));
	dataPtr->reconstructionValues = XMFLOAT4(currentFrame, frameSteps, 0, 0);
	// Unlock the constant buffer.
	deviceContext->Unmap(m_lightBuffer, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 1;

	deviceContext->PSSetConstantBuffers(bufferNumber, 1, &m_lightBuffer);
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_lightBuffer);

	return true;
}

