#pragma once

//#include <d3d11_1.h>
#include <d3d11.h>
#include <directxmath.h>
#include <directxcolors.h>
#include <vector>

using namespace DirectX;

class DXManager
{
public:
	DXManager(void);
	~DXManager(void);

	bool Initialize(int screenWidth, int screenHeight, bool vsync, HWND hwnd, bool fullscreen);
	void BeginScene(float r, float g, float b, float a);
	void EndScene();

	void EnableAlphaBlending(bool enable);
	void EnableZBuffer(bool enable);
	void SetBackgroundZBuffer();
	void SetRasterStateWireframe(bool enable);
	void SetRasterStateBackside(bool enable);
	void SetRasterStateSkybox(bool enable);
	void TranslucencyState(bool enable);
	void InitializeViewport(int screenWidth, int screenHeight);

	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetDeviceContext();
	ID3D11DepthStencilView* GetDepthStencilView();

private:
	bool InitializeSwapChain(HWND hwnd, bool fullscreen, int screenWidth, int screenHeight, unsigned  int numerator, unsigned int denominator);
	bool InitializeDepthBuffer(int screendWidth, int screenHeight);
	bool InitializeDepthStencilBuffer();
	bool InitializeStencilView();
	bool InitializeRasterizerState();
	bool InitializeAlphaBlending();
	bool InitializeZBuffer();

	bool m_vsync_enabled;
	int m_videoCardMemory;
	char m_videoCardDescription[128];
	IDXGISwapChain* m_swapChain;
	ID3D11Device* m_device;
	ID3D11DeviceContext* m_deviceContext;
	ID3D11RenderTargetView* m_renderTargetView;
	ID3D11Texture2D* m_depthStencilBuffer;
	ID3D11DepthStencilState* m_depthStencilState;
	ID3D11DepthStencilView* m_depthStencilView;
	ID3D11RasterizerState* m_rasterState;
	ID3D11RasterizerState* m_rasterStateWireframe;
	ID3D11RasterizerState* m_rasterStateBackside;
	ID3D11RasterizerState* m_rasterStateSkybox;
	ID3D11BlendState* m_alphaEnableBlendingState;
	ID3D11BlendState* m_alphaDisableBlendingState;
	ID3D11DepthStencilState* m_depthDisabledStencilState;
	ID3D11DepthStencilState* m_depthStencilStateBackground;
	ID3D11DepthStencilState* m_depthStencilStateTranslucency;
};
