#include "GameObject.h"
#include "Scene.h"
#include <iostream>

GameObject::GameObject()
{
	m_components.reserve(RESERVED_COMPONENTS);
	m_transform = 0;
	m_name = 0;
}

GameObject::~GameObject()
{
	if (m_name)
	{
		delete[] m_name;
		m_name = NULL;
	}
}

void GameObject::Initialize(char * name)
{
	m_name = name;
	m_transform = new Transform();
	m_transform->Initialize(this);
}

comp_list GameObject::GetComponents()
{
	return m_components;
}

void GameObject::AddComponent(Component * comp)
{
	m_components.push_back(comp);
}

Transform * GameObject::GetTransform()
{
	return m_transform;
}

/*
	Allows to change the order of renderers
*/
Renderer * GameObject::GetRenderer(int id)
{
	if ((unsigned int)id < m_renderer.size())
		return m_renderer[id];
	return nullptr;
}

void GameObject::SetRenderer(Renderer * renderer, int id)
{
	if (id<m_renderer.size())
		m_renderer[id] = renderer;
}

int GameObject::AddRenderer(Renderer * renderer)
{
	m_renderer.push_back(renderer);
	return m_renderer.size() - 1;
}

int GameObject::GetNumRenderer()
{
	return m_renderer.size();
}

char * GameObject::GetName()
{
	return m_name;
}

void GameObject::Update(Scene * scene)
{
	for (auto i = m_components.begin(); i != m_components.end(); ++i) {

		Component * comp = (Component*)*i;
		comp->Update(scene);
	}
	m_transform->Update(scene);
}