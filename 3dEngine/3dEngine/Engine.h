#pragma once

#include "Graphics.h"
#include "Camera.h"
#include "Input.h"
#include "AssetManager.h"
#include "TextureShader.h"
#include "ComputeShader.h"
#include "Scene.h"
#include "Mesh.h"
#include "RenderTexture.h"
#include "DeferredShader.h"
#include "DepthTexture.h"
#include <SpriteFont.h>

#define NUM_ELEMENTS 1024
#define RESERVED_SCENES 8

typedef std::vector<Scene*> scene_list;

class Engine
{
public:

	struct RenderPassData
	{
		XMFLOAT4X4 viewMatrix;
		XMFLOAT4X4 orthoMatrix;
		XMFLOAT4X4 worldMatrix;
		XMFLOAT3 viewTarget;
		XMFLOAT3 viewUp;
		float aspectRatio;
	};

	~Engine(void);

	bool InitializeGraphics(HWND hwnd);
	bool Initialize(HINSTANCE hinstance, HWND hwnd);
	void Run();

	void Release();

	Graphics* GetGraphics();
	Input* GetInput();
	Scene* GetCurrentScene();
	scene_list GetScenes();

	static Engine* GetEngine();

	const int RENDERER_FRAME_STEPS = 2;

private:
	Engine(void);

	void Update();
	void Render();
	void RenderShadowmap();
	void RenderDeferredPass();
	void RenderTranslucency();
	void RenderScene();
	void PostProcessing();
	void PostProcessingPass(int pass, int passNr);

	Graphics* m_graphics;
	Input* m_input;
	AssetManager* m_resourceManager;

	static Engine* m_instance;

	scene_list m_sceneList;
	Scene* m_currentScene;

	DeferredShader* m_deferredShader;
	DeferredShader* m_postProcessShader;
	Mesh* m_deferredPlane;
	RenderTexture* m_renderTargets;
	RenderTexture* m_renderTargets_half;
	DepthTexture * m_depthTexture;
	DepthTexture * m_depthTexture_2;
	DepthTexture * m_deferredDepthTexture;
	DepthTexture * m_shadowMap;
	RenderTexture* m_shadowColor;
	XMFLOAT4X4 m_lightViewMatrix;
	XMFLOAT4X4 m_lightOrthoMatrix;
	RenderTexture* m_postProcessTargets;
	RenderTexture* m_translucencyTarget;
	RenderTexture* m_deferredTarget;
	RenderTexture* m_deferredShadowTarget;
	RenderTexture* m_deferredLuminanceTarget;
	RenderPassData m_deferredRenderPassData;

	ID3D11RenderTargetView * targetViewsEmpty[3];
	ID3D11RenderTargetView * targetViewsDeferred[3];
	ID3D11RenderTargetView * targetViewsScene[3];
	ID3D11RenderTargetView * targetViewsScene2[3];
	ID3D11RenderTargetView * targetViewsShadow;
	ID3D11RenderTargetView * targetViewsTranslucency;

	int m_currentFrameStep;
};
