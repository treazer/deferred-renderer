#include "Scene.h"
#include "Renderer.h"
#include <iostream>
#include "TextureShader.h"
#include "StandardShader.h"
#include "Camera.h"
#include "SystemDefs.h"
#include "Mesh.h"
#include "AssetManager.h"
#include "Texture.h"
#include "Light.h"
#include "DDSTextureLoader.h"
#include "Foliage.h"
#include "TerrainShader.h"
#include "ComputeShader.h"
#include "ObjectPreview.h"
Scene::Scene()
{
	m_SceneName = 0;
	m_objects.reserve(128);
	m_SceneName = 0;
	m_activeCamera = 0;
	m_sun = 0;
	m_skybox = 0;
	m_wind = 0;
}


Scene::~Scene()
{
}

void Scene::Initialize(char* sceneName, Graphics* graphics)
{
	m_SceneName = sceneName;
	m_objects.reserve(RESERVED_OBJECTS);
	m_graphics = graphics;
	AssetManager * resourceManager = AssetManager::GetInstance();

	// Environment map
	HRESULT status;
	status = DirectX::CreateDDSTextureFromFile(graphics->GetDevice(), graphics->GetDeviceContext(), L"Textures/cubemap.dds", NULL, &m_environmentMap);
	status = DirectX::CreateDDSTextureFromFile(graphics->GetDevice(), graphics->GetDeviceContext(), L"Textures/irradiance.dds", NULL, &m_irradianceMap);
	if (FAILED(status))
		MessageBox(graphics->GetHwnd(), L"failed to load environment map #1", L"Error", MB_OK);

	if (m_environmentMap == NULL)
		MessageBox(graphics->GetHwnd(), L"failed to load environment map #2", L"Error", MB_OK);
	
	GameObject* cam = new GameObject();
	cam->Initialize("Camera");
	m_activeCamera = new Camera();
	m_activeCamera->InitializeProjectionMatrix(1.09956f, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, SCREEN_NEAR, SCREEN_DEPTH);
	m_activeCamera->InitializeOthoMatrix(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_NEAR, SCREEN_DEPTH);
	m_activeCamera->SetPosition(0.0f, 0.0f, 0.0f);
	cam->AddComponent(m_activeCamera);
	m_activeCamera->Initialize(cam);
	Add(cam);

	GameObject* sun = new GameObject();
	sun->Initialize("Sun");
	Light* light = new Light();
	sun->AddComponent(light);
	light->InitializeLight(sun, 1.0f, 2.0f, 2.0f, 2.0f, 1.95f);
	sun->GetTransform()->Initialize(sun);
	sun->GetTransform()->SetPosition(1, 1, 1);
	sun->GetTransform()->RotateX(-0.5f);
	sun->GetTransform()->RotateY(0.9f);

	m_sun = sun;
	Add(sun);

	GameObject* sky = new GameObject();
	sky->Initialize("Sky");
	sky->GetTransform()->SetPosition(0, 0, 0);
	sky->AddRenderer(new Renderer());
	sky->GetRenderer()->Initialize(sky);
	sky->GetRenderer()->SetShader((StandardShader*)resourceManager->GetShaderByName("skybox.fx"));
	sky->GetRenderer()->SetMesh(Mesh::Load(graphics->GetDevice(), "Meshes/skybox.obj"));
	sky->GetRenderer()->GetShader()->SetTessellation(false);
	m_skybox = sky;

	m_wind = new Wind();
}

void Scene::Add(GameObject * object)
{
	m_objects.push_back(object);
}

void Scene::Remove(GameObject * object)
{
	
}

void Scene::Update(Input* input)
{
	m_input = input;
	if (m_input->IsKeyDown(DIK_SPACE))
	{
		m_activeCamera->Update(input);
	}
	// Updates all gameobjects according to their components
	for (auto i = m_objects.begin(); i != m_objects.end(); ++i) {
		GameObject * object = (GameObject*)*i;
		object->Update(this);
	}
}

void Scene::Render(Graphics* graphics, int currentFrame, int frameSteps)
{
	XMFLOAT4X4 viewMatrix = m_activeCamera->GetViewMatrix();
	XMFLOAT4X4 projMatrix = m_activeCamera->GetProjectionMatrix();
	AssetManager * resourceManager = AssetManager::GetInstance();
	GameObject * object;
	graphics->EnableZBuffer(true);
	graphics->EnableAlphaBlending(false);
	// Render all gameobjects
	for (auto i = m_objects.begin(); i != m_objects.end(); ++i) {

		object = (GameObject*)*i;
		if (object->GetRenderer() != NULL)
		{
			for (auto u = 0; u < object->GetComponents().size(); u++)
			{
				object->GetComponents()[u]->Render(this);
			}

			object->GetRenderer()->GetShader()->SetShaderParameters(graphics->GetDeviceContext(), object->GetTransform()->GetMatrix(), viewMatrix, projMatrix, m_activeCamera->GetEyeVector(), m_wind->SampleWindForPosition(object->GetTransform()->GetPosition()), m_activeCamera->GetViewMatrixOld(), m_activeCamera->GetProjectionMatrixOld(), currentFrame, frameSteps);
			object->GetRenderer()->GetShader()->SetShaderParameters(graphics->GetDeviceContext(), object->GetRenderer(0)->GetMesh());

			for (int j = 0; j < object->GetNumRenderer(); j++)
			{
				object->GetRenderer(j)->Render(graphics, viewMatrix, projMatrix, m_activeCamera->GetEyeVector());
			}
		}
	}
	
}

void Scene::RenderTranslucency(Graphics* graphics)
{
	XMFLOAT4X4 viewMatrix = m_activeCamera->GetViewMatrix();
	XMFLOAT4X4 projMatrix = m_activeCamera->GetProjectionMatrix();
	GameObject * object;
	// Render all gameobjects
	graphics->EnableAlphaBlending(false);
	for (auto i = m_objects.begin(); i != m_objects.end(); ++i) {

		object = (GameObject*)*i;
		if (object->GetRenderer() != NULL)
		{
			for (auto u = 0; u < object->GetComponents().size(); u++)
			{
				object->GetComponents()[u]->Render(this);
			}
			object->GetRenderer()->GetShader()->SetShaderParameters(graphics->GetDeviceContext(), object->GetTransform()->GetMatrix(), viewMatrix, projMatrix, m_activeCamera->GetEyeVector(), m_wind->SampleWindForPosition(object->GetTransform()->GetPosition()), m_activeCamera->GetViewMatrixOld(), m_activeCamera->GetProjectionMatrixOld(),0,0);
			object->GetRenderer()->GetShader()->SetShaderParameters(graphics->GetDeviceContext(), object->GetRenderer(0)->GetMesh());
			for (int j = 0; j < object->GetNumRenderer(); j++)
			{
				object->GetRenderer(j)->Render(graphics, viewMatrix, projMatrix, m_activeCamera->GetEyeVector(),false,true);
			}
		}
	}

}

void Scene::RenderShadows(Graphics* graphics, XMFLOAT4X4 viewMatrix, XMFLOAT4X4 projMatrix)
{
	// Render all gameobjects
	graphics->EnableAlphaBlending(false);
	for (auto i = m_objects.begin(); i != m_objects.end(); ++i) {
		GameObject * object = (GameObject*)*i;
		if (object->GetRenderer() != NULL)
		{
			object->GetRenderer()->GetShader()->SetShaderParameters(graphics->GetDeviceContext(), object->GetTransform()->GetMatrix(), viewMatrix, projMatrix, m_activeCamera->GetEyeVector(), m_wind->SampleWindForPosition(object->GetTransform()->GetPosition()), m_activeCamera->GetViewMatrixOld(), m_activeCamera->GetProjectionMatrixOld(),0,0);
			object->GetRenderer()->GetShader()->SetShaderParameters(graphics->GetDeviceContext(), object->GetRenderer(0)->GetMesh());
			for (int j = 0; j < object->GetNumRenderer(); j++)
			{
				object->GetRenderer(j)->Render(graphics, viewMatrix, projMatrix, m_activeCamera->GetEyeVector(),true);
			}
		}
	}

}

void Scene::RenderSky(Graphics* graphics, int currentFrame, int frameSteps)
{
	graphics->EnableAlphaBlending(false);
	if (m_skybox->GetRenderer() != NULL)
	{
		//graphics->EnableZBuffer(false);
		graphics->EnableZBuffer(true);
		//graphics->SetBackgroundZBuffer();
		graphics->SetRasterStateSkybox(true);
		XMFLOAT4X4 viewMatrix = m_activeCamera->GetViewMatrix();
		XMFLOAT4X4 projMatrix = m_activeCamera->GetProjectionMatrix();

		viewMatrix._41 = 0;
		viewMatrix._42 = 0;
		viewMatrix._43 = 0;
		m_skybox->GetRenderer()->SetShader((StandardShader*)AssetManager::GetInstance()->GetShaderByName("skybox.fx"));
		m_skybox->GetRenderer()->GetShader()->SetShaderParameters(graphics->GetDeviceContext(), m_skybox->GetTransform()->GetMatrix(), viewMatrix, projMatrix, m_activeCamera->GetEyeVector(), m_wind->SampleWindForPosition(m_skybox->GetTransform()->GetPosition()), m_activeCamera->GetViewMatrixOld(), m_activeCamera->GetProjectionMatrixOld(), currentFrame, frameSteps);
		m_skybox->GetRenderer()->GetShader()->SetShaderParameters(graphics->GetDeviceContext(), m_environmentMap, 0);
		ID3D11ShaderResourceView * tex = AssetManager::GetInstance()->GetTextureByName("noise")->GetTexture();
		graphics->GetDeviceContext()->PSSetShaderResources(1, 1, &tex);
		m_skybox->GetRenderer()->GetShader()->Begin(graphics->GetDeviceContext());
		m_skybox->GetRenderer()->GetMesh()->Render(graphics->GetDeviceContext(), m_skybox->GetRenderer()->GetShader());
	}
	graphics->SetRasterStateSkybox(false);
}

void Scene::SetActiveCamera(Camera * camera)
{
	m_activeCamera = camera;
}

Camera * Scene::GetActiveCamera()
{
	return m_activeCamera;
}

GameObject * Scene::GetSkybox()
{
	return m_skybox;
}

ID3D11ShaderResourceView * Scene::GetEnvironmentMap()
{
	return m_environmentMap;
}

ID3D11ShaderResourceView * Scene::GetIrradianceMap()
{
	return m_irradianceMap;
}

Input * Scene::GetInput()
{
	return m_input;
}

obj_list Scene::GetObjects()
{
	return m_objects;
}

char * Scene::GetSceneName()
{
	return m_SceneName;
}

GameObject * Scene::GetSun()
{
	return m_sun;
}

Graphics * Scene::GetGraphics()
{
	return m_graphics;
}

Wind * Scene::GetWind()
{
	return m_wind;
}

void Scene::CreateTestSphere(XMFLOAT4 position)
{
	AssetManager * resourceManager = AssetManager::GetInstance();
	GameObject * sphere = new GameObject();
	char * objectName = "helperSphere";
	objectName += m_objects.size();
	sphere->Initialize(objectName);
	sphere->GetTransform()->SetPosition(position.x, position.z, position.y);
	sphere->AddRenderer(new Renderer());
	sphere->GetRenderer()->Initialize(sphere);
	sphere->GetRenderer()->SetUVTiling(1, 1);
	sphere->GetRenderer()->SetShader((StandardShader*)resourceManager->GetShaderByName("standard.fx"));
	sphere->GetRenderer()->SetAlbedo(AssetManager::GetInstance()->GetTextureByName((char*)"Soil_C"), XMFLOAT4(0.8f, 0.7f, 0.8f, 1.0f));
	sphere->GetRenderer()->SetNormal(AssetManager::GetInstance()->GetTextureByName((char*)"Soil_N"));
	sphere->GetRenderer()->SetDisplacement(AssetManager::GetInstance()->GetTextureByName((char*)"Soil_T"));
	sphere->GetRenderer()->SetCutout(AssetManager::GetInstance()->GetTextureByName((char*)"white"));
	sphere->GetRenderer()->SetMetal(AssetManager::GetInstance()->GetTextureByName((char*)"Soil_M"), 1.0f);
	sphere->GetRenderer()->SetTranslucency(AssetManager::GetInstance()->GetTextureByName((char*)"dark"));
	sphere->GetRenderer()->SetRoughness(AssetManager::GetInstance()->GetTextureByName((char*)"white"), 0.5f);
	sphere->GetRenderer()->SetMesh(Mesh::Load(m_graphics->GetDevice(), "Meshes/testSphere.dae"));
	sphere->GetRenderer()->GetShader()->SetTessellation(true);
	sphere->GetRenderer()->SetTessellation(1);
	sphere->GetRenderer()->SetDisplacementStrength(0.015f);
	sphere->GetRenderer()->DefineMetal(true);
	Add(sphere);
}

void Scene::CreateGameObject(std::string name, std::string materialName, std::string leafMaterialName, bool isTree, bool foliageOnly, bool hasLightmap)
{
	AssetManager * resourceManager = AssetManager::GetInstance();
	GameObject * newObject = new GameObject();
	char * objectName = &name[0u];
	newObject->Initialize(objectName);

	std::string fileLocation = "Scene/";
	fileLocation.append(objectName);
	
	newObject->GetTransform()->SetPosition(0,0,0);
	newObject->AddRenderer(new Renderer());
	newObject->GetRenderer()->Initialize(newObject);
	newObject->GetRenderer()->SetUVTiling(1, 1);
	if (isTree && !foliageOnly)
		newObject->GetRenderer()->SetShader((StandardShader*)resourceManager->GetShaderByName("tree.fx"));
	else
		newObject->GetRenderer()->SetShader((StandardShader*)resourceManager->GetShaderByName("standard.fx"));

	size_t newsize = strlen(("Scene/" + materialName + "_C.png").c_str()) + 1;
	wchar_t * wcstring = new wchar_t[newsize];
	size_t convertedChars = 0;
	mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/" + materialName + "_C.png").c_str(), _TRUNCATE);
	resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, true);

	mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/" + materialName + "_M.png").c_str(), _TRUNCATE);
	resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, true);

	mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/" + materialName + "_N.png").c_str(), _TRUNCATE);
	resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, false);

	mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/" + materialName + "_R.png").c_str(), _TRUNCATE);
	resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, true);
	if (hasLightmap)
	{
		mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/" + materialName + "_L.png").c_str(), _TRUNCATE);
		resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, true);
	}
	if (isTree)
	{
		mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/" + materialName + "_A.png").c_str(), _TRUNCATE);
		resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, true);
	}
	newObject->GetRenderer()->SetAlbedo(AssetManager::GetInstance()->GetTextureByName((char*)(materialName + "_C").c_str()), XMFLOAT4(1.0, 1.0, 1.0, 1));
	newObject->GetRenderer()->SetNormal(AssetManager::GetInstance()->GetTextureByName((char*)(materialName + "_N").c_str()));
	newObject->GetRenderer()->SetDisplacement(AssetManager::GetInstance()->GetTextureByName((char*)"dark"));
	
	if (isTree) newObject->GetRenderer()->SetCutout(AssetManager::GetInstance()->GetTextureByName((char*)(materialName + "_A").c_str()));
	else		newObject->GetRenderer()->SetCutout(AssetManager::GetInstance()->GetTextureByName((char*)"white"));

	newObject->GetRenderer()->SetMetal(AssetManager::GetInstance()->GetTextureByName((char*)(materialName + "_M").c_str()), 1.0f);
	newObject->GetRenderer()->SetTranslucency(AssetManager::GetInstance()->GetTextureByName((char*)"dark"));
	newObject->GetRenderer()->SetRoughness(AssetManager::GetInstance()->GetTextureByName((char*)(materialName + "_R").c_str()), 1.0f);
	if (hasLightmap)
	{
		newObject->GetRenderer()->SetLightmap(AssetManager::GetInstance()->GetTextureByName((char*)(materialName + "_L").c_str()));
	}
	else
	{
		newObject->GetRenderer()->SetLightmap(AssetManager::GetInstance()->GetTextureByName((char*)"white"));
	}


	std::string fileLocationLeaves = fileLocation;
	if (isTree)
		fileLocation.append(".dae");
	else
		fileLocation.append(".fbx");
	

	if (isTree)
		newObject->GetRenderer()->SetMesh(Mesh::Load(m_graphics->GetDevice(), fileLocation.c_str(),false));
	else
		newObject->GetRenderer()->SetMesh(Mesh::Load(m_graphics->GetDevice(), fileLocation.c_str(), true));

	newObject->GetRenderer()->GetShader()->SetTessellation(true);
	newObject->GetRenderer()->SetTessellation(1);
	newObject->GetRenderer()->SetDisplacementStrength(0.0f);
	newObject->GetRenderer()->DefineMetal(true);
	if (isTree)
	{
		newsize = strlen(("Scene/Foliage/" + leafMaterialName + "_C.png").c_str()) + 1;
		wcstring = new wchar_t[newsize];
		convertedChars = 0;

		mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/Foliage/" + leafMaterialName + "_C.png").c_str(), _TRUNCATE);
		resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, true);

		mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/Foliage/" + leafMaterialName + "_N.png").c_str(), _TRUNCATE);
		resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, false);

		mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/Foliage/" + leafMaterialName + "_R.png").c_str(), _TRUNCATE);
		resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, true);

		mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/Foliage/" + leafMaterialName + "_A.png").c_str(), _TRUNCATE);
		resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, true);


		//BAYTREE
		Shader::TreeBufferType treeInfo3;
		treeInfo3.worldPosition = XMFLOAT4();
		treeInfo3.leafDimension = XMFLOAT4(0.105f, 0.008f, 0.25f, 0.25f); //0.075
		treeInfo3.orientationFlattening = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
		treeInfo3.leafGravity = XMFLOAT4(0, 0.0f, 0, 1);
		treeInfo3.numLeaves = 0;
		treeInfo3.numVertices = 0;
		treeInfo3.minimumDistanceLeaves = 0.0004f;
		treeInfo3.distancePadding = 0.3f;
		treeInfo3.leafStrength = 0.12f;
		treeInfo3.leafBranchInfluence = 0.5f;
		treeInfo3.leafStiffness = 0.5f;
		treeInfo3.doubleSided = 0;
		treeInfo3.staticBranches = 0;

		//IVY
		if (name == "IvyLeaves")
		{
			treeInfo3.worldPosition = XMFLOAT4();
			treeInfo3.leafDimension = XMFLOAT4(0.07f, 0.07f, 0.15f, 0.15f); //0.075
			treeInfo3.orientationFlattening = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
			treeInfo3.leafGravity = XMFLOAT4(0.0f, -0.2f, 0, 1);
			treeInfo3.numLeaves = 0;
			treeInfo3.numVertices = 0;
			treeInfo3.minimumDistanceLeaves = 0.004f;
			treeInfo3.distancePadding = 0.1f;
			treeInfo3.leafStrength = 0.4f;
			treeInfo3.leafBranchInfluence = 0.0f;
			treeInfo3.leafStiffness = 0.9f;
			treeInfo3.doubleSided = 0;
			treeInfo3.staticBranches = 1;
		}
		else if (name == "Pine2")
		{
			//PINES
			treeInfo3.worldPosition = XMFLOAT4();
			treeInfo3.leafDimension = XMFLOAT4(0.08f, 0.08f, 0.33f, 0.33f);
			treeInfo3.orientationFlattening = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
			treeInfo3.leafGravity = XMFLOAT4(0, 0.4f, 0, 1);
			treeInfo3.numLeaves = 0;
			treeInfo3.numVertices = 0;
			treeInfo3.minimumDistanceLeaves = 0.025f;
			treeInfo3.distancePadding = 0.0f;
			treeInfo3.leafStrength = 0.12f;
			treeInfo3.leafBranchInfluence = 0.5f;
			treeInfo3.leafStiffness = 0.5f;
			treeInfo3.doubleSided = 0;
			treeInfo3.staticBranches = 0;
		}
		else if (name == "Bompje")
		{
			//Bompje
			treeInfo3.worldPosition = XMFLOAT4();
			treeInfo3.leafDimension = XMFLOAT4(0.2f, 0.1f, 0.2f, 0.2f);
			treeInfo3.orientationFlattening = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
			treeInfo3.leafGravity = XMFLOAT4(0, -0.02f, 0, 1);
			treeInfo3.numLeaves = 0;
			treeInfo3.numVertices = 0;
			treeInfo3.minimumDistanceLeaves = 0.07f;
			treeInfo3.distancePadding = 0.5f;
			treeInfo3.leafStrength = 0.12f;
			treeInfo3.leafBranchInfluence = 0.5f;
			treeInfo3.leafStiffness = 0.5f;
			treeInfo3.doubleSided = 0;
			treeInfo3.staticBranches = 0;
		}
		else if (name == "Cypress")
		{
			//PINES
			treeInfo3.worldPosition = XMFLOAT4();
			treeInfo3.leafDimension = XMFLOAT4(0.2f, 0.2f, 0.2f, 0.2f);
			treeInfo3.orientationFlattening = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
			treeInfo3.leafGravity = XMFLOAT4(0, 0.12f, 0, 1);
			treeInfo3.numLeaves = 0;
			treeInfo3.numVertices = 0;
			treeInfo3.minimumDistanceLeaves = 0.07f;
			treeInfo3.distancePadding = 0.0f;
			treeInfo3.leafStrength = 0.15f;
			treeInfo3.leafBranchInfluence = 0.01f;
			treeInfo3.leafStiffness = 0.96f;
			treeInfo3.doubleSided = 0;
			treeInfo3.staticBranches = 1;
			treeInfo3.branchStrength = 1.5f;
		}
		else if (name == "Boxbush")
		{
			//BOXBUSH
			treeInfo3.worldPosition = XMFLOAT4();
			treeInfo3.leafDimension = XMFLOAT4(0.05f, 0.05f, 0.2f, 0.2f);
			treeInfo3.orientationFlattening = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
			treeInfo3.leafGravity = XMFLOAT4(0, 0.03f, 0, 1);
			treeInfo3.numLeaves = 0;
			treeInfo3.numVertices = 0;
			treeInfo3.minimumDistanceLeaves = 0.01f;
			treeInfo3.distancePadding = 0.5f;
			treeInfo3.leafStrength = 0.12f;
			treeInfo3.leafBranchInfluence = 0.5f;
			treeInfo3.leafStiffness = 0.8f;
			treeInfo3.doubleSided = 0;
			treeInfo3.staticBranches = 1;
			treeInfo3.branchStrength = 1.5f;
		}
		else if (name == "Bush")
		{
			//BUSH
			treeInfo3.worldPosition = XMFLOAT4();
			treeInfo3.leafDimension = XMFLOAT4(0.06f, 0.06f, 0.2f, 0.2f);
			treeInfo3.orientationFlattening = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
			treeInfo3.leafGravity = XMFLOAT4(0, 0.0f, 0, 1);
			treeInfo3.numLeaves = 0;
			treeInfo3.numVertices = 0;
			treeInfo3.minimumDistanceLeaves = 0.01f;
			treeInfo3.distancePadding = 0.6f;
			treeInfo3.leafStrength = 0.12f;
			treeInfo3.leafBranchInfluence = 0.5f;
			treeInfo3.leafStiffness = 0.35f;
			treeInfo3.doubleSided = 0;
			treeInfo3.staticBranches = 0;
			treeInfo3.branchStrength = 0.002f;
		}

		Foliage * foliage = new Foliage();
		foliage->Initialize(newObject, m_graphics, m_graphics->GetHwnd());
		newObject->AddComponent(foliage);
		foliage->SetTreeInfo(treeInfo3);
		fileLocationLeaves.append("Leaves.fbx");
		
		foliage->LoadLeafData(m_graphics->GetDevice(), Mesh::Load(m_graphics->GetDevice(), fileLocationLeaves), m_graphics->GetHwnd());
		foliage->GetRenderer()->SetDisplacement(AssetManager::GetInstance()->GetTextureByName((char*)(leafMaterialName + "_T").c_str()));
		foliage->GetRenderer()->SetAlbedo(AssetManager::GetInstance()->GetTextureByName((char*)(leafMaterialName + "_C").c_str()), XMFLOAT4(1, 1, 1, 1));
		foliage->GetRenderer()->SetNormal(AssetManager::GetInstance()->GetTextureByName((char*)(leafMaterialName + "_N").c_str()));
		foliage->GetRenderer()->SetCutout(AssetManager::GetInstance()->GetTextureByName((char*)(leafMaterialName + "_A").c_str()));
		foliage->GetRenderer()->SetMetal(AssetManager::GetInstance()->GetTextureByName((char*)"dark"), 0.0f);
		foliage->GetRenderer()->SetTranslucency(AssetManager::GetInstance()->GetTextureByName((char*)(leafMaterialName + "_C").c_str()));
		foliage->GetRenderer()->SetRoughness(AssetManager::GetInstance()->GetTextureByName((char*)(leafMaterialName + "_R").c_str()), 0.6f);
		foliage->GetRenderer()->SetTranslucencyColor(XMFLOAT4(0.8f, 0.8f, 1, 1));
		foliage->GetRenderer()->SetTessellation(1);
		foliage->GetRenderer()->SetDisplacementStrength(0.0f);
		
	}
	Add(newObject);
	
}

void Scene::CreateGameObjectTransparent(std::string name, std::string materialName, std::string leafMaterialName)
{
	AssetManager * resourceManager = AssetManager::GetInstance();
	GameObject * newObject = new GameObject();
	char * objectName = &name[0u];

	newObject->Initialize(objectName);

	std::string fileLocation = "Scene/";
	fileLocation.append(objectName);

	newObject->GetTransform()->SetPosition(0, 0, 0);
	newObject->AddRenderer(new Renderer());
	newObject->GetRenderer()->Initialize(newObject);
	newObject->GetRenderer()->SetUVTiling(1, 1);
	
	newObject->GetRenderer()->SetShader((StandardShader*)resourceManager->GetShaderByName("standard.fx"));

	size_t newsize = strlen(("Scene/" + materialName + "_C.png").c_str()) + 1;
	wchar_t * wcstring = new wchar_t[newsize];
	size_t convertedChars = 0;
	mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/" + materialName + "_C.png").c_str(), _TRUNCATE);
	resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, true);

	mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/" + materialName + "_M.png").c_str(), _TRUNCATE);
	resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, true);

	mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/" + materialName + "_N.png").c_str(), _TRUNCATE);
	resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, false);

	mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/" + materialName + "_R.png").c_str(), _TRUNCATE);
	resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, true);

	mbstowcs_s(&convertedChars, wcstring, newsize, ("Scene/" + materialName + "_A.png").c_str(), _TRUNCATE);
	resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), wcstring, true);

	newObject->GetRenderer()->SetAlbedo(AssetManager::GetInstance()->GetTextureByName((char*)(materialName + "_C").c_str()), XMFLOAT4(1.0, 1.0, 1.0, 1));
	newObject->GetRenderer()->SetNormal(AssetManager::GetInstance()->GetTextureByName((char*)(materialName + "_N").c_str()));
	newObject->GetRenderer()->SetDisplacement(AssetManager::GetInstance()->GetTextureByName((char*)"dark"));
	newObject->GetRenderer()->SetCutout(AssetManager::GetInstance()->GetTextureByName((char*)(materialName + "_A").c_str()));
	newObject->GetRenderer()->SetMetal(AssetManager::GetInstance()->GetTextureByName((char*)(materialName + "_M").c_str()), 1.0f);
	newObject->GetRenderer()->SetTranslucency(AssetManager::GetInstance()->GetTextureByName((char*)"dark"));
	newObject->GetRenderer()->SetRoughness(AssetManager::GetInstance()->GetTextureByName((char*)(materialName + "_R").c_str()), 1.0f);
	newObject->GetRenderer()->SetLightmap(AssetManager::GetInstance()->GetTextureByName((char*)"white"));

	std::string fileLocationLeaves = fileLocation;
	fileLocation.append(".fbx");
	newObject->GetRenderer()->SetMesh(Mesh::Load(m_graphics->GetDevice(), fileLocation.c_str(), true));

	newObject->GetRenderer()->GetShader()->SetTessellation(true);
	newObject->GetRenderer()->SetTessellation(1);
	newObject->GetRenderer()->SetDisplacementStrength(0.0f);
	newObject->GetRenderer()->DefineMetal(true);
	Add(newObject);

}