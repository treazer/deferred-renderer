#include "TerrainShader.h"
#include <iostream>

TerrainShader::TerrainShader(ID3D11Device * device, HWND hwnd, WCHAR * shaderFileName, bool usesTessellation) :
	StandardShader(device, hwnd, shaderFileName, usesTessellation)
{
}


TerrainShader::~TerrainShader()
{
}

bool TerrainShader::SetShaderParameters(ID3D11DeviceContext * deviceContext,
	ID3D11ShaderResourceView * albedo,
	ID3D11ShaderResourceView * normal,
	ID3D11ShaderResourceView * displacement,
	ID3D11ShaderResourceView * roughness,
	ID3D11ShaderResourceView * metal,
	ID3D11ShaderResourceView * translucency,
	ID3D11ShaderResourceView * cutout,
	float emission,
	float displacementStrength,
	int materialId)
{
	deviceContext->PSSetShaderResources(0, 1, &albedo);
	deviceContext->PSSetShaderResources(1, 1, &normal);
	if (m_useTessellation)
		deviceContext->DSSetShaderResources(2, 1, &displacement);
	deviceContext->PSSetShaderResources(3, 1, &roughness);
	deviceContext->PSSetShaderResources(4, 1, &metal);
	deviceContext->PSSetShaderResources(5, 1, &translucency);
	deviceContext->PSSetShaderResources(6, 1, &cutout);

	return true;
}
