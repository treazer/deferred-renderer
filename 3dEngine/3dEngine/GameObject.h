#pragma once

#include <vector>
#include <directxmath.h>
#include "Transform.h"
#include "Renderer.h"
#include "Component.h"

#define RESERVED_COMPONENTS 8

typedef std::vector<Component*> comp_list;
class Scene;
class GameObject
{
public:
	GameObject();
	~GameObject();

	void Initialize(char* name);
	comp_list GetComponents();
	void AddComponent(Component* comp);
	Transform* GetTransform();
	Renderer* GetRenderer(int id = 0);
	void SetRenderer(Renderer* renderer, int id);
	int AddRenderer(Renderer * renderer);
	int GetNumRenderer();
	char* GetName();
	void Update(Scene * scene);

private:
	Transform* m_transform;
	std::vector<Renderer*> m_renderer;
	comp_list m_components;
	char* m_name;
};