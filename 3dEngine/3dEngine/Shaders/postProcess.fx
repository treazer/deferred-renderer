static const float PI = 3.14159265f;
static float2 poissonDisk[64] =
{
        float2(-0.613392, 0.617481),float2(0.170019, -0.040254),float2(-0.299417, 0.791925),float2(0.645680, 0.493210),float2(-0.651784, 0.717887),float2(0.421003, 0.027070),float2(-0.817194, -0.271096),float2(-0.705374, -0.668203),float2(0.977050, -0.108615),float2(0.063326, 0.142369),float2(0.203528, 0.214331),float2(-0.667531, 0.326090),float2(-0.098422, -0.295755),float2(-0.885922, 0.215369),float2(0.566637, 0.605213),float2(0.039766, -0.396100),float2(0.751946, 0.453352),float2(0.078707, -0.715323),float2(-0.075838, -0.529344),float2(0.724479, -0.580798),float2(0.222999, -0.215125),float2(-0.467574, -0.405438),float2(-0.248268, -0.814753),float2(0.354411, -0.887570),float2(0.175817, 0.382366),float2(0.487472, -0.063082),float2(-0.084078, 0.898312),float2(0.488876, -0.783441),float2(0.470016, 0.217933),float2(-0.696890, -0.549791),float2(-0.149693, 0.605762),float2(0.034211, 0.979980),float2(0.503098, -0.308878),float2(-0.016205, -0.872921),float2(0.385784, -0.393902),float2(-0.146886, -0.859249),float2(0.643361, 0.164098),float2(0.634388, -0.049471),float2(-0.688894, 0.007843),float2(0.464034, -0.188818),float2(-0.440840, 0.137486),float2(0.364483, 0.511704),float2(0.034028, 0.325968),float2(0.099094, -0.308023),float2(0.693960, -0.366253),float2(0.678884, -0.204688),float2(0.001801, 0.780328),float2(0.145177, -0.898984),float2(0.062655, -0.611866),float2(0.315226, -0.604297),float2(-0.780145, 0.486251),float2(-0.371868, 0.882138),float2(0.200476, 0.494430),float2(-0.494552, -0.711051),float2(0.612476, 0.705252),float2(-0.578845, -0.768792),float2(-0.772454, -0.090976),float2(0.504440, 0.372295),float2(0.155736, 0.065157),float2(0.391522, 0.849605),float2(-0.620106, -0.328104),float2(0.789239, -0.419965),float2(-0.545396, 0.538133),float2(-0.178564, -0.596057)
};

static float3 sampleSphere[16] = {float3( 0.2024537f, 0.841204f, -0.9060141f), float3(-0.2200423f, 0.6282339f, -0.8275437f), float3( 0.3677659f, 0.1086345f, -0.4466777f), float3( 0.8775856f, 0.4617546f, -0.6427765f), float3( 0.7867433f, -0.141479f, -0.1567597f), float3( 0.4839356f, -0.8253108f, -0.1563844f), float3( 0.4401554f, -0.4228428f, -0.3300118f), float3( 0.0019193f, -0.8048455f, 0.0726584f), float3(-0.7578573f, -0.5583301f, 0.2347527f), float3(-0.4540417f, -0.252365f, 0.0694318f), float3(-0.0483353f, -0.2527294f, 0.5924745f), float3(-0.4192392f, 0.2084218f, -0.3672943f), float3(-0.8433938f, 0.1451271f, 0.2202872f), float3(-0.4037157f, -0.8263387f, 0.4698132f), float3(-0.6657394f, 0.6298575f, 0.6342437f), float3(-0.0001783f, 0.2834622f, 0.8343929f), }; 

static const float BlurWeights[9] =
{
0.026995,
0.064759,
0.120985,
0.176033,
0.199471,
0.176033,
0.120985,
0.064759,
0.026995,
};

/////////////
// GLOBALS //
/////////////
Texture2D albedoBuffer : register(t0);
Texture2D normalBuffer : register(t1);
//Texture2D roughnessBuffer : register(t2);
Texture2D depthBuffer : register(t3);
TextureCube environmentTexture : register(t4);
Texture2D shadowMap : register(t5);
Texture2D shadowColor : register(t6);
Texture2D translucencyBuffer : register(t7);
TextureCube irradianceTexture : register(t8);
Texture2D deferredBuffer : register(t9); ////
Texture2D postProcessBuffer : register(t10); ////
Texture2D deferredShadowBuffer : register(t11); ////
Texture2D deferredLuminanceBuffer : register(t12); ////
SamplerState SampleType : register(s0);
SamplerComparisonState SamplerShadow : register(s1);

//////////////
// CBUFFERS //
//////////////

cbuffer FrameDataBuffer : register(b0)
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projectionMatrix;
    matrix viewMatrixOld;
    matrix projectionMatrixOld;
    float4 eyePosition;
    float4 wind;
    float4 time;
};

cbuffer DeferredBufferType : register(b1)
{
    matrix lightViewMatrix;
    matrix lightOrthoMatrix;
    matrix worldMatrixDeferred;
    matrix viewMatrixDeferred;
    matrix projectionMatrixDeferred;
    float4 sunColor;
    float4 sunDirection;
    float4 sunPosition;
    float4 eye;
};

cbuffer RendererBuffer: register(b2)
{
    float tessellationAmount;
    float emission;
    float displacementStrength;
    float materialId;
    float normalStrength;
    float uvTilingX;
    float uvTilingY;
    float a;
};

cbuffer ObjectBuffer: register(b3)
{
    matrix treeBones[256];
    matrix treeBoneOffsets[256];
    float4 treeBoneHeights[256];
};

cbuffer PostProcessBufferType: register(b4)
{
	float passType;		
    float passNr;
	float screenResolutionX;
    float screenResolutionY;
    float d;
    float e;
    float f;
    float g;
};


//////////////
// TYPEDEFS //
//////////////
struct VertexInputType
{
    float4 position : POSITION;
    float4 color : COLOR;
    float4 tex : TEXCOORD0;
    float4 normal : NORMAL;
    float4 tangent : TANGENT;
    float4 bitangent : BITANGENT;
    float4 boneWeights : COLOR1;
    float4 boneIDs : COLOR2;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
    float2 tex : TEXCOORD0;
    float3 normal : TEXCOORD1;
};

struct HullInputType
{
    float3 position : POSITION;
    float4 color : COLOR;
};

struct HullOutputType
{
    float3 position : POSITION;
    float4 color : COLOR;
};

struct ConstantOutputType
{
    float edges[3] : SV_TessFactor;
    float inside : SV_InsideTessFactor;
};

struct SURFACE_DATA
{
    float3 Position;
    float3 Normal;
    float Roughness;
    float Metalness;
    float3 Color;
};

struct PS_OUTPUT
{
float4 Diffuse : SV_Target0;
};


//FUNCTION DEFINITIONS
float3 ComputeSpecFactor( in SURFACE_DATA SurfaceData, in float3 LightDirection, in float NdotL );
float random( float2 p );
float3 randomNormal(float2 vec);
float4 AddFog(float4 color, float3 eyePosition, float3 pixelPosition);
float Vignetting(float2 vec);

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelInputType VSMain(VertexInputType input)
{
    PixelInputType output;
    //HullInputType output; //switch to this one
    
    // Change the position vector to be 4 units for proper matrix calculations.
    input.position.w = 1.0f;
    
    // Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(input.position, worldMatrixDeferred);
    output.position = mul(output.position, viewMatrixDeferred);
    output.position = mul(output.position, projectionMatrixDeferred);

    // Store the texture coordinates for the pixel shader.
    output.tex = input.tex;
    output.normal = input.normal;
    return output;
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////

float3 BlurLight(Texture2D sampleBase, int isVertical, float2 texCoord, float strength)
{
    float pixelSizeX = 1/screenResolutionX;
    float pixelSizeY = 1/screenResolutionY;
    float2 samplePoint = float2(pixelSizeX,0.0f)*1.5f;
    if (isVertical==1)
    {
        samplePoint = float2(0,pixelSizeY);
    }

    float3 result;
    float level = 0;
    
    float mainDepth = depthBuffer.SampleLevel(SampleType, texCoord.xy,level).x;
    
    result = sampleBase.SampleLevel(SampleType, texCoord.xy,level) * BlurWeights[4];     
    result += sampleBase.SampleLevel(SampleType, texCoord.xy - (samplePoint*4),level).xyz * BlurWeights[0];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy - (samplePoint*3),level).xyz * BlurWeights[1];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy - (samplePoint*2),level).xyz * BlurWeights[2];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy - samplePoint,level).xyz* BlurWeights[3];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy + samplePoint,level).xyz * BlurWeights[5];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy + (samplePoint*2),level).xyz * BlurWeights[6];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy + (samplePoint*3),level).xyz * BlurWeights[7];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy + (samplePoint*4),level).xyz * BlurWeights[8];
    
    level = 1;
    pixelSizeX = 1/(screenResolutionX/2);
    pixelSizeY = 1/(screenResolutionY/2);
    samplePoint = float2(pixelSizeX,0.0f)*1.5f;
    if (isVertical==1)
    {
        samplePoint = float2(0,pixelSizeY);
    }
    
    result += sampleBase.SampleLevel(SampleType, texCoord.xy,level) * BlurWeights[4];     
    result += sampleBase.SampleLevel(SampleType, texCoord.xy - (samplePoint*4),level).xyz * BlurWeights[0];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy - (samplePoint*3),level).xyz * BlurWeights[1];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy - (samplePoint*2),level).xyz * BlurWeights[2];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy - samplePoint,level).xyz* BlurWeights[3];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy + samplePoint,level).xyz * BlurWeights[5];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy + (samplePoint*2),level).xyz * BlurWeights[6];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy + (samplePoint*3),level).xyz * BlurWeights[7];
    result += sampleBase.SampleLevel(SampleType, texCoord.xy + (samplePoint*4),level).xyz * BlurWeights[8];
result/=2;
    return result;
    
}

float3 BlurSample(int isVertical, float2 texCoord, float strength)
{
    float pixelSizeX = 1/screenResolutionX;
    float pixelSizeY = 1/screenResolutionY;
    float2 samplePoint = float2(pixelSizeX,0.0f)*1.5f;
    if (isVertical==1)
    {
        //samplePoint = float2(0,pixelSizeY);
    }

    float3 result;
    float level = 0;
    int rounds = 4;
    for (int i = 0; i<rounds;i++)
    {
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy - (samplePoint*4),level) * BlurWeights[0];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy - (samplePoint*3),level) * BlurWeights[1];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy - (samplePoint*2),level) * BlurWeights[2];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy - samplePoint,level) * BlurWeights[3];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy,1) * BlurWeights[4];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy + samplePoint,level) * BlurWeights[5];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy + (samplePoint*2),level) * BlurWeights[6];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy + (samplePoint*3),level) * BlurWeights[7];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy + (samplePoint*4),level) * BlurWeights[8];
    
    samplePoint = float2(0,pixelSizeY)*1.5f;

    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy - (samplePoint*4),level) * BlurWeights[0];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy - (samplePoint*3),level) * BlurWeights[1];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy - (samplePoint*2),level) * BlurWeights[2];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy - samplePoint,level) * BlurWeights[3];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy,1) * BlurWeights[4];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy + samplePoint,level) * BlurWeights[5];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy + (samplePoint*2),level) * BlurWeights[6];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy + (samplePoint*3),level) * BlurWeights[7];
    result += deferredBuffer.SampleLevel(SampleType, texCoord.xy + (samplePoint*4),level) * BlurWeights[8];

    }
    result/=rounds;
    return result;
    
}

float3 BP_PShader(float2 texCoord, float level, float illumination)
{
	float3 Value = deferredBuffer.SampleLevel(SampleType, texCoord, level);
	Value *= illumination;
	Value -= 0.5f;
	Value  = max(Value,0.0f);
	Value /= (10.0f + Value);

	return Value;
}

float3 BlurShadow(Texture2D sampleBase, int isVertical, float2 texCoord, float strength)
{
    float pixelSizeX = 1/screenResolutionX;
    float pixelSizeY = 1/screenResolutionY;
    float2 samplePoint = float2(pixelSizeX,0.0f);
    if (isVertical==1)
    {
        samplePoint = float2(0,pixelSizeY);
    }

    float3 result;
    float level = 0;
    
    float mainDepth = depthBuffer.SampleLevel(SampleType, texCoord.xy,level).x;
    
    result = sampleBase.SampleLevel(SampleType, texCoord.xy,level) * BlurWeights[4];
        
    //if (abs(mainDepth-depthBuffer.SampleLevel(SampleType, texCoord.xy - (samplePoint*4),level).x)<0.001f)
        result += sampleBase.SampleLevel(SampleType, texCoord.xy - (samplePoint*4),level).w * BlurWeights[0];
    
    //if (abs(mainDepth-depthBuffer.SampleLevel(SampleType, texCoord.xy - (samplePoint*3),level).x)<0.001f)   
        result += sampleBase.SampleLevel(SampleType, texCoord.xy - (samplePoint*3),level).w * BlurWeights[1];
    
    //if (abs(mainDepth-depthBuffer.SampleLevel(SampleType, texCoord.xy - (samplePoint*2),level).x)<0.001f)   
        result += sampleBase.SampleLevel(SampleType, texCoord.xy - (samplePoint*2),level).w * BlurWeights[2];
    
    //if (abs(mainDepth-depthBuffer.SampleLevel(SampleType, texCoord.xy - (samplePoint),level).x)<0.001f)   
        result += sampleBase.SampleLevel(SampleType, texCoord.xy - samplePoint,level).w * BlurWeights[3];
    
    //if (abs(mainDepth-depthBuffer.SampleLevel(SampleType, texCoord.xy + (samplePoint),level).x)<0.001f)   
        result += sampleBase.SampleLevel(SampleType, texCoord.xy + samplePoint,level).w * BlurWeights[5];
    
    //if (abs(mainDepth-depthBuffer.SampleLevel(SampleType, texCoord.xy + (samplePoint*2),level).x)<0.001f)   
    result += sampleBase.SampleLevel(SampleType, texCoord.xy + (samplePoint*2),level).w * BlurWeights[6];
    
    //if (abs(mainDepth-depthBuffer.SampleLevel(SampleType, texCoord.xy + (samplePoint*3),level).x)<0.002f)   
    result += sampleBase.SampleLevel(SampleType, texCoord.xy + (samplePoint*3),level).w * BlurWeights[7];
    
    //if (abs(mainDepth-depthBuffer.SampleLevel(SampleType, texCoord.xy + (samplePoint*4),level).x)<0.001f)   
    result += sampleBase.SampleLevel(SampleType, texCoord.xy + (samplePoint*4),level).w * BlurWeights[8];
    //result = pow(result,0.9f);

    //result*=0.5f;
    //result =  postProcessBuffer.SampleLevel(SampleType, texCoord.xy,0).xyz;
    return result;
    
}

float3 SampleLightmass(Texture2D sampleBase, float2 texCoord, float level)
{
    float pixelSizeX = 1/(screenResolutionX/level);
    float pixelSizeY = 1/(screenResolutionY/level);
    float numSamples = 12;
    float3 light = float3(0,0,0);
    for (int i = 0; i < numSamples; ++i)
    { 
        float2 offset = (poissonDisk[i].xy*level)*float2(pixelSizeX,pixelSizeY);
        light += sampleBase.SampleLevel(SampleType, texCoord+offset,level).xxx; //xyz
    }
    //light /= numSamples;
    light*=float3(1,1,0.95f);
    return light;
}

PS_OUTPUT PSMain(PixelInputType input) : SV_TARGET
{
	PS_OUTPUT output;
    float3 sunlight;
    float shadow = 1.0f;

    Texture2D sampleBase;
    if (passNr==7)
    {
        sunlight = (deferredBuffer.Sample(SampleType, input.tex).xyz);
        float3 light = deferredShadowBuffer.Sample(SampleType, input.tex).xyz;
        float shadowSample = saturate(postProcessBuffer.Sample(SampleType, input.tex).w);
        sunlight += BlurLight(postProcessBuffer, passType,input.tex,1);
        sunlight = sunlight + (light*12)*shadowSample.xxx;

        //sunlight = (deferredBuffer.Sample(SampleType, input.tex).xyz); //debug
        //sunlight = (deferredLuminanceBuffer.Sample(SampleType, input.tex).xxx); //debug
        shadow = 1.0f;
    }
    else if (passNr == 0)
    {
        sampleBase = deferredShadowBuffer;
        sunlight = deferredLuminanceBuffer.Sample(SampleType, input.tex).xxx*0; //.xyz

        sunlight += SampleLightmass(deferredLuminanceBuffer,input.tex,1);
        sunlight += SampleLightmass(deferredLuminanceBuffer,input.tex,2)*float3(1,0.8f,0.6f);
        sunlight += SampleLightmass(deferredLuminanceBuffer,input.tex,3)*float3(1,0.8f,0.6f);
        sunlight += SampleLightmass(deferredLuminanceBuffer,input.tex,4)*float3(1,0.8f,0.6f)*0.1f;
        sunlight += SampleLightmass(deferredLuminanceBuffer,input.tex,5)*float3(1,0.8f,0.6f)*0.05f;
        sunlight += SampleLightmass(deferredLuminanceBuffer,input.tex,6)*float3(1,0.8f,0.6f)*0.05f;
        sunlight += SampleLightmass(deferredLuminanceBuffer,input.tex,8)*float3(1,0.8f,0.6f)*0.2f;
        sunlight += SampleLightmass(deferredLuminanceBuffer,input.tex,9)*float3(1,0.8f,0.6f)*0.2f;

        sunlight/=2;
        shadow = sampleBase.Sample(SampleType, input.tex).w;
    }
    else
    {
        sampleBase = postProcessBuffer;
        shadow = BlurShadow(sampleBase, passType,input.tex,1).x;
        sunlight = BlurLight(sampleBase, passType,input.tex,1);
    }
    output.Diffuse = float4(sunlight,shadow);
    return output;
}


////////////////////////////////////////////////////////////////////////////////
// Assisting functions
////////////////////////////////////////////////////////////////////////////////

float random( float2 p )
{
  // We need irrationals for pseudo randomness.
  // Most (all?) known transcendental numbers will (generally) work.
  const float2 r = float2(
    23.1406926327792690,  // e^pi (Gelfond's constant)
     2.6651441426902251); // 2^sqrt(2) (Gelfond–Schneider constant)
  return frac( cos( fmod( 123456789., 1e-7 + 256. * dot(p,r) ) ) );  
}

//Based on: https://gist.github.com/keijiro/ee7bc388272548396870
float3 randomNormal(float2 vec)
{
     float x = frac(sin(dot(vec, float2(12.9898, 78.233))) * 41758.5453);
     float y = frac(sin(dot(vec, float2(13.9898, 76.233))) * 53758.5453);
     float z = frac(sin(dot(vec, float2(14.9898, 73.233))) * 43258.5453);
     return normalize(float3(x,y,z));
}

float Vignetting(float2 vec)
{
    vec+=float2(-0.5f,-0.5f);
    float c = 1;
    float lensRadius = 1.3; //1.05
    vec /= lensRadius;
    float sin2 = vec.x*vec.x+vec.y*vec.y;
    float cos2 = 1.0-min(sin2*sin2,1.0);
    float cos4 = cos2*cos2;
    c *= cos4;
    return c;
}

PS_OUTPUT PSMainShadow(PixelInputType input) : SV_TARGET
{
    PS_OUTPUT output;
    output.Diffuse = float4(1,1,1,1);
    return output;
}

PS_OUTPUT PSMainTranslucency(PixelInputType input) : SV_TARGET
{
    PS_OUTPUT output;
    output.Diffuse = float4(1,0.5f,1,1);
    return output;
}