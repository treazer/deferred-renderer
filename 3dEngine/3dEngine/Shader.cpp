#include "Shader.h"
#include <d3dcompiler.h>
#include <iostream>
#include <vector>
#include "Timer.h"
#include "Mesh.h"
Shader::Shader(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName, bool usesTessellation)
{
	m_vertexShader = 0;
	m_pixelShader = 0;
	m_pixelShadowShader = 0;
	m_pixelTranslucencyShader = 0;
	m_hullShader = 0;
	m_domainShader = 0;
	m_layout = 0;
	m_objectBuffer = 0;
	m_rendererBuffer = 0;
	m_useTessellation = false;
	m_name = "";

	m_initialized = Initialize(device, hwnd, shaderFileName, usesTessellation);
	assert(m_initialized);
}

Shader::~Shader(void)
{
	// Release the matrix constant buffer.
	if (m_objectBuffer)
	{
		m_objectBuffer->Release();
		m_objectBuffer = 0;
	}

	// Release the layout.
	if (m_layout)
	{
		m_layout->Release();
		m_layout = 0;
	}

	// Release the pixel shader.
	if (m_pixelShader)
	{
		m_pixelShader->Release();
		m_pixelShader = 0;
	}

	// Release the vertex shader.
	if (m_vertexShader)
	{
		m_vertexShader->Release();
		m_vertexShader = 0;
	}
	// Release the tessellation constant buffer.
	if (m_rendererBuffer)
	{
		m_rendererBuffer->Release();
		m_rendererBuffer = 0;
	}
	m_name.clear();
}

bool Shader::Initialize(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName, bool usesTessellation)
{
	wstring shaderFilePath(shaderFileName);
	m_name = string(shaderFilePath.begin(), shaderFilePath.end());

	int pos = m_name.find_last_of("/");
	if (pos >= 0)
	{
		m_name = m_name.substr(pos + 1, m_name.length());
	}

	//initialize the vertex and pixel shaders
	return InitializeShader(device, hwnd, shaderFileName, usesTessellation);
}

void Shader::Begin(ID3D11DeviceContext* deviceContext)
{
	//set the vertex input layout
	deviceContext->IASetInputLayout(m_layout);

	//Set the vertex and pixel shaders that will used to render
	deviceContext->VSSetShader(m_vertexShader, NULL, 0);
	deviceContext->PSSetShader(m_pixelShader, NULL, 0);
	if (m_useTessellation)
	{
		deviceContext->HSSetShader(m_hullShader, NULL, 0);
		deviceContext->DSSetShader(m_domainShader, NULL, 0);
	}
	else
	{
		deviceContext->HSSetShader(NULL, NULL, 0);
		deviceContext->DSSetShader(NULL, NULL, 0);
	}
}

void Shader::BeginShadow(ID3D11DeviceContext* deviceContext)
{
	//set the vertex input layout
	deviceContext->IASetInputLayout(m_layout);

	//Set the vertex and pixel shaders that will used to render
	deviceContext->VSSetShader(m_vertexShader, NULL, 0);
	deviceContext->PSSetShader(m_pixelShadowShader, NULL, 0);
	if (m_useTessellation)
	{
		deviceContext->HSSetShader(m_hullShader, NULL, 0);
		deviceContext->DSSetShader(m_domainShader, NULL, 0);
	}
	else
	{
		deviceContext->HSSetShader(NULL, NULL, 0);
		deviceContext->DSSetShader(NULL, NULL, 0);
	}
}

void Shader::BeginTranslucency(ID3D11DeviceContext* deviceContext)
{
	//set the vertex input layout
	deviceContext->IASetInputLayout(m_layout);

	//Set the vertex and pixel shaders that will used to render
	deviceContext->VSSetShader(m_vertexShader, NULL, 0);
	deviceContext->PSSetShader(m_pixelTranslucencyShader, NULL, 0);
	if (m_useTessellation)
	{
		deviceContext->HSSetShader(m_hullShader, NULL, 0);
		deviceContext->DSSetShader(m_domainShader, NULL, 0);
	}
	else
	{
		deviceContext->HSSetShader(NULL, NULL, 0);
		deviceContext->DSSetShader(NULL, NULL, 0);
	}
}


void Shader::End(ID3D11DeviceContext* deviceContext)
{
	deviceContext->IASetInputLayout(NULL);
	deviceContext->VSSetShader(NULL, NULL, 0);
	deviceContext->PSSetShader(NULL, NULL, 0);
	deviceContext->HSSetShader(NULL, NULL, 0);
	deviceContext->DSSetShader(NULL, NULL, 0);
}

bool Shader::InitializeShader(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName, bool usesTessellation)
{
	HRESULT result;
	ID3D10Blob* errorMessage = 0;
	ID3D10Blob* vertexShaderBuffer = 0;
	ID3D10Blob* pixelShaderBuffer = 0;
	ID3D10Blob* pixelShadowShaderBuffer = 0;
	ID3D10Blob* pixelTranslucencyShaderBuffer = 0;
	ID3D10Blob* hullShaderBuffer = 0;
	ID3D10Blob* domainShaderBuffer = 0;
	D3D11_INPUT_ELEMENT_DESC polygonLayout[8];
	unsigned int numElements;
	D3D11_BUFFER_DESC frameDataBufferDesc;
	D3D11_BUFFER_DESC rendererBufferDesc;
	D3D11_BUFFER_DESC objectBufferDesc;
	D3D11_BUFFER_DESC postProcessBufferDesc;
	D3D11_BUFFER_DESC treeRenderBufferDesc;

	// Compile vertex shader
	result = D3DCompileFromFile(shaderFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "VSMain", "vs_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &vertexShaderBuffer, &errorMessage);
	if (FAILED(result))
	{
		if (errorMessage)
		{
			OutputShadeErrorMessage(errorMessage, hwnd, shaderFileName);
		}
		else
		{
			MessageBox(hwnd, (LPCWSTR)shaderFileName, L"Error in VS Shader File", MB_OK);
		}

		return false;
	}

	// Compile pixel shader
	result = D3DCompileFromFile(shaderFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PSMain", "ps_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &pixelShaderBuffer, &errorMessage);
	if (FAILED(result))
	{
		if (errorMessage)
		{
			OutputShadeErrorMessage(errorMessage, hwnd, shaderFileName);
		}
		else
		{
			MessageBox(hwnd, (LPCWSTR)shaderFileName, L"Error in PS Shader File", MB_OK);
		}
	}

	// Compile pixel shadow shader
	result = D3DCompileFromFile(shaderFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PSMainShadow", "ps_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &pixelShadowShaderBuffer, &errorMessage);
	if (FAILED(result))
	{
		if (errorMessage)
		{
			OutputShadeErrorMessage(errorMessage, hwnd, shaderFileName);
		}
		else
		{
			MessageBox(hwnd, (LPCWSTR)shaderFileName, L"Error in PS Shadow Shader File", MB_OK);
		}
	}

	// Compile pixel translucency shader
	result = D3DCompileFromFile(shaderFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PSMainTranslucency", "ps_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &pixelTranslucencyShaderBuffer, &errorMessage);
	if (FAILED(result))
	{
		if (errorMessage)
		{
			OutputShadeErrorMessage(errorMessage, hwnd, shaderFileName);
		}
		else
		{
			MessageBox(hwnd, (LPCWSTR)shaderFileName, L"Error in PS Translucency Shader File", MB_OK);
		}
	}

	if (usesTessellation)
	{

		// Compile the hull shader code.
		result = D3DCompileFromFile(shaderFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "ColorHullShader", "hs_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &hullShaderBuffer, &errorMessage);
		if (FAILED(result))
		{
			std::cout << "failed in hull shader" << std::endl;
			// If the shader failed to compile it should have writen something to the error message.
			if (errorMessage)
			{
				OutputShadeErrorMessage(errorMessage, hwnd, shaderFileName);
			}
			// If there was nothing in the error message then it simply could not find the shader file itself.
			else
			{
				MessageBox(hwnd, (LPCWSTR)shaderFileName, L"Error in Hull Shader File", MB_OK);
			}

			return false;
		}

		// Compile the domain shader code.
		result = D3DCompileFromFile(shaderFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "ColorDomainShader", "ds_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &domainShaderBuffer, &errorMessage);
		if (FAILED(result))
		{
			// If the shader failed to compile it should have writen something to the error message.
			if (errorMessage)
			{
				OutputShadeErrorMessage(errorMessage, hwnd, shaderFileName);
			}
			// If there was nothing in the error message then it simply could not find the shader file itself.
			else
			{
				MessageBox(hwnd, (LPCWSTR)shaderFileName, L"Error in Domain Shader File", MB_OK);
			}

			return false;
		}
	}

	// Create vertex shader buffera
	result = device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), NULL, &m_vertexShader);
	if (FAILED(result))
	{
		return false;
	}

	// Create pixel shader buffer
	result = device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(), pixelShaderBuffer->GetBufferSize(), NULL, &m_pixelShader);
	if (FAILED(result))
	{
		return false;
	}

	// Create pixel shadow shader buffer
	result = device->CreatePixelShader(pixelShadowShaderBuffer->GetBufferPointer(), pixelShadowShaderBuffer->GetBufferSize(), NULL, &m_pixelShadowShader);
	if (FAILED(result))
	{
		return false;
	}

	// Create pixel shadow shader buffer
	result = device->CreatePixelShader(pixelTranslucencyShaderBuffer->GetBufferPointer(), pixelTranslucencyShaderBuffer->GetBufferSize(), NULL, &m_pixelTranslucencyShader);
	if (FAILED(result))
	{
		return false;
	}

	if (usesTessellation)
	{
		// Create the hull shader from the buffer.
		result = device->CreateHullShader(hullShaderBuffer->GetBufferPointer(), hullShaderBuffer->GetBufferSize(), NULL, &m_hullShader);
		if (FAILED(result))
		{
			std::cout << "failed creating hull shader" << std::endl;
			return false;
		}

		// Create the domain shader from the buffer.
		result = device->CreateDomainShader(domainShaderBuffer->GetBufferPointer(), domainShaderBuffer->GetBufferSize(), NULL, &m_domainShader);
		if (FAILED(result))
		{
			std::cout << "failed creating domain shader" << std::endl;
			return false;
		}
	}
	// Setup the layout of the data that goes into the shader
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "COLOR";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32B32A32_FLOAT; //DXGI_FORMAT_R32G32B32A32_FLOAT
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	polygonLayout[2].SemanticName = "TEXCOORD";
	polygonLayout[2].SemanticIndex = 0;
	polygonLayout[2].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	polygonLayout[2].InputSlot = 0;
	polygonLayout[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[2].InstanceDataStepRate = 0;

	polygonLayout[3].SemanticName = "NORMAL";
	polygonLayout[3].SemanticIndex = 0;
	polygonLayout[3].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	polygonLayout[3].InputSlot = 0;
	polygonLayout[3].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[3].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[3].InstanceDataStepRate = 0;

	polygonLayout[4].SemanticName = "TANGENT";
	polygonLayout[4].SemanticIndex = 0;
	polygonLayout[4].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	polygonLayout[4].InputSlot = 0;
	polygonLayout[4].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[4].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[4].InstanceDataStepRate = 0;

	polygonLayout[5].SemanticName = "BITANGENT";
	polygonLayout[5].SemanticIndex = 0;
	polygonLayout[5].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	polygonLayout[5].InputSlot = 0;
	polygonLayout[5].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[5].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[5].InstanceDataStepRate = 0;
	
	polygonLayout[6].SemanticName = "COLOR";
	polygonLayout[6].SemanticIndex = 1;
	polygonLayout[6].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	polygonLayout[6].InputSlot = 0;
	polygonLayout[6].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[6].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[6].InstanceDataStepRate = 0;

	polygonLayout[7].SemanticName = "COLOR";
	polygonLayout[7].SemanticIndex = 2;
	polygonLayout[7].Format = DXGI_FORMAT_R32G32B32A32_FLOAT; // DXGI_FORMAT_R16G16B16A16_SINT;
	polygonLayout[7].InputSlot = 0;
	polygonLayout[7].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[7].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[7].InstanceDataStepRate = 0;
	
	numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	// Create the vertex input layout
	result = device->CreateInputLayout(polygonLayout, numElements, vertexShaderBuffer->GetBufferPointer(),
		vertexShaderBuffer->GetBufferSize(), &m_layout);
	if (FAILED(result))
	{
		std::cout << "failed to create input layout" << std::endl;
		return false;
	}

	// Release shader buffers
	vertexShaderBuffer->Release();
	vertexShaderBuffer = 0;

	pixelShaderBuffer->Release();
	pixelShaderBuffer = 0;

	// Setup the frame data buffer description
	frameDataBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	frameDataBufferDesc.ByteWidth = sizeof(FrameDataBufferType);
	frameDataBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	frameDataBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	frameDataBufferDesc.MiscFlags = 0;
	frameDataBufferDesc.StructureByteStride = 0;

	// Setup the description of the renderer constant buffer that is in the hull shader.
	rendererBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	rendererBufferDesc.ByteWidth = sizeof(RendererBufferType);
	rendererBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	rendererBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	rendererBufferDesc.MiscFlags = 0;
	rendererBufferDesc.StructureByteStride = 0;

	// Setup the description of the object constant buffer that is in the hull shader.
	objectBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	objectBufferDesc.ByteWidth = sizeof(ObjectBufferType);
	objectBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	objectBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	objectBufferDesc.MiscFlags = 0;
	objectBufferDesc.StructureByteStride = 0;

	// Setup the description of the post process constant buffer that is in the hull shader.
	postProcessBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	postProcessBufferDesc.ByteWidth = sizeof(PostProcessBufferType);
	postProcessBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	postProcessBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	postProcessBufferDesc.MiscFlags = 0;
	postProcessBufferDesc.StructureByteStride = 0;

	// Setup the description of the tree constant buffer that is in the hull shader.
	treeRenderBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	treeRenderBufferDesc.ByteWidth = sizeof(TreeRenderBufferType);
	treeRenderBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	treeRenderBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	treeRenderBufferDesc.MiscFlags = 0;
	treeRenderBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointers

	result = device->CreateBuffer(&rendererBufferDesc, NULL, &m_rendererBuffer);
	if (FAILED(result))
	{
		std::cout << "failed to create renderer buffer desc" << std::endl;
		return false;
	}

	result = device->CreateBuffer(&frameDataBufferDesc, NULL, &m_frameDataBuffer);
	if (FAILED(result))
	{
		std::cout << "failed to create matrix buffer desc" << std::endl;
		return false;
	}

	result = device->CreateBuffer(&objectBufferDesc, NULL, &m_objectBuffer);
	if (FAILED(result))
	{
		std::cout << "failed to create object buffer desc" << std::endl;
		return false;
	}

	result = device->CreateBuffer(&postProcessBufferDesc, NULL, &m_postProcessBuffer);
	if (FAILED(result))
	{
		std::cout << "failed to create post process buffer desc" << std::endl;
		return false;
	}

	result = device->CreateBuffer(&treeRenderBufferDesc, NULL, &m_treeBuffer);
	if (FAILED(result))
	{
		std::cout << "failed to create tree rendering buffer desc" << std::endl;
		return false;
	}


	return true;
}

void Shader::OutputShadeErrorMessage(ID3D10Blob* errorMessage, HWND hwnd, WCHAR* shaderFileName)
{
	char* compileErrors = (char*)errorMessage->GetBufferPointer();
	unsigned long bufferSize = errorMessage->GetBufferSize();

	ofstream fout;
	fout.open("Shaders/shader-error.txt");

	for (unsigned int i = 0; i < bufferSize; ++i)
	{
		fout << compileErrors[i];
	}

	fout.close();

	errorMessage->Release();
	errorMessage = 0;

	MessageBox(hwnd, L"Error compiling shader. Check shader-error.txt for message", (LPCWSTR)shaderFileName, MB_OK);

	return;
}

bool Shader::SetShaderParameters(ID3D11DeviceContext* deviceContext, ID3D11ShaderResourceView* texture, int slot)
{
	if (m_useTessellation)
		deviceContext->DSSetShaderResources(slot, 1, &texture);
	deviceContext->PSSetShaderResources(slot, 1, &texture);
	return true;
}

bool Shader::SetShaderParameters(ID3D11DeviceContext* deviceContext,
									XMFLOAT4X4 worldMatrix,
									XMFLOAT4X4 viewMatrix,
									XMFLOAT4X4 projectionMatrix,
									XMFLOAT4 eyePosition,
									XMFLOAT4 wind,
									XMFLOAT4X4 viewMatrixOld,
									XMFLOAT4X4 projectionMatrixOld,
									int currentFrame,
									int frameSteps)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	FrameDataBufferType* dataPtr;
	unsigned int bufferNumber;

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_frameDataBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	if (FAILED(result))
	{
		return false;
	}

	// Get a pointer to the data in the constant buffer.
	dataPtr = (FrameDataBufferType*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	dataPtr->worldMatrix = XMMatrixTranspose(XMLoadFloat4x4(&worldMatrix));
	dataPtr->viewMatrix = XMMatrixTranspose(XMLoadFloat4x4(&viewMatrix));
	dataPtr->projectionMatrix = XMMatrixTranspose(XMLoadFloat4x4(&projectionMatrix));
	dataPtr->viewMatrixOld = XMMatrixTranspose(XMLoadFloat4x4(&viewMatrixOld));
	dataPtr->projectionMatrixOld = XMMatrixTranspose(XMLoadFloat4x4(&projectionMatrixOld));
	dataPtr->time = XMFLOAT4(Timer::GetTimeSinceStart(), Timer::GetTimeSinceStart(), Timer::GetTimeSinceStart(), Timer::GetTimeSinceStart());
	dataPtr->eyePosition = eyePosition;
	dataPtr->wind = wind;
	dataPtr->reconstructionValues = XMFLOAT4(currentFrame, frameSteps, 0, 0);
	// Unlock the constant buffer.
	deviceContext->Unmap(m_frameDataBuffer, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;

	// Finanly set the constant buffer in the vertex shader with the updated values. //domain shader now
	if (m_useTessellation)
	{
		deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_frameDataBuffer);
		deviceContext->DSSetConstantBuffers(bufferNumber, 1, &m_frameDataBuffer);
	}
	else
	{
		deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_frameDataBuffer);
	}
	deviceContext->PSSetConstantBuffers(bufferNumber, 1, &m_frameDataBuffer);
	return true;
}

bool Shader::SetShaderParameters(ID3D11DeviceContext * deviceContext, Mesh * mesh)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ObjectBufferType* dataPtr;
	unsigned int bufferNumber;

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_objectBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	if (FAILED(result))
	{
		return false;
	}

	// Get a pointer to the data in the tessellation constant buffer.
	dataPtr = (ObjectBufferType*)mappedResource.pData;

	// Copy the tessellation data into the constant buffer.
	if (mesh != nullptr && mesh->HasBones())
	{

		std::vector<Mesh::BoneInfoData> boneData = *mesh->GetBoneInfo();

		for (unsigned int i = 0; i < MAX_BONES; i++)
		{
			if (i < boneData.size())
				dataPtr->treeBones[i] = boneData[i].BoneTransform;
			else
				dataPtr->treeBones[i] = XMFLOAT4X4();
		}

		for (unsigned int i = 0; i < MAX_BONES; i++)
		{
			if (i < boneData.size())
				dataPtr->treeBoneOffsets[i] = boneData[i].BoneOffset;
			else
				dataPtr->treeBoneOffsets[i] = XMFLOAT4X4();
		}

		for (unsigned int i = 0; i < MAX_BONES; i++)
		{
			if (i < boneData.size())
			{
				dataPtr->treeBoneHeights[i] = XMFLOAT4(boneData[i].height, (float)boneData[i].level, boneData[i].height, boneData[i].height);
			}
			else
				dataPtr->treeBoneHeights[i] = XMFLOAT4(0.0f, 0, 0, 0);
		}

		for (unsigned int i = 0; i < MAX_BONES; i++)
		{
			if (i < boneData.size())
			{
				dataPtr->treeForces[i] = boneData[i].force;
			}
			else
				dataPtr->treeForces[i] = XMFLOAT4(0.0f, 0, 0, 0);
		}
	}
	else
	{
		for (unsigned int i = 0; i < MAX_BONES; i++)
		{
			dataPtr->treeBones[i] = XMFLOAT4X4();
		}

		for (unsigned int i = 0; i < MAX_BONES; i++)
		{
			dataPtr->treeBoneOffsets[i] = XMFLOAT4X4();
		}

		for (unsigned int i = 0; i < MAX_BONES; i++)
		{
			dataPtr->treeBoneHeights[i] = XMFLOAT4(0.0f, 0, 0, 0);
		}

		for (unsigned int i = 0; i < MAX_BONES; i++)
		{
			dataPtr->treeForces[i] = XMFLOAT4(0.0f, 0, 0, 0);
		}

	}

	// Unlock the tessellation constant buffer.
	deviceContext->Unmap(m_objectBuffer, 0);

	// Set the position of the tessellation constant buffer in the hull shader.
	bufferNumber = 3;

	// Now set the constant buffer in the shader with the updated values.
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_objectBuffer);
	deviceContext->CSSetConstantBuffers(bufferNumber, 1, &m_objectBuffer);
	if (m_useTessellation)
	{
		deviceContext->HSSetConstantBuffers(bufferNumber, 1, &m_objectBuffer);
		deviceContext->DSSetConstantBuffers(bufferNumber, 1, &m_objectBuffer);
		deviceContext->GSSetConstantBuffers(bufferNumber, 1, &m_objectBuffer);
	}
	return true;
}

bool Shader::SetShaderParameters(ID3D11DeviceContext* deviceContext, RendererBufferType rendererBuffer)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	RendererBufferType* dataPtr;
	unsigned int bufferNumber;

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_rendererBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	if (FAILED(result))
	{
		return false;
	}

	// Get a pointer to the data in the tessellation constant buffer.
	dataPtr = (RendererBufferType*)mappedResource.pData;

	// Copy the tessellation data into the constant buffer.
	dataPtr->tessellationAmount = rendererBuffer.tessellationAmount;
	dataPtr->displacementStrength = rendererBuffer.displacementStrength;
	dataPtr->emission = rendererBuffer.emission;
	dataPtr->materialId = rendererBuffer.materialId;
	dataPtr->normalStrength = rendererBuffer.normalStrength;
	dataPtr->uvTilingX = rendererBuffer.uvTilingX;
	dataPtr->uvTilingY = rendererBuffer.uvTilingY;
	dataPtr->baseColor = rendererBuffer.baseColor;
	dataPtr->isMetal = rendererBuffer.isMetal;
	dataPtr->mulMetal = rendererBuffer.mulMetal;
	dataPtr->mulRoughness = rendererBuffer.mulRoughness;
	dataPtr->shadowColor = rendererBuffer.shadowColor;
	dataPtr->translucencyColor = rendererBuffer.translucencyColor;
	dataPtr->a = rendererBuffer.a;
	dataPtr->a = rendererBuffer.b;

	// Unlock the tessellation constant buffer.
	deviceContext->Unmap(m_rendererBuffer, 0);

	// Set the position of the tessellation constant buffer in the hull shader.
	bufferNumber = 2;

	// Now set the constant buffer in the shader with the updated values.
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_rendererBuffer);
	deviceContext->PSSetConstantBuffers(bufferNumber, 1, &m_rendererBuffer);
	if (m_useTessellation)
	{
		deviceContext->HSSetConstantBuffers(bufferNumber, 1, &m_rendererBuffer);
		deviceContext->DSSetConstantBuffers(bufferNumber, 1, &m_rendererBuffer);
		deviceContext->GSSetConstantBuffers(bufferNumber, 1, &m_rendererBuffer);
	}
	return true;
}

bool Shader::SetShaderParameters(ID3D11DeviceContext* deviceContext, PostProcessBufferType postProcessBuffer)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	PostProcessBufferType* dataPtr;
	unsigned int bufferNumber;

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_postProcessBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	if (FAILED(result))
	{
		return false;
	}

	// Get a pointer to the data in the tessellation constant buffer.
	dataPtr = (PostProcessBufferType*)mappedResource.pData;

	// Copy the tessellation data into the constant buffer.
	dataPtr->passType = postProcessBuffer.passType;
	dataPtr->passNr = postProcessBuffer.passNr;
	dataPtr->screenResolutionX = postProcessBuffer.screenResolutionX;
	dataPtr->screenResolutionY = postProcessBuffer.screenResolutionY;
	// Unlock the tessellation constant buffer.
	deviceContext->Unmap(m_postProcessBuffer, 0);

	// Set the position of the tessellation constant buffer in the hull shader.
	bufferNumber = 4;

	// Now set the constant buffer in the shader with the updated values.
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_postProcessBuffer);
	deviceContext->PSSetConstantBuffers(bufferNumber, 1, &m_postProcessBuffer);
	if (m_useTessellation)
	{
		deviceContext->HSSetConstantBuffers(bufferNumber, 1, &m_postProcessBuffer);
		deviceContext->DSSetConstantBuffers(bufferNumber, 1, &m_postProcessBuffer);
		deviceContext->GSSetConstantBuffers(bufferNumber, 1, &m_postProcessBuffer);
	}
	return true;
}


bool Shader::SetShaderParameters(ID3D11DeviceContext* deviceContext, float leafStrength, float leafBranchInfluence, float leafInfluence)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	TreeRenderBufferType* dataPtr;
	unsigned int bufferNumber;

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_treeBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	if (FAILED(result))
	{
		return false;
	}

	// Get a pointer to the data in the constant buffer.
	dataPtr = (TreeRenderBufferType*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	dataPtr->foliageInformation = XMFLOAT4(leafStrength, leafBranchInfluence, leafInfluence,0);

	// Unlock the constant buffer.
	deviceContext->Unmap(m_treeBuffer, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 5;

	// Finanly set the constant buffer in the vertex shader with the updated values. //domain shader now
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_treeBuffer);

	return true;
}

string Shader::GetName()
{
	return m_name;
}

bool Shader::IsInitialized()
{
	return m_initialized;
}

void Shader::SetTessellation(bool activate)
{
	m_useTessellation = activate;
}

bool Shader::UsesTessellation()
{
	return m_useTessellation;
}
