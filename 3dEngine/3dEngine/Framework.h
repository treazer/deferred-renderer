#pragma once

#define WIN32_LEAN_AND_MEAN

#include "Engine.h"

class Framework
{
public:
	Framework(void);
	~Framework(void);

	bool Initialize();
	void Run();

private:
	bool CreateDXWindow(LPCWSTR windowTitle, int x, int y, int width, int height);

	LPCWSTR m_applicationName;
	HINSTANCE m_hInstance;
};
