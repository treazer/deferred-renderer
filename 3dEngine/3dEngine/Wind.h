#pragma once

#include <d3d11_1.h>
#include <directxmath.h>
#include "Shader.h"

class Wind
{
public:
	Wind();
	~Wind();
	XMFLOAT4 SampleWindForPosition(XMFLOAT3 position);
};

