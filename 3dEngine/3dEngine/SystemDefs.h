#pragma once

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720
#define WINDOW_POSX 400
#define WINDOW_POSY 100

const float SCREEN_DEPTH = 100.0f;
const float SCREEN_NEAR = 0.05f;
const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = true;
const int SHADOWMAP_RESOLUTION = 4096;
