#pragma once
#include "StandardShader.h"

class TerrainShader :
	public StandardShader
{
public:
	TerrainShader(ID3D11Device * device, HWND hwnd, WCHAR * shaderFileName, bool usesTessellation);
	~TerrainShader();

	bool SetShaderParameters(ID3D11DeviceContext* deviceContext,
		ID3D11ShaderResourceView* albedo,
		ID3D11ShaderResourceView* normal,
		ID3D11ShaderResourceView* displacement,
		ID3D11ShaderResourceView* roughness,
		ID3D11ShaderResourceView* metal,
		ID3D11ShaderResourceView* translucency,
		ID3D11ShaderResourceView* cutout,
		float emission,
		float displacementStrength,
		int materialId);

private:

};

