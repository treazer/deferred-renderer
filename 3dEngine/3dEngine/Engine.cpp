#include "Engine.h"
#include <iostream>
#include "GUtility.h"
#include "DeferredShader.h"
#include "Light.h"
#include "Component.h"

#include "CommonStates.h"
#include "DDSTextureLoader.h"
#include "DirectXHelpers.h"
#include "Effects.h"
#include "GamePad.h"
#include "GeometricPrimitive.h"
//#include "GraphicsMemory.h"
#include "Keyboard.h"
#include "Model.h"
#include "Mouse.h"
#include "PrimitiveBatch.h"
#include "ScreenGrab.h"
#include "SimpleMath.h"
#include "SpriteBatch.h"
#include "SpriteFont.h"
#include "VertexTypes.h"
#include "WICTextureLoader.h"
#include "DepthTexture.h"

#include <sstream>
Engine* Engine::m_instance = NULL;

Engine::Engine(void)
{
	m_graphics = NULL;
	m_resourceManager = NULL;
	m_input = NULL;

	m_sceneList.reserve(4);
	m_currentScene = 0;

	m_deferredShader = 0;
	m_deferredPlane = 0;
	m_renderTargets = 0;
	m_currentFrameStep = 0;
}

Engine::~Engine(void)
{
	if (m_graphics)
	{
		delete m_graphics;
		m_graphics = NULL;
	}

	if (m_resourceManager)
	{
		delete m_resourceManager;
		m_resourceManager = NULL;
	}

	if (m_input)
	{
		delete m_input;
		m_input = NULL;
	}
}

bool Engine::InitializeGraphics(HWND hwnd)
{
	m_graphics = new Graphics();

	return m_graphics->InitializeDX(hwnd);
}

bool Engine::Initialize(HINSTANCE hinstance, HWND hwnd)
{

	m_resourceManager = AssetManager::GetInstance();
	std::cout << "Preloading Shaders" << std::endl;
	m_resourceManager->LoadStandardShader(m_graphics->GetDevice(), hwnd, L"Shaders/standard.fx", true);
	//m_resourceManager->LoadShader(m_graphics->GetDevice(), hwnd, L"Shaders/deferred.fx", false);
	m_resourceManager->LoadShader(m_graphics->GetDevice(), hwnd, L"Shaders/skybox.fx", false);
	m_resourceManager->LoadStandardShader(m_graphics->GetDevice(), hwnd, L"Shaders/tree.fx", true);
	m_resourceManager->LoadStandardShader(m_graphics->GetDevice(), hwnd, L"Shaders/leaf.fx", true);
	//m_resourceManager->LoadShader(m_graphics->GetDevice(), hwnd, L"Shaders/postProcess.fx", false);
	
	/*
	std::cout << "Preloading Textures" << std::endl;
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/leafColor.png");
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/leafNormal.png", false);
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/leafRoughness.png");
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/leafCutout.png");
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/leafDisplacement.png");
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/leafTranslucency.png");

	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/barkColor.png");
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/barkDisp.png");
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/barkMetal.png");
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/barkNormal.png", false);
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/barkRoughness.png");

	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/leaf2_COLOR.png", false);
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/leaf2_NORM.png");
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/leaf2_AO.png");
	m_resourceManager->LoadTexture(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), L"Textures/leaf2_R.png");
	*/
	m_graphics->Initialize();

	m_input = new Input();
	m_input->Initialize(hinstance, hwnd, SCREEN_WIDTH, SCREEN_HEIGHT);
	m_input->Update();

	//initialize scenes
	m_sceneList.reserve(RESERVED_SCENES);
	Scene* scene = new Scene();
	scene->m_graphics = m_graphics;
	m_resourceManager->LoadScene(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), scene, "Assets/Scene.txt");

	scene->Initialize("Testscene", m_graphics);
	std::cout << "Initialized Scene" << std::endl;

	//add scene to scenelist
	m_sceneList.push_back(scene);
	m_currentScene = scene;

	// Initialize the deferred rendering process
	m_deferredShader = new DeferredShader(m_graphics->GetDevice(), m_graphics->GetHwnd(), L"Shaders/deferred.fx");
	m_postProcessShader = new DeferredShader(m_graphics->GetDevice(), m_graphics->GetHwnd(), L"Shaders/postProcess.fx");
	m_deferredPlane = Mesh::Load(m_graphics->GetDevice(), "Meshes/plane.obj");
	m_renderTargets = new RenderTexture[6];
	m_renderTargets[0].Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT / 2, false, DXGI_FORMAT_R16G16B16A16_FLOAT);
	m_renderTargets[1].Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT / 2, false, DXGI_FORMAT_R16G16_FLOAT);
	m_renderTargets[2].Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT / 2, false, DXGI_FORMAT_R16G16B16A16_FLOAT);
	m_renderTargets[3].Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT / 2, false, DXGI_FORMAT_R16G16_FLOAT);
	//velocity:
	m_renderTargets[4].Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT / 2, false, DXGI_FORMAT_R16G16_FLOAT);
	m_renderTargets[5].Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT / 2, false, DXGI_FORMAT_R16G16_FLOAT);

	m_depthTexture = new DepthTexture();
	m_depthTexture->Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT / 2);
	m_depthTexture_2 = new DepthTexture();
	m_depthTexture_2->Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT / 2);
	m_deferredDepthTexture = new DepthTexture();
	m_deferredDepthTexture->Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT);
	m_shadowMap = new DepthTexture();
	m_shadowMap->Initialize(m_graphics->GetDevice(), SHADOWMAP_RESOLUTION, SHADOWMAP_RESOLUTION);
	m_shadowColor = new RenderTexture();
	m_shadowColor->Initialize(m_graphics->GetDevice(), SHADOWMAP_RESOLUTION, SHADOWMAP_RESOLUTION, false, DXGI_FORMAT_R8G8B8A8_UNORM_SRGB);

	m_postProcessTargets = new RenderTexture[2];
	m_postProcessTargets[0].Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT, true, DXGI_FORMAT_R16G16B16A16_FLOAT);
	m_postProcessTargets[1].Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT, true, DXGI_FORMAT_R16G16B16A16_FLOAT);

	m_translucencyTarget = new RenderTexture();
	m_translucencyTarget->Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT, false, DXGI_FORMAT_R8G8B8A8_UNORM_SRGB);

	m_deferredTarget = new RenderTexture();
	m_deferredTarget->Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT, true, DXGI_FORMAT_R16G16B16A16_FLOAT);
	m_deferredShadowTarget = new RenderTexture();
	m_deferredShadowTarget->Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT, true, DXGI_FORMAT_R8G8B8A8_UNORM_SRGB);
	m_deferredLuminanceTarget = new RenderTexture();
	m_deferredLuminanceTarget->Initialize(m_graphics->GetDevice(), SCREEN_WIDTH, SCREEN_HEIGHT, true, DXGI_FORMAT_R16G16B16A16_FLOAT);

	// Load font placeholder 

	std::cout << "Initializing Deferred Renderer" << std::endl;

	targetViewsEmpty[0] = NULL;
	targetViewsEmpty[1] = NULL;
	targetViewsEmpty[2] = NULL;
	targetViewsDeferred[0] = m_deferredTarget->GetRenderTargetView();
	targetViewsDeferred[1] = m_deferredLuminanceTarget->GetRenderTargetView();
	targetViewsDeferred[2] = m_deferredShadowTarget->GetRenderTargetView();
	targetViewsScene[0] = m_renderTargets[0].GetRenderTargetView();
	targetViewsScene[1] = m_renderTargets[1].GetRenderTargetView();
	targetViewsScene[2] = m_renderTargets[4].GetRenderTargetView();
	targetViewsScene2[0] = m_renderTargets[2].GetRenderTargetView();
	targetViewsScene2[1] = m_renderTargets[3].GetRenderTargetView();
	targetViewsScene2[2] = m_renderTargets[5].GetRenderTargetView();
	targetViewsShadow = m_shadowColor->GetRenderTargetView();
	targetViewsTranslucency = m_translucencyTarget->GetRenderTargetView();
	return true;
}

void Engine::Run()
{
	Update();
	Render();
}

void Engine::Update()
{
	m_input->Update();

	// Reload
	if (m_input->IsKeyDown(DIK_O))
	{
		//m_resourceManager->LoadStandardShader(m_graphics->GetDevice(), m_graphics->GetHwnd(), L"Shaders/leaf.fx", true);
		m_resourceManager->LoadStandardShader(m_graphics->GetDevice(), m_graphics->GetHwnd(), L"Shaders/standard.fx", true);
		m_resourceManager->LoadStandardShader(m_graphics->GetDevice(), m_graphics->GetHwnd(), L"Shaders/skybox.fx", false);
		//m_resourceManager->LoadShader(m_graphics->GetDevice(), m_graphics->GetHwnd(), L"Shaders/deferred.fx", false);
		m_deferredShader = new DeferredShader(m_graphics->GetDevice(), m_graphics->GetHwnd(), L"Shaders/deferred.fx");
		m_postProcessShader = new DeferredShader(m_graphics->GetDevice(), m_graphics->GetHwnd(), L"Shaders/postProcess.fx");

		std::cout << "Reload Complete" << std::endl;
	}
	m_currentScene->Update(m_input);

}

void Engine::Render()
{
	m_graphics->EnableZBuffer(true);
	m_graphics->SetViewport(SCREEN_WIDTH, SCREEN_HEIGHT/2);
	if (m_input->IsKeyDown(DIK_SPACE))
	{
		RenderScene();
	}
	m_graphics->SetViewport(SCREEN_WIDTH, SCREEN_HEIGHT);
	//m_graphics->SetRasterStateBackside(true); //temp deactivated for debugging
	//RenderTranslucency(); //temp deactivated for debugging
	m_graphics->EnableZBuffer(true);
	m_graphics->SetRasterStateBackside(false);
	m_graphics->SetViewport(SHADOWMAP_RESOLUTION, SHADOWMAP_RESOLUTION);
	RenderShadowmap();
	m_graphics->SetViewport(SCREEN_WIDTH, SCREEN_HEIGHT);
	m_graphics->SetRasterStateWireframe(false);
	m_graphics->EnableZBuffer(false);
	RenderDeferredPass();
	PostProcessing();
}

void Engine::RenderShadowmap()
{
	m_graphics->EnableZBuffer(true);
	m_graphics->GetDeviceContext()->OMSetRenderTargets(0, NULL, m_shadowMap->GetDepthStencilView()); //&targetViewsShadow
	m_graphics->GetDeviceContext()->ClearDepthStencilView(m_shadowMap->GetDepthStencilView(), D3D11_CLEAR_DEPTH, 1.0f, 0);
	float v = SHADOWMAP_RESOLUTION / SHADOWMAP_RESOLUTION;
	XMStoreFloat4x4(&m_lightOrthoMatrix, XMMatrixOrthographicLH(60.0f*v, 60.0f, 1.0f, 100.0f));
	XMFLOAT3 target = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 up = XMFLOAT3(0.0f, 0.0f, 1.0f);

	XMStoreFloat4x4(&m_lightViewMatrix, XMMatrixLookAtLH(XMLoadFloat3(new XMFLOAT3(13.46904f,17.32476f, -18.32174f)), XMLoadFloat3(&target), XMLoadFloat3(&up)));
	Component* comp = m_currentScene->GetSun()->GetComponents()[0];

	m_currentScene->RenderShadows(m_graphics, m_lightViewMatrix, m_lightOrthoMatrix);
	m_graphics->GetDeviceContext()->OMSetRenderTargets(3, targetViewsEmpty, m_depthTexture->GetDepthStencilView());
}


void Engine::RenderTranslucency()
{
	m_graphics->GetDeviceContext()->OMSetRenderTargets(1, &targetViewsTranslucency, m_depthTexture->GetDepthStencilView());
	m_translucencyTarget->ClearRenderTarget(m_graphics->GetDeviceContext(), m_depthTexture->GetDepthStencilView(), 0.0f, 0.0f, 0.0f, 0.0f);

	m_currentScene->RenderTranslucency(m_graphics);
	m_graphics->GetDeviceContext()->OMSetRenderTargets(3, targetViewsEmpty, m_depthTexture->GetDepthStencilView());

}

void Engine::RenderScene()
{
	// Progressive Renderer
	m_currentFrameStep++;
	if (m_currentFrameStep >= RENDERER_FRAME_STEPS)
		m_currentFrameStep = 0;

	if (m_currentFrameStep == 0)
	{
		m_graphics->GetDeviceContext()->OMSetRenderTargets(3, targetViewsScene, m_depthTexture->GetDepthStencilView());
		m_graphics->GetDeviceContext()->ClearDepthStencilView(m_depthTexture->GetDepthStencilView(), D3D11_CLEAR_DEPTH, 1.0f, 0);
	}
	else
	{
		m_graphics->GetDeviceContext()->OMSetRenderTargets(3, targetViewsScene2, m_depthTexture_2->GetDepthStencilView());
		m_graphics->GetDeviceContext()->ClearDepthStencilView(m_depthTexture_2->GetDepthStencilView(), D3D11_CLEAR_DEPTH, 1.0f, 0);
	}

	
	//m_renderTargets[0].ClearRenderTarget(m_graphics->GetDeviceContext(), m_depthTexture->GetDepthStencilView(), 0.0f, 0.0f, 0.0f, 1.0f);

	// Draw data into buffers
	m_currentScene->Render(m_graphics, m_currentFrameStep, RENDERER_FRAME_STEPS);
	//m_graphics->SetBackgroundZBuffer();
	m_currentScene->RenderSky(m_graphics, m_currentFrameStep, RENDERER_FRAME_STEPS);
	//m_graphics->EnableZBuffer(true);
	m_graphics->GetDeviceContext()->OMSetRenderTargets(3, targetViewsEmpty, m_deferredDepthTexture->GetDepthStencilView());
}

void Engine::PostProcessing()
{
	// Create mipmap of main-rendering for blurring and other effects
	m_graphics->GetDeviceContext()->GenerateMips(m_deferredTarget->GetShaderResourceView());
	//m_graphics->GetDeviceContext()->GenerateMips(m_deferredShadowTarget->GetShaderResourceView());
	m_graphics->GetDeviceContext()->GenerateMips(m_deferredLuminanceTarget->GetShaderResourceView());

	int numberPasses = 8;
	for (int i = 0; i < numberPasses; i++)
	{
		if (i+1 == numberPasses)
			m_graphics->BeginScene(0.0f, 0.0f, 0.0f, 1.0f);
		else
		{
			ID3D11RenderTargetView * views = m_postProcessTargets[(i+1) % 2].GetRenderTargetView();
			m_graphics->GetDeviceContext()->OMSetRenderTargets(1, &views, m_deferredDepthTexture->GetDepthStencilView());
		}
		// Alternate between the two buffers to allow multiple passes
		if (i == 0)
		{
			m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_deferredTarget->GetShaderResourceView(), 10);
		}
		else
		{
			m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_postProcessTargets[i % 2].GetShaderResourceView(), 10);
		}

		PostProcessingPass(i % 2, i);

		if (i + 1 == numberPasses)
			m_graphics->EndScene();
		else
		{
			m_graphics->GetDeviceContext()->OMSetRenderTargets(3, targetViewsEmpty, m_deferredDepthTexture->GetDepthStencilView());
		}
		m_graphics->GetDeviceContext()->GenerateMips(m_postProcessTargets[0].GetShaderResourceView());
		m_graphics->GetDeviceContext()->GenerateMips(m_postProcessTargets[1].GetShaderResourceView());
	}

	//m_renderTargets[0].ClearRenderTarget(m_graphics->GetDeviceContext(), m_depthTexture->GetDepthStencilView(), 0, 0, 0, 1);
	//m_renderTargets[1].ClearRenderTarget(m_graphics->GetDeviceContext(), m_depthTexture->GetDepthStencilView(), 0, 0, 0, 1);
}

void Engine::PostProcessingPass(int pass, int passNr)
{
	Shader::PostProcessBufferType postProcessBuffer;
	postProcessBuffer.passType = (float)pass;
	postProcessBuffer.passNr = (float)passNr;
	postProcessBuffer.screenResolutionX = SCREEN_WIDTH;
	postProcessBuffer.screenResolutionY = SCREEN_HEIGHT;

	XMFLOAT4X4 viewMatrix;
	XMFLOAT4X4 orthoMatrix;
	XMFLOAT4X4 worldMatrix;
	float v = SCREEN_WIDTH / SCREEN_HEIGHT;
	XMStoreFloat4x4(&orthoMatrix, XMMatrixOrthographicLH(2.0f*v, 2.0f, 0.01f, 100.0f));
	XMFLOAT3 target = XMFLOAT3(0.0f, 0.0f, 1.0f);

	XMFLOAT3 up = XMFLOAT3(0.0f, 1.0f, 0.0f);
	XMStoreFloat4x4(&viewMatrix, XMMatrixLookAtLH(XMLoadFloat3(new XMFLOAT3(0, 0.0f, -20)), XMLoadFloat3(&target), XMLoadFloat3(&up)));

	XMStoreFloat4x4(&worldMatrix, XMMatrixIdentity());
	worldMatrix._11 = 1;
	worldMatrix._12 = 0;
	worldMatrix._13 = 0;
	worldMatrix._14 = 0;
	worldMatrix._21 = 0;
	worldMatrix._22 = 0;
	worldMatrix._23 = -1;
	worldMatrix._24 = 0;
	worldMatrix._31 = 0;
	worldMatrix._32 = 1;
	worldMatrix._33 = 0;
	worldMatrix._34 = 0;
	worldMatrix._41 = 0;
	worldMatrix._42 = 0;
	worldMatrix._43 = 0;
	worldMatrix._44 = 1;

	// Define necessary resources for gbuffer rendering

	Component* comp = m_currentScene->GetSun()->GetComponents()[0];
	Light* sunlight = (Light *)comp;

	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), postProcessBuffer);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), worldMatrix, m_currentScene->GetActiveCamera()->GetViewMatrix(), m_currentScene->GetActiveCamera()->GetProjectionMatrix(), m_currentScene->GetActiveCamera()->GetEyeVector(), XMFLOAT4(0, 0, 0, 0), m_currentScene->GetActiveCamera()->GetViewMatrixOld(), m_currentScene->GetActiveCamera()->GetProjectionMatrixOld(),0,0);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), sunlight->GetDirection(), XMFLOAT4(0, 0, 0, 0), sunlight->GetColor(), 1.0f, m_currentScene->GetActiveCamera()->GetEyeVector(), m_lightViewMatrix, m_lightOrthoMatrix, worldMatrix, viewMatrix, orthoMatrix, m_currentScene->GetActiveCamera()->GetProjectionMatrix(), m_currentScene->GetActiveCamera()->GetViewMatrix(), m_currentFrameStep, RENDERER_FRAME_STEPS);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_renderTargets[0].GetShaderResourceView(), 0);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_renderTargets[1].GetShaderResourceView(), 1);
	//m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_renderTargets[4].GetShaderResourceView(), 13); //velocity
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_depthTexture->GetShaderResourceView(), 3);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_currentScene->GetEnvironmentMap(), 4);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_shadowMap->GetShaderResourceView(), 5);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_shadowColor->GetShaderResourceView(), 6);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_translucencyTarget->GetShaderResourceView(), 7);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_currentScene->GetIrradianceMap(), 8);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_deferredTarget->GetShaderResourceView(), 9);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_deferredShadowTarget->GetShaderResourceView(), 11);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_deferredLuminanceTarget->GetShaderResourceView(), 12);
	m_graphics->EnableAlphaBlending(false);
	m_graphics->EnableZBuffer(false);
	m_deferredPlane->Render(m_graphics->GetDeviceContext(), m_postProcessShader);

	ID3D11ShaderResourceView* nullSRV[1];
	nullSRV[0] = nullptr;
	m_graphics->GetDeviceContext()->PSSetShaderResources(0, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(1, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(2, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(3, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(4, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(5, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(6, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(7, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(8, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(9, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(10, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(11, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(12, 1, nullSRV);
}

void Engine::RenderDeferredPass()
{
	//m_deferredRenderPassData.viewMatrix = 
	XMFLOAT4X4 viewMatrix;
	XMFLOAT4X4 orthoMatrix;
	XMFLOAT4X4 worldMatrix;
	float v = SCREEN_WIDTH / SCREEN_HEIGHT;
	XMStoreFloat4x4(&m_deferredRenderPassData.orthoMatrix, XMMatrixOrthographicLH(2.0f*v, 2.0f, 0.01f, 100.0f));
	XMFLOAT3 target = XMFLOAT3(0.0f, 0.0f, 1.0f);

	XMFLOAT3 up = XMFLOAT3(0.0f, 1.0f, 0.0f);
	XMStoreFloat4x4(&m_deferredRenderPassData.viewMatrix, XMMatrixLookAtLH(XMLoadFloat3(new XMFLOAT3(0, 0.0f, -20)), XMLoadFloat3(&target), XMLoadFloat3(&up)));

	XMStoreFloat4x4(&m_deferredRenderPassData.worldMatrix, XMMatrixIdentity());
	m_deferredRenderPassData.worldMatrix._11 = 1;
	m_deferredRenderPassData.worldMatrix._12 = 0;
	m_deferredRenderPassData.worldMatrix._13 = 0;
	m_deferredRenderPassData.worldMatrix._14 = 0;
	m_deferredRenderPassData.worldMatrix._21 = 0;
	m_deferredRenderPassData.worldMatrix._22 = 0;
	m_deferredRenderPassData.worldMatrix._23 = -1;
	m_deferredRenderPassData.worldMatrix._24 = 0;
	m_deferredRenderPassData.worldMatrix._31 = 0;
	m_deferredRenderPassData.worldMatrix._32 = 1;
	m_deferredRenderPassData.worldMatrix._33 = 0;
	m_deferredRenderPassData.worldMatrix._34 = 0;
	m_deferredRenderPassData.worldMatrix._41 = 0;
	m_deferredRenderPassData.worldMatrix._42 = 0;
	m_deferredRenderPassData.worldMatrix._43 = 0;
	m_deferredRenderPassData.worldMatrix._44 = 1;

	// Define necessary resources for deferred rendering

	Component* comp = m_currentScene->GetSun()->GetComponents()[0];
	Light* sunlight = (Light *)comp;

	m_graphics->GetDeviceContext()->OMSetRenderTargets(3, targetViewsDeferred, m_deferredDepthTexture->GetDepthStencilView());

	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_deferredRenderPassData.worldMatrix, m_currentScene->GetActiveCamera()->GetViewMatrix(), m_currentScene->GetActiveCamera()->GetProjectionMatrix(), m_currentScene->GetActiveCamera()->GetEyeVector(), XMFLOAT4(0,0,0,0), m_currentScene->GetActiveCamera()->GetViewMatrixOld(), m_currentScene->GetActiveCamera()->GetProjectionMatrixOld(),0,0);
	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), sunlight->GetDirection(), XMFLOAT4(0, 0, 0, 0), sunlight->GetColor() , 1.0f, m_currentScene->GetActiveCamera()->GetEyeVector(), m_lightViewMatrix, m_lightOrthoMatrix, m_deferredRenderPassData.worldMatrix, m_deferredRenderPassData.viewMatrix, m_deferredRenderPassData.orthoMatrix, m_currentScene->GetActiveCamera()->GetProjectionMatrix(), m_currentScene->GetActiveCamera()->GetViewMatrix(), m_currentFrameStep, RENDERER_FRAME_STEPS);
	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_renderTargets[0].GetShaderResourceView(), 0);
	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_renderTargets[1].GetShaderResourceView(), 1);
	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_renderTargets[2].GetShaderResourceView(), 9);
	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_renderTargets[3].GetShaderResourceView(), 10);
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_renderTargets[4].GetShaderResourceView(), 12); //velocity
	m_postProcessShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_renderTargets[5].GetShaderResourceView(), 13); //velocity
	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_depthTexture->GetShaderResourceView(), 3);
	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_depthTexture_2->GetShaderResourceView(), 11);
	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_currentScene->GetEnvironmentMap(), 4);
	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_shadowMap->GetShaderResourceView(), 5);
	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_shadowColor->GetShaderResourceView(), 6);
	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_translucencyTarget->GetShaderResourceView(), 7);
	m_deferredShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_currentScene->GetIrradianceMap(), 8);
	m_graphics->EnableAlphaBlending(false);
	m_graphics->EnableZBuffer(false);
	m_deferredPlane->Render(m_graphics->GetDeviceContext(), m_deferredShader);

	ID3D11ShaderResourceView* nullSRV[1];
	nullSRV[0] = nullptr;
	m_graphics->GetDeviceContext()->PSSetShaderResources(0, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(1, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(3, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(4, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(5, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(6, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(7, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(8, 1, nullSRV);
	m_graphics->GetDeviceContext()->PSSetShaderResources(9, 1, nullSRV);

	m_graphics->GetDeviceContext()->OMSetRenderTargets(3, targetViewsEmpty, m_deferredDepthTexture->GetDepthStencilView());
}

Engine* Engine::GetEngine()
{
	if (m_instance == NULL)
	{
		m_instance = new Engine();
	}

	return m_instance;
}

void Engine::Release()
{
	if (m_instance)
	{
		delete m_instance;
		m_instance = NULL;
	}
}

Graphics* Engine::GetGraphics()
{
	return m_graphics;
}

Input* Engine::GetInput()
{
	return m_input;
}

Scene * Engine::GetCurrentScene()
{
	return m_currentScene;
}

scene_list Engine::GetScenes()
{
	return m_sceneList;
}
