#pragma once
#include <Windows.h>

class Timer
{
public:
	static void Update();

	static float GetDeltaTime();
	static float GetTimeSinceStart();
private:
	static __int64 m_prevFrame;
	static __int64 m_currentFrame;
	static __int64 m_freq;
	static float m_deltaTime;
	static float m_timeSinceStart;
};
