#include "Camera.h"
#include <string>
#include "GameObject.h"
#include "Scene.h"
Camera::Camera(void)
{
	m_viewMatrix = XMFLOAT4X4();
	m_projectionMatrix = XMFLOAT4X4();
	m_viewMatrixOld = XMFLOAT4X4();
	m_projectionMatrixOld = XMFLOAT4X4();
	m_orthoMatrix = XMFLOAT4X4();
	m_up = XMFLOAT3(0, 0, 0);
	mTarget = XMFLOAT3(0,0,0);
	m_lastMousePosition = XMVECTOR();

	m_position = XMVECTOR();
	m_position = XMVectorSet(0, 0, 0, 0);
	m_rotation = XMVECTOR();
	m_rotation = XMVectorSet(0, 0, 0, 0);

	rotationX = 0;
	rotationY = 0;
	smoothRotationX = 0;
	smoothRotationY = 0;
}

Camera::~Camera(void)
{
}

void Camera::Initialize(GameObject* gameObject)
{
	m_position = XMVectorSet(0, 0, 0, 0);
	m_rotation = XMVectorSet(0, 0, 0, 0);

	Component::m_gameObject = gameObject;
	mTarget = XMFLOAT3(20.0f, 0.0f, 1.0f);
	XMFLOAT3 * up = new XMFLOAT3(0, 1, 0);
	m_up = XMFLOAT3(0, 1, 0);
	XMStoreFloat4x4(&m_viewMatrix, XMMatrixLookAtLH(XMLoadFloat3(&m_gameObject->GetTransform()->GetPosition()), XMLoadFloat3(&mTarget), XMLoadFloat3(&this->CalcUp())));
}

void Camera::InitializeViewMatrix()
{
	XMStoreFloat4x4(&m_viewMatrix, XMMatrixLookAtLH(XMLoadFloat3(&m_gameObject->GetTransform()->GetPosition()), XMLoadFloat3(&mTarget), XMLoadFloat3(&this->CalcUp())));
}

void Camera::Rotate(XMFLOAT3 axis, float degrees)
{
	
	if (XMVector3Equal(GMathFV(axis), XMVectorZero()) ||
		degrees == 0.0f)
		return;

	// rotate vectors
	XMFLOAT3 look_at_target = GMathVF(GMathFV(mTarget) - GMathFV(m_gameObject->GetTransform()->GetPosition()));
	XMFLOAT3 look_at_up = GMathVF(GMathFV(m_up) - GMathFV(m_gameObject->GetTransform()->GetPosition()));
	look_at_target = GMathVF(XMVector3Transform(GMathFV(look_at_target), XMMatrixRotationAxis(GMathFV(axis), XMConvertToRadians(degrees))));
	look_at_up = GMathVF(XMVector3Transform(GMathFV(look_at_up), XMMatrixRotationAxis(GMathFV(axis), XMConvertToRadians(degrees))));

	// restore vectors's end points mTarget and mUp from new rotated vectors
	mTarget = GMathVF(GMathFV(m_gameObject->GetTransform()->GetPosition()) + GMathFV(look_at_target));
	m_up = GMathVF(GMathFV(m_gameObject->GetTransform()->GetPosition()) + GMathFV(look_at_up));

	InitializeViewMatrix();
	
}

string Camera::GetName()
{
	return Component::m_name;
}

GameObject * Camera::GetGameObject()
{
	return m_gameObject;
}

void Camera::Update(Scene * scene)
{
}

void Camera::InitializeOthoMatrix(int screenWidth, int screenHeight, float screenNear, float screenFar)
{
	XMStoreFloat4x4(&m_orthoMatrix, XMMatrixOrthographicLH((float)screenWidth, (float)screenHeight, screenNear, screenFar));
}

void Camera::InitializeProjectionMatrix(float fov, float screenAspect, float screenNear, float screenFar)
{
	XMStoreFloat4x4(&m_projectionMatrix, XMMatrixPerspectiveFovLH(fov, screenAspect, screenNear, screenFar));
}

void Camera::Update(Input* input)
{
	m_viewMatrixOld = GetViewMatrix();
	m_projectionMatrixOld = m_projectionMatrix;

	XMVECTOR position = GMathFV(m_gameObject->GetTransform()->GetPosition());
	m_position = position;

	POINT point;
	GetCursorPos(&point);
	int x = 0;
	int y = 0;
	input->GetMouseChange(x, y);
	float xf = (float)x*mouseSpeed;
	float yf = (float)y*mouseSpeed;

	rotationX += xf;
	rotationY += yf;

	smoothRotationX += (rotationX-smoothRotationX)*0.45f;
	smoothRotationY += (rotationY-smoothRotationY)*0.45f;

	//load matrices
	XMMATRIX rotationMatrix = XMMatrixRotationRollPitchYaw(smoothRotationY, smoothRotationX, 0);
	XMMATRIX xmViewMatrix = XMMatrixIdentity();
	XMFLOAT4X4 fViewMatrix;

	//calculate
	rotationMatrix = XMMatrixMultiply(xmViewMatrix, rotationMatrix);
	
	XMVECTOR movement = { 0,0,0 };
	float speed = movementSpeed;
	if (input->IsKeyDown(DIK_LSHIFT))
		speed = movementSpeedSlow;
	if (input->IsKeyDown(DIK_W))
		movement = { m_viewMatrix._31*speed, m_viewMatrix._32*speed, m_viewMatrix._33*speed };
	if (input->IsKeyDown(DIK_S))
		movement = { -m_viewMatrix._31*speed, -m_viewMatrix._32*speed, -m_viewMatrix._33*speed };
	if (input->IsKeyDown(DIK_D))
		movement = { m_viewMatrix._11*speed, m_viewMatrix._12*speed, m_viewMatrix._13*speed };
	if (input->IsKeyDown(DIK_A))
		movement = { -m_viewMatrix._11*speed, -m_viewMatrix._12*speed, -m_viewMatrix._13*speed };

	m_position += movement;
	XMMATRIX translationMatrix = XMMatrixTranslationFromVector(m_position);
	rotationMatrix = XMMatrixMultiply(rotationMatrix, translationMatrix);

	//store matrices
	XMStoreFloat4x4(&m_viewMatrix, rotationMatrix);

	m_gameObject->GetTransform()->SetMatrix(m_viewMatrix);
	
}

void Camera::Render(Scene * scene)
{
}

XMVECTOR Camera::GetPosition()
{
	return m_position;
}

XMFLOAT3 Camera::GetPositionVector()
{
	XMFLOAT3 float3;
	XMStoreFloat3(&float3, m_position);
	return float3;
}

XMVECTOR Camera::GetRotation()
{
	return m_rotation;
}

XMFLOAT4X4 Camera::GetViewMatrix()
{
	XMFLOAT4X4 matrix;
	XMStoreFloat4x4(&matrix, XMMatrixInverse(nullptr, XMLoadFloat4x4(&m_gameObject->GetTransform()->GetMatrix())));
	return matrix;
}

XMFLOAT4X4 Camera::GetProjectionMatrix()
{
	return m_projectionMatrix;
}

XMFLOAT4X4 Camera::GetProjectionMatrixOld()
{
	return m_projectionMatrixOld;
}

XMFLOAT4X4 Camera::GetOrthoMatrix()
{
	return m_orthoMatrix;
}

XMFLOAT4 Camera::GetEyeVector()
{
	XMVECTOR vec = XMVector3Transform(m_position, XMLoadFloat4x4(&m_viewMatrix));

	XMFLOAT4 float4;
		vec = m_position;
	XMStoreFloat4(&float4, vec);
	return float4;
}

XMFLOAT4X4 Camera::GetViewMatrixOld()
{
	return m_viewMatrixOld;
}

void Camera::SetPosition(float x, float y, float z)
{
	m_position = XMVectorSet(x, y, z, 0);
	
}

void Camera::SetRotation(float x, float y, float z)
{
	m_rotation = XMVectorSet(x * (float)XM_PI / 180.0f, y * (float)XM_PI / 180.0f, z * (float)XM_PI / 180.0f, 0);
}

XMFLOAT3 Camera::CalcUp()
{
	return GMathVF(GMathFV(m_up) - GMathFV(m_gameObject->GetTransform()->GetPosition()));
}
