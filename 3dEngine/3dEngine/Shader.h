#pragma once
//#include <d3d11_1.h>
#include <d3d11.h>
#include <directxmath.h>
#include <fstream>
#include <string>

#define MAX_BONES 256

using namespace std;

using namespace DirectX;

class Mesh;
class Shader
{
public:

	struct PostProcessBufferType
	{
		float passType;
		float passNr;
		float screenResolutionX;
		float screenResolutionY;
		float d;
		float e;
		float f;
		float g;
	};

	struct RendererBufferType
	{
		XMFLOAT4 baseColor;
		XMFLOAT4 shadowColor;
		XMFLOAT4 translucencyColor;
		float tessellationAmount;
		float emission;
		float displacementStrength;
		float materialId;
		float normalStrength;
		float uvTilingX;
		float uvTilingY;
		float isMetal;
		float mulRoughness;
		float mulMetal;
		float a;
		float b;
	};

	struct TreeBufferType
	{
		XMFLOAT4 worldPosition;
		XMFLOAT4 leafDimension; //size x,y + variation x,y
		XMFLOAT4 orientationFlattening;
		XMFLOAT4 leafGravity;
		float numLeaves;
		float numVertices;
		float minimumDistanceLeaves;
		float distancePadding;
		float leafStrength;
		float leafBranchInfluence;
		float leafStiffness;
		float doubleSided;
		float staticBranches;
		float branchStrength;
		float b;
		float c;
	};

protected:

	struct ObjectBufferType
	{
		XMFLOAT4X4 treeBones[MAX_BONES];
		XMFLOAT4X4 treeBoneOffsets[MAX_BONES];
		XMFLOAT4 treeBoneHeights[MAX_BONES];
		XMFLOAT4 treeForces[MAX_BONES];
		XMFLOAT4 customValues;
	};

	struct FrameDataBufferType
	{
		XMMATRIX worldMatrix;
		XMMATRIX viewMatrix;
		XMMATRIX projectionMatrix;
		XMMATRIX viewMatrixOld;
		XMMATRIX projectionMatrixOld;
		XMFLOAT4 eyePosition;
		XMFLOAT4 wind;
		XMFLOAT4 time;
		XMFLOAT4 reconstructionValues;
	};

	__declspec(align(16))
	struct DeferredBufferType
	{
		XMFLOAT4X4 lightViewMatrix;
		XMFLOAT4X4 lightOrthoMatrix;
		XMMATRIX worldMatrixDeferred;
		XMMATRIX viewMatrixDeferred;
		XMMATRIX projectionMatrixDeferred;
		XMMATRIX inverseProjection;
		XMMATRIX inverseView;
		XMFLOAT4 color;
		XMFLOAT4 direction;
		XMFLOAT4 position;
		XMFLOAT4 eye;
		XMFLOAT4 reconstructionValues;
		
	};

	struct TreeRenderBufferType
	{
		XMFLOAT4 foliageInformation;
	};

public:
	Shader(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName, bool usesTessellation);
	virtual ~Shader(void);

	virtual void Begin(ID3D11DeviceContext* deviceContext);
	virtual void BeginShadow(ID3D11DeviceContext* deviceContext);
	virtual void BeginTranslucency(ID3D11DeviceContext* deviceContext);
	virtual void End(ID3D11DeviceContext* deviceContext);

	bool SetShaderParameters(ID3D11DeviceContext* deviceContext, ID3D11ShaderResourceView* texture, int slot);
	//bool SetShaderParameters(ID3D11DeviceContext* deviceContext, XMFLOAT4X4 worldMatrix, XMFLOAT4X4 viewMatrix, XMFLOAT4X4 projectionMatrix, XMFLOAT4 eyePosition, float normalStrength, float uvTilingX, float uvTilingY, Mesh* mesh, float tessellationAmount = 0, float displacementStrength = 0);
	//bool SetShaderParameters(ID3D11DeviceContext* deviceContext, float normalStrength, float uvTilingX, float uvTilingY, Mesh* mesh, float tessellationAmount, float displacementStrength);
	bool SetShaderParameters(ID3D11DeviceContext* deviceContext, RendererBufferType rendererBuffer);
	bool SetShaderParameters(ID3D11DeviceContext* deviceContext, XMFLOAT4X4 worldMatrix, XMFLOAT4X4 viewMatrix, XMFLOAT4X4 projectionMatrix, XMFLOAT4 eyePosition, XMFLOAT4 wind, XMFLOAT4X4 viewMatrixOld, XMFLOAT4X4 projectionMatrixOld, int currentFrame, int frameSteps);
	bool SetShaderParameters(ID3D11DeviceContext* deviceContext, Mesh* mesh);
	bool SetShaderParameters(ID3D11DeviceContext* deviceContext, float leafStrength, float leafBranchInfluence, float leafInfluence);
	//bool SetShaderParameters(ID3D11DeviceContext* deviceContext, TreeBufferType treeBuffer);
	bool SetShaderParameters(ID3D11DeviceContext* deviceContext, PostProcessBufferType postProcessBuffer);
	string GetName();
	bool IsInitialized();
	void SetTessellation(bool activate);
	bool UsesTessellation();
	virtual bool Initialize(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName, bool usesTessellation);
protected:
	//virtual bool Initialize(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName, bool usesTessellation);

	bool m_initialized;
	bool m_useTessellation;
	ID3D11Buffer* m_rendererBuffer;
	ID3D11Buffer* m_objectBuffer;
private:
	bool InitializeShader(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName, bool usesTessellation);
	void OutputShadeErrorMessage(ID3D10Blob* errorMessage, HWND hwnd, WCHAR* shaderFileName);

	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11PixelShader* m_pixelShadowShader;
	ID3D11PixelShader* m_pixelTranslucencyShader;
	ID3D11HullShader* m_hullShader;
	ID3D11DomainShader* m_domainShader;
	ID3D11InputLayout* m_layout;
	ID3D11Buffer* m_frameDataBuffer;
	ID3D11Buffer* m_postProcessBuffer;
	ID3D11Buffer* m_treeBuffer;
	string m_name;
	
};

