#include "Renderer.h"
#include <iostream>
#include "GameObject.h"
#include "Texture.h"
#include "Mesh.h"
#include "AssetManager.h"
#include <iostream>
#include "Foliage.h"
#include "Scene.h"
Renderer::Renderer()
{
	m_gameObject = 0;
	m_shader = 0;
	m_texture = 0;
	m_mesh = 0;
	m_materialAttributes.displacementStrength = 0.0001f;
	m_materialAttributes.tessellationAmount = 1.0f;
	m_normalStrength = 1;
	m_materialAttributes.a = 43.0f;
	m_materialAttributes.b = 44.0f;
	m_materialAttributes.uvTilingX = 1.0f;
	m_materialAttributes.uvTilingY= 1.0f;
	m_materialAttributes.emission = 0;
	m_materialAttributes.normalStrength = 1.0f;
	m_materialAttributes.materialId = 0;
	m_materialAttributes.isMetal = 0.012f;
	m_materialAttributes.translucencyColor = XMFLOAT4(0, 0, 0, 1);
}


Renderer::~Renderer()
{
}

void Renderer::Initialize(GameObject * gameObject)
{
	m_gameObject = gameObject;
	SetAlbedo(AssetManager::GetInstance()->GetTextureByName((char*)"default"), XMFLOAT4(1, 1, 1, 1));
}

void Renderer::SetShader(StandardShader * shader)
{
	m_shader = shader;
}

void Renderer::SetGameObject(GameObject * object)
{
	m_gameObject = object;
}

void Renderer::SetTexture(Texture * texture)
{
	m_texture = texture;
}

void Renderer::SetMesh(Mesh * mesh)
{
	m_mesh = mesh;
}

void Renderer::Render(Scene * scene)
{
}

void Renderer::Render(Graphics* graphics, XMFLOAT4X4 viewMatrix, XMFLOAT4X4 projectionMatrix, XMFLOAT4 eyePosition, bool shadowOnly, bool translucencyOnly)
{
	GetShader()->SetShaderParameters(graphics->GetDeviceContext(), m_materialAttributes);
	//Shader
	//m_shader->SetShaderParameters(graphics->GetDeviceContext(), m_texture->GetTexture(), 0);

	ID3D11ShaderResourceView * norm = (m_normal != nullptr) ? m_normal->GetTexture() : m_albedo->GetTexture();
	ID3D11ShaderResourceView * disp = (m_displacement != nullptr) ? m_displacement->GetTexture() : m_albedo->GetTexture();
	ID3D11ShaderResourceView * rough = (m_roughness != nullptr) ? m_roughness->GetTexture() : m_albedo->GetTexture();
	ID3D11ShaderResourceView * metal = (m_metal != nullptr) ? m_metal->GetTexture() : m_albedo->GetTexture();
	ID3D11ShaderResourceView * trans = (m_translucency != nullptr) ? m_translucency->GetTexture() : m_albedo->GetTexture();
	ID3D11ShaderResourceView * cut = (m_cutout != nullptr) ? m_cutout->GetTexture() : m_albedo->GetTexture();
	ID3D11ShaderResourceView * light = (m_lightmap != nullptr) ? m_lightmap->GetTexture() : m_albedo->GetTexture();
	
	if (m_albedo->GetTexture() == nullptr) return;
	if (norm == nullptr) return;
	if (disp == nullptr) return;
	if (rough == nullptr) return;
	if (metal == nullptr) return;
	if (trans == nullptr) return;
	if (cut == nullptr) return;
	if (light == nullptr) return;
	if (m_mesh == nullptr) return;

	GetShader()->SetShaderParameters(graphics->GetDeviceContext(), m_albedo->GetTexture(), norm, disp, rough, metal, trans, cut, light, m_emission, 1);
	//Mesh
	m_mesh->Render(graphics->GetDeviceContext(), m_shader, shadowOnly, translucencyOnly);

}

StandardShader * Renderer::GetShader()
{
	return m_shader;
}

Mesh * Renderer::GetMesh()
{
	return m_mesh;
}

string Renderer::GetName()
{
	return Component::m_name;
}

Texture * Renderer::GetTexture()
{
	return m_texture;
}

GameObject * Renderer::GetGameObject()
{
	return m_gameObject;
}

float Renderer::GetTessellation()
{
	return m_materialAttributes.tessellationAmount;
}

float Renderer::GetDisplacementStrength()
{
	return m_materialAttributes.displacementStrength;
}

float Renderer::GetNormalStrength()
{
	return m_normalStrength;
}

float Renderer::GetUVTilingX()
{
	return m_materialAttributes.uvTilingX;
}

float Renderer::GetUVTilingY()
{
	return m_materialAttributes.uvTilingY;
}

void Renderer::Update(Scene * scene)
{
}

void Renderer::SetAlbedo(Texture * texture, XMFLOAT4 color)
{
	m_albedo = texture;
	m_materialAttributes.baseColor = color;
}

void Renderer::SetNormal(Texture * texture)
{
	m_normal = texture;
}

void Renderer::SetRoughness(Texture * texture, float multiplier)
{
	m_roughness = texture;
	m_materialAttributes.mulRoughness = multiplier;
}

void Renderer::SetMetal(Texture * texture, float multiplier)
{
	m_metal = texture;
	m_materialAttributes.mulMetal = multiplier;
}

void Renderer::SetTranslucency(Texture * texture)
{
	m_translucency = texture;
}

void Renderer::SetCutout(Texture * texture)
{
	m_cutout = texture;
}

void Renderer::SetDisplacement(Texture * texture)
{
	m_displacement = texture;
}

void Renderer::SetDisplacementStrength(float strength)
{
	m_materialAttributes.displacementStrength = strength;
}

void Renderer::SetTessellation(float tessellation)
{
	m_materialAttributes.tessellationAmount = tessellation;
}

void Renderer::SetEmission(float emission)
{
	m_emission = emission;
}

void Renderer::SetMaterialId(int id)
{
	m_materialAttributes.materialId = (float)id;
}

void Renderer::SetNormalStrength(float strength)
{
	m_normalStrength = strength;
}

void Renderer::SetUVTiling(float x, float y)
{
	m_materialAttributes.uvTilingX = x;
	m_materialAttributes.uvTilingY = y;
}

void Renderer::SetLightmap(Texture * texture)
{
	m_lightmap = texture;
}

void Renderer::DefineMetal(bool metal)
{
	if (metal)
		m_materialAttributes.isMetal = 1.0;
	else
		m_materialAttributes.isMetal = 0.0;
}

void Renderer::SetTranslucencyColor(XMFLOAT4 color)
{
	m_materialAttributes.translucencyColor = color;
}
