#pragma once

#include "Shader.h"

#include <d3d11_1.h>
#include <directxmath.h>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing fla
#include <vector>
#include <map>

#pragma comment(lib, "assimp.lib")

class Scene;
class Mesh
{
public:
	struct VertexType
	{
		XMFLOAT4 position;
		XMFLOAT4 color;
		XMFLOAT4 uv;
		XMFLOAT4 normal;
		XMFLOAT4 tangent;
		XMFLOAT4 bitangent;
		XMFLOAT4 boneWeights;
		XMFLOAT4 boneIDs;
		
	};

	struct BoneInfoData
	{
		XMFLOAT4X4 BoneOffset;
		XMFLOAT4X4 FinalTransformation;
		XMFLOAT4 nodePosition;
		XMFLOAT4 nodeRotation;
		XMFLOAT4X4 BoneTransform;
		XMFLOAT4 force;
		XMFLOAT4 boneDirection;
		int parent = -1;
		float strength;
		float height;
		int level;
		int isEnd;
		int active;
		int replacedBy;
		float branchLength;
	};

	struct VertexBoneData
	{
		int IDs[4];
		float Weights[4];
		void AddBoneData(unsigned int BoneID, float Weight);
	};


	Mesh(void);
	~Mesh(void);

	bool Initialize(ID3D11Device* device, float size, bool writeable = false);
	void Render(ID3D11DeviceContext* deviceContext, Shader* shader, bool shadowOnly = false, bool translucencyOnly = false);
	static Mesh * Load(ID3D11Device* device, string fileName, bool flippedAxis = false);
	bool CreateBuffers(ID3D11Device* device, bool writeable, int numIndices, int numVertices);
	bool HasBones();
	VertexType* GetVertices();
	std::vector< unsigned long > GetIndices();
	ID3D11Buffer* GetVertexBuffer();
	int GetIndexCount();
	void SetIndexCount(int indeces);
	int GetVertexCount();
	void SetVertexCount(int vertices);
	void SetVertices(VertexType* m_vertices);
	void SetIndices(std::vector< unsigned long > indices);
	string GetName();
	std::map< std::string, unsigned int> GetBoneMap();
	void LoadBones(const aiMesh* pMesh);
	static XMFLOAT4X4 CreateDirectXMatrix(aiMatrix4x4* matrix);
	void LoadNodeHierarchy(aiNode* pNode, int parent);
	void ReduceBones();
	void DefineBoneDirection();
	std::vector<BoneInfoData>* Mesh::GetBoneInfo();
	void CalculateBonePositions();

	Assimp::Importer _importer;
	VertexType* m_vertices;

private:
	ID3D11Buffer* m_vertexBuffer;
	ID3D11Buffer* m_indexBuffer;
	int m_vertexCount;
	int m_indexCount;
	string m_name;
	const aiScene * meshScene;

	std::vector< unsigned long > m_indices;
	std::vector< XMFLOAT3 > m_normals;
	std::vector< XMFLOAT2 > m_uvs;
	std::vector< XMFLOAT3 > m_tangents;
	std::vector< XMFLOAT3 > m_bitangents;
	std::vector< XMFLOAT4 > m_color1;
	std::vector< XMFLOAT4 > m_color2;
	XMFLOAT4X4 m_globalInverseTransform;

	//bones
	std::vector<VertexBoneData> m_bones;
	std::vector<BoneInfoData> m_boneInfo;
	std::map< std::string, unsigned int> m_boneMap;
	bool m_hasBones;
	int m_numBones;
};
