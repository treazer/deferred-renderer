#include "ObjectPreview.h"
#include "Scene.h"


ObjectPreview::ObjectPreview()
{
}


ObjectPreview::~ObjectPreview()
{
}

void ObjectPreview::Initialize(GameObject* gameObject)
{
	Component::m_gameObject = gameObject;
}

string ObjectPreview::GetName()
{
	return Component::m_name;
}

GameObject * ObjectPreview::GetGameObject()
{
	return m_gameObject;
}

void ObjectPreview::Update(Scene * scene)
{
	float speed = 0.03f;
	float movementSpeedSlow = 0.005f;
	if (scene->GetInput()->IsKeyDown(DIK_LSHIFT))
		speed = movementSpeedSlow;
	if (scene->GetInput()->IsKeyDown(DIK_T))
		m_gameObject->GetTransform()->RotateX(speed);
	if (scene->GetInput()->IsKeyDown(DIK_G))
		m_gameObject->GetTransform()->RotateX(-speed);
	if (scene->GetInput()->IsKeyDown(DIK_H))
		m_gameObject->GetTransform()->RotateY(speed);
	if (scene->GetInput()->IsKeyDown(DIK_F))
		m_gameObject->GetTransform()->RotateY(-speed);
}

void ObjectPreview::Update(Input* input)
{
	/*
	if (input->IsKeyDown(DIK_LSHIFT))
		speed = movementSpeedSlow;
	if (input->IsKeyDown(DIK_W))
		movement = { m_viewMatrix._31*speed, m_viewMatrix._32*speed, m_viewMatrix._33*speed };
	if (input->IsKeyDown(DIK_S))
		movement = { -m_viewMatrix._31*speed, -m_viewMatrix._32*speed, -m_viewMatrix._33*speed };
	if (input->IsKeyDown(DIK_D))
		movement = { m_viewMatrix._11*speed, m_viewMatrix._12*speed, m_viewMatrix._13*speed };
	if (input->IsKeyDown(DIK_A))
		movement = { -m_viewMatrix._11*speed, -m_viewMatrix._12*speed, -m_viewMatrix._13*speed };
*/

}
