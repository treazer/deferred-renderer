#pragma once

#include <vector>
#include "Graphics.h"
#include "Component.h"
#include "Shader.h"
#include "StandardShader.h"
class Texture;
class Mesh;
class Scene;
class GameObject;
class Renderer : public Component
{
public:
	Renderer();
	~Renderer();

	void Initialize(GameObject* gameObject);
	void SetShader(StandardShader* shader);
	void SetGameObject(GameObject* object);
	void SetTexture(Texture* texture);
	void SetMesh(Mesh* mesh);
	void Render(Scene * scene);
	void Render(Graphics* graphics, XMFLOAT4X4 viewMatrix, XMFLOAT4X4 projectionMatrix, XMFLOAT4 eyePosition, bool shadowOnly = false, bool translucencyOnly = false);
	StandardShader* GetShader();
	Mesh* GetMesh();
	string GetName();
	Texture* GetTexture();
	GameObject* GetGameObject();
	float GetTessellation();
	float GetDisplacementStrength();
	float GetNormalStrength();
	float GetUVTilingX();
	float GetUVTilingY();
	void Update(Scene * scene);

	void SetAlbedo(Texture * texture, XMFLOAT4 color);
	void SetNormal(Texture* texture);
	void SetRoughness(Texture * texture, float multiplier);
	void SetMetal(Texture* texture, float multiplier);
	void SetTranslucency(Texture* texture);
	void SetCutout(Texture* texture);
	void SetDisplacement(Texture* texture);
	void SetDisplacementStrength(float strength);
	void SetTessellation(float tessellation);
	void SetEmission(float emission);
	void SetMaterialId(int id);
	void SetNormalStrength(float strength);
	void SetUVTiling(float x, float y);
	void SetLightmap(Texture* texture);
	void DefineMetal(bool metal);
	void SetTranslucencyColor(XMFLOAT4 color);
private:

	GameObject* m_gameObject;
	StandardShader* m_shader;
	Texture* m_texture;
	Mesh* m_mesh;
	Shader::RendererBufferType m_materialAttributes;

	Texture* m_albedo;
	Texture* m_normal;
	Texture* m_roughness;
	Texture* m_metal;
	Texture* m_translucency;
	Texture* m_cutout;
	Texture* m_displacement;
	Texture* m_lightmap;
	float m_displacementStrength;
	float m_tessellation;
	float m_emission;
	float m_normalStrength;
	float m_uvTilingX;
	float m_uvTilingY;
	int m_materialId;

};