#include "Foliage.h"
#include <iostream>
#include "ComputeShader.h"
#include <string>
#include "Mesh.h"
#include "GameObject.h"
#include "AssetManager.h"
#include "Scene.h"
#include "Timer.h"
#include "Wind.h"
#include <iostream>
using namespace std;

Foliage::Foliage()
{
	g_pBuf0 = 0;
	g_pBufResult = 0;
	g_pBuf0SRV = 0;
	g_pBuf1SRV = 0;
	g_pBufResultUAV = 0;

	g_vBuf0[128000];
	g_vBuf1[128000];

	m_baseStrength = 100;
	m_branchStrength_1 = 10;
	m_branchStrength_2 = 10;
	m_branchStrength_3 = 10;
	m_leafStrength = 100;

	m_treeInfo.worldPosition = XMFLOAT4();
	m_treeInfo.leafDimension = XMFLOAT4();
	m_treeInfo.orientationFlattening = XMFLOAT4();
	m_treeInfo.leafGravity = XMFLOAT4();
	m_treeInfo.numLeaves = 0;
	m_treeInfo.numVertices = 0;
	m_treeInfo.minimumDistanceLeaves = 0;
	m_treeInfo.distancePadding = 0;
	m_treeInfo.leafStrength = 0;
	m_treeInfo.leafBranchInfluence = 0;
	m_treeInfo.leafStiffness = 0;
	m_treeInfo.doubleSided = 0;
	m_treeInfo.staticBranches = 0;
	m_treeInfo.branchStrength = 1.0f;
}


Foliage::~Foliage()
{
}

float Foliage::VectorDistance(const XMFLOAT3& v1, const XMFLOAT3& v2)
{
	XMVECTOR vector1 = XMLoadFloat3(&v1);
	XMVECTOR vector2 = XMLoadFloat3(&v2);
	XMVECTOR vectorSub = XMVectorSubtract(vector1, vector2);
	XMVECTOR length = XMVector3Length(vectorSub);

	float distance = 0.0f;
	XMStoreFloat(&distance, length);
	distance = abs(distance);
	return distance;
}

float Foliage::VectorDistance(const XMFLOAT4& v1, const XMFLOAT4& v2)
{
	XMFLOAT4 w1 = v1;
	XMFLOAT4 w2 = v2;
	w1.w = 0;
	w2.w = 0;
	XMVECTOR vector1 = XMLoadFloat4(&v1);
	XMVECTOR vector2 = XMLoadFloat4(&v2);
	XMVECTOR vectorSub = XMVectorSubtract(vector1, vector2);
	XMVECTOR length = XMVector3Length(vectorSub);

	float distance = 0.0f;
	XMStoreFloat(&distance, length);
	distance = abs(distance);
	return distance;
}

void Foliage::Initialize(GameObject* gameObject, Graphics * graphics, HWND hwnd)
{
	Component::m_gameObject = gameObject;
	m_graphics = graphics;

	m_branchMovementTexture = AssetManager::GetInstance()->GetTextureByName((char*)"branchMovement");

	m_computeShader = new ComputeShader();
	m_computeShader->Initialize(m_graphics->GetDevice(), m_graphics->GetHwnd(), "Shaders/computeShader.hlsl");
	
}

void Foliage::Initialize(GameObject* gameObject)
{
	Component::m_gameObject = gameObject;

}

string Foliage::GetName()
{
	return Component::m_name;
}

GameObject* Foliage::GetGameObject()
{
	return m_gameObject;
}

void Foliage::Update(Scene * scene)
{
	if (m_treeInfo.staticBranches<=0)
		CalculateForces(*GetGameObject()->GetRenderer()->GetMesh()->GetBoneInfo(), scene->GetWind());
}

void Foliage::Render(Scene * scene)
{

	ID3D11ShaderResourceView * branchMovementRes = m_branchMovementTexture->GetTexture();
	scene->GetGraphics()->GetDeviceContext()->VSSetShaderResources(7, 1, &branchMovementRes);
	GetGameObject()->GetRenderer()->GetShader()->SetShaderParameters(scene->GetGraphics()->GetDeviceContext(), m_treeInfo.leafStrength, m_treeInfo.leafBranchInfluence, m_treeInfo.leafStiffness);
}

void Foliage::Render(ID3D11DeviceContext * deviceContext)
{

}

void Foliage::ExecuteComputeShader(Mesh * mesh, int leaves, Mesh::VertexType * vertices, int numVertices)
{
	for (int i = 0; i < leaves; ++i)
	{
		g_vBuf0[i] = mesh->GetVertices()[i];
	}

	for (int i = 0; i < numVertices; ++i)
	{
		g_vBuf1[i] = vertices[i];
	}

	//compute shader
	if (FAILED(m_computeShader->CreateStructuredBuffer(m_graphics->GetDevice(), sizeof(Mesh::VertexType), leaves, &g_vBuf0[0], &g_pBuf0))) std::cout << "failed" << std::endl;
	if (FAILED(m_computeShader->CreateStructuredBuffer(m_graphics->GetDevice(), sizeof(Mesh::VertexType), numVertices, &g_vBuf1[0], &g_pBuf1))) std::cout << "failed" << std::endl;
	if (FAILED(m_computeShader->CreateStructuredBuffer(m_graphics->GetDevice(), sizeof(LeafData), leaves, nullptr, &g_pBufResult))) std::cout << "failed" << std::endl;
	if (FAILED(m_computeShader->CreateStructuredBuffer(m_graphics->GetDevice(), sizeof(Mesh::VertexType), leaves*4, nullptr, &g_pBufResult2))) std::cout << "failed" << std::endl;

	if (FAILED(m_computeShader->CreateBufferSRV(m_graphics->GetDevice(), g_pBuf0, &g_pBuf0SRV))) std::cout << "failed" << std::endl;
	if (FAILED(m_computeShader->CreateBufferSRV(m_graphics->GetDevice(), g_pBuf1, &g_pBuf1SRV))) std::cout << "failed" << std::endl;
	if (FAILED(m_computeShader->CreateBufferUAV(m_graphics->GetDevice(), g_pBufResult, &g_pBufResultUAV))) std::cout << "failed" << std::endl;
	if (FAILED(m_computeShader->CreateBufferUAV(m_graphics->GetDevice(), g_pBufResult2, &g_pBufResultUAV2))) std::cout << "failed" << std::endl;

	m_treeInfo.numLeaves = leaves;
	m_treeInfo.numVertices = numVertices;
	m_computeShader->SetShaderParameters(m_graphics->GetDeviceContext(), m_treeInfo);

	//COMPUTE SHADER

	m_graphics->GetDeviceContext()->CSSetShader(m_computeShader->GetComputeShader(), NULL, 0);
	ID3D11ShaderResourceView* aRViews[2] = { g_pBuf0SRV, g_pBuf1SRV };

	m_graphics->GetDeviceContext()->CSSetShaderResources(0, 2, aRViews);
	m_graphics->GetDeviceContext()->CSSetUnorderedAccessViews(0, 1, &g_pBufResultUAV, nullptr);
	m_graphics->GetDeviceContext()->CSSetUnorderedAccessViews(1, 1, &g_pBufResultUAV2, nullptr);
	int rest = leaves % 32;
	int calls = leaves / 32;

	m_graphics->GetDeviceContext()->Dispatch(calls, 1, 1);

	m_graphics->GetDeviceContext()->CSSetShader(nullptr, nullptr, 0);

	ID3D11Buffer* debugbuf = m_computeShader->CreateAndCopyToDebugBuf(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), g_pBufResult);
	D3D11_MAPPED_SUBRESOURCE MappedResource;
	LeafData *p;
	Mesh::VertexType *p2;

	m_graphics->GetDeviceContext()->Map(debugbuf, 0, D3D11_MAP_READ, 0, &MappedResource);
	p = (LeafData*)MappedResource.pData;

	debugbuf = m_computeShader->CreateAndCopyToDebugBuf(m_graphics->GetDevice(), m_graphics->GetDeviceContext(), g_pBufResult2);
	m_graphics->GetDeviceContext()->Map(debugbuf, 0, D3D11_MAP_READ, 0, &MappedResource);
	p2 = (Mesh::VertexType*)MappedResource.pData;

	debugbuf->Release();

	m_leafData = p;

	m_leaves->SetVertices(p2);
	m_leaves->SetIndexCount(m_numLeaves * 12);
	m_leaves->SetVertexCount(m_numLeaves * 4);
	
	//cleanup
	LeafData * newLeafData;
	Mesh::VertexType* newVertexData;
	std::vector<unsigned long> newIndexData;
	newIndexData = std::vector<unsigned long>();

	//figure out number of leaves
	int newNumberLeaves = 0;
	for (int i = 0; i < m_numLeaves; i++)
	{
		if (m_leafData[i].position.x != 0 && m_leafData[i].position.y != 0)
			newNumberLeaves++;
	}

	newLeafData = new LeafData[newNumberLeaves];
	newVertexData = new Mesh::VertexType[newNumberLeaves * 4];

	int leafId = 0;
	for (int i = 0; i < m_numLeaves; i++)
	{
		if (m_leafData[i].position.x != 0 || m_leafData[i].position.y != 0 || m_leafData[i].position.z != 0)
		{
			//vertices
			newVertexData[(leafId * 4)] = p2[i*4];
			newVertexData[(leafId * 4) + 1] = p2[i*4 + 1];
			newVertexData[(leafId * 4) + 2] = p2[i*4 + 2];
			newVertexData[(leafId * 4) + 3] = p2[i*4 + 3];
			//indices
			newIndexData.push_back((leafId * 4) + 0);
			newIndexData.push_back((leafId * 4) + 2);
			newIndexData.push_back((leafId * 4) + 1);
			newIndexData.push_back((leafId * 4) + 3);
			newIndexData.push_back((leafId * 4) + 1);
			newIndexData.push_back((leafId * 4) + 2);
			//back
			newIndexData.push_back((leafId * 4) + 0);
			newIndexData.push_back((leafId * 4) + 1);
			newIndexData.push_back((leafId * 4) + 2);
			newIndexData.push_back((leafId * 4) + 3);
			newIndexData.push_back((leafId * 4) + 2);
			newIndexData.push_back((leafId * 4) + 1);
			leafId++;
		}
	}

	m_numLeaves = newNumberLeaves;
	m_leaves->SetVertices(newVertexData);
	m_leaves->SetIndices(newIndexData);
	m_leaves->SetIndexCount(m_numLeaves * 12);
	m_leaves->SetVertexCount(m_numLeaves * 4);

	delete newLeafData;
}

Renderer * Foliage::GetRenderer()
{
	return m_leafRenderer;
}

Shader::TreeBufferType Foliage::GetTreeInfo()
{
	return m_treeInfo;
}

void Foliage::SetTreeInfo(Shader::TreeBufferType info)
{
	m_treeInfo = info;
}


void Foliage::LoadLeafData(ID3D11Device* device, Mesh* mesh, HWND hwnd)
{
	XMFLOAT3 upVector = XMFLOAT3(0, 1, 0);

	m_leaves = mesh;
	m_numLeaves = mesh->GetVertexCount();
	Mesh::VertexType * leafPoints = mesh->GetVertices();
	if (m_numLeaves == 0) return;
	const Mesh::VertexType * vertices = m_gameObject->GetRenderer()->GetMesh()->GetVertices();
	std::vector<LeafData> leafData;
	leafData.resize(0);
	int amountLeaves = 0;
	XMFLOAT4 leafDir, leafSideDir, leafUpDir;

	ExecuteComputeShader(m_leaves, m_numLeaves, (Mesh::VertexType*)vertices, m_gameObject->GetRenderer()->GetMesh()->GetVertexCount());
	m_leaves->CreateBuffers(device, true, m_numLeaves * 12, m_numLeaves * 4);

	
	AddRendererToObject(device, hwnd);
}

void Foliage::CreateLeafMesh(ID3D11Device* device)
{
	float leafLength = 3.3f;
	float leafWidth = 1.1f;

	m_leaves->GetIndices().clear();
	Mesh::VertexType * vertices = new Mesh::VertexType[m_numLeaves * 4];
	std::vector< unsigned long > indices;

	XMVECTOR basePosition, widthVec, lengthVec, normal, point, uvCoord, pointLength;
	XMVECTOR aUV, bUV, cUV, dUV;
	XMVECTOR gravity = XMLoadFloat4(new XMFLOAT4(0.0f, -0.03f, 0.0f, 0));
	int slot;
	XMFLOAT4 pos;
	XMFLOAT2 uv;
	XMFLOAT4 anchor;
	float posLength;

	aUV = XMLoadFloat2(new XMFLOAT2(-1.0, -0.5));
	bUV = XMLoadFloat2(new XMFLOAT2(-1.0, 0.0));
	cUV = XMLoadFloat2(new XMFLOAT2(0.0, -0.5));
	dUV = XMLoadFloat2(new XMFLOAT2(0.0, 0.0));

	for (int i = 0; i < m_numLeaves; i++)
	{
		leafLength = 0.3f;
		leafWidth = 0.1f;

		basePosition = XMLoadFloat4(&m_leafData[i].position);
		widthVec = XMVector3Normalize(XMLoadFloat4(&m_leafData[i].tangent));
		lengthVec = XMVector3Normalize(XMLoadFloat4(&m_leafData[i].bitangent));
		normal = XMVector3Normalize(XMLoadFloat4(&m_leafData[i].normal));
		
		XMStoreFloat4(&anchor, basePosition);
		leafLength += (rand() % 10 + 1)*0.01f;
		leafWidth += (rand() % 10 + 1)*0.007f;
		// Saving
		for (unsigned int j = 0; j < 4; j++) {

			if (j == 0) 
			{
				uvCoord = aUV;
				point = XMVectorSubtract(basePosition, widthVec*leafWidth);
			}
			if (j==1)
			{
				uvCoord = bUV;
				point = XMVectorAdd(basePosition, widthVec*leafWidth);
			}
			if (j == 2)
			{
				uvCoord = cUV;
				point = XMVectorAdd(basePosition, lengthVec*leafLength);
				point = XMVectorSubtract(point, widthVec*leafWidth);
				point = XMVectorAdd(point, gravity);
			}
			if (j == 3)
			{
				uvCoord = dUV;
				point = XMVectorAdd(basePosition, lengthVec*leafLength);
				point = XMVectorAdd(point, widthVec*leafWidth);
				point = XMVectorAdd(point, gravity);
			}

			// Store the anchor in the position and offset in the color to calculate the real position in the shader fast
			point = XMVectorSubtract(point, basePosition);
			pointLength = XMVector3Length(point);
			point = XMVector3Normalize(point);

			XMStoreFloat4(&pos, point);
			XMStoreFloat(&posLength, pointLength);
			XMStoreFloat2(&uv, uvCoord);

			slot = (i * 4) + j;
			vertices[slot].position = anchor;
			vertices[slot].uv = XMFLOAT4(uv.x, uv.y,0,0);
			vertices[slot].normal = XMFLOAT4(m_leafData[i].normal.x, m_leafData[i].normal.y, m_leafData[i].normal.z,0);
			vertices[slot].tangent = XMFLOAT4(m_leafData[i].tangent.x, m_leafData[i].tangent.y, m_leafData[i].tangent.z,0);
			vertices[slot].bitangent = XMFLOAT4(m_leafData[i].bitangent.x, m_leafData[i].bitangent.y, m_leafData[i].bitangent.z,0);
			vertices[slot].color = XMFLOAT4(pos.x,pos.y,pos.z,posLength);
			vertices[slot].boneIDs = m_leafData[i].boneIDs;
			vertices[slot].boneWeights = m_leafData[i].boneWeights;
		}

		//front
		indices.push_back((i * 4) + 0);
		indices.push_back((i * 4) + 2);
		indices.push_back((i * 4) + 1);
		indices.push_back((i * 4) + 3);
		indices.push_back((i * 4) + 1);
		indices.push_back((i * 4) + 2);
		//back
		
		indices.push_back((i * 4) + 0);
		indices.push_back((i * 4) + 1);
		indices.push_back((i * 4) + 2);
		indices.push_back((i * 4) + 3);
		indices.push_back((i * 4) + 2);
		indices.push_back((i * 4) + 1);
	}

	m_leaves->SetVertices(vertices);
	m_leaves->SetIndices(indices);
	m_leaves->SetIndexCount(m_numLeaves * 12);
	m_leaves->SetVertexCount(m_numLeaves * 4);
	m_leaves->CreateBuffers(device,true, m_numLeaves*12,m_numLeaves*4);
}

void Foliage::SetFoliageShader(FoliageShader * shader)
{
	m_foliageShader = shader;
}

void Foliage::AddRendererToObject(ID3D11Device* device, HWND hwnd)
{
	AssetManager * resourceManager = AssetManager::GetInstance();

	m_leafRenderer = new Renderer();
	m_rendererId = m_gameObject->AddRenderer(m_leafRenderer);
	m_gameObject->GetRenderer(m_rendererId)->Initialize(m_gameObject);

	m_leafRenderer->SetShader((StandardShader*)resourceManager->GetShaderByName("leaf.fx"));
	m_leafRenderer->SetAlbedo(AssetManager::GetInstance()->GetTextureByName((char*)"GroundFoliage1_C"), XMFLOAT4(1, 1, 1, 1));
	m_leafRenderer->SetNormal(AssetManager::GetInstance()->GetTextureByName((char*)"normalDefault"));
	m_leafRenderer->SetDisplacement(AssetManager::GetInstance()->GetTextureByName((char*)"dark"));
	m_leafRenderer->SetCutout(AssetManager::GetInstance()->GetTextureByName((char*)"GroundFoliage1_A"));
	m_leafRenderer->SetMetal(AssetManager::GetInstance()->GetTextureByName((char*)"dark"), 1);
	m_leafRenderer->SetTranslucency(AssetManager::GetInstance()->GetTextureByName((char*)"white"));
	m_leafRenderer->SetRoughness(AssetManager::GetInstance()->GetTextureByName((char*)"leaf2_R"), 1);
	m_leafRenderer->DefineMetal(false);
	m_leafRenderer->SetMesh(m_leaves);
	m_leafRenderer->GetShader()->SetTessellation(true);
	m_leafRenderer->SetTessellation(1);
	m_leafRenderer->SetDisplacementStrength(0.0f);
	m_leafRenderer->SetUVTiling(1, 1);
	m_leafRenderer->SetNormalStrength(1);
	
}


void Foliage::AssignBranchHierarchyToVector(int vertexCount, Mesh::VertexType * vertices, const std::vector<Mesh::BoneInfoData> boneInfo)
{

	float mainBone;
	float mainBoneWeight;

	std::vector<float> parentList;
	std::vector<float> parentWeightsList;
	for (int i = 0; i < vertexCount; i++) {
		mainBone = -1;
		mainBoneWeight = 5;

		// The assignment order in boneIDs might be unsorted
		// Therefore, bone with the highest weight is being picked and assigned as the main bone
		mainBone = vertices[i].boneIDs.x;
		if (mainBone < 0 || mainBone > boneInfo.size() - 1) mainBone = 0;
		if (vertices[i].boneWeights.x < vertices[i].boneWeights.y || vertices[i].boneIDs.y >= 0 && boneInfo[vertices[i].boneIDs.y].parent == mainBone)
			mainBone = vertices[i].boneIDs.y;
		if (vertices[i].boneWeights.y < vertices[i].boneWeights.z || vertices[i].boneIDs.z >= 0 && boneInfo[vertices[i].boneIDs.z].parent == mainBone)
			mainBone = vertices[i].boneIDs.z;
		if (vertices[i].boneWeights.z < vertices[i].boneWeights.w || vertices[i].boneIDs.w >= 0 && boneInfo[vertices[i].boneIDs.w].parent == mainBone)
			mainBone = vertices[i].boneIDs.w;
		mainBoneWeight = boneInfo[(unsigned int)mainBone].strength;
		parentList.push_back(mainBone);
		parentWeightsList.push_back(mainBoneWeight);
		// Look for parents of the bone to create hierarchy
		while (boneInfo[parentList[parentList.size()-1]].parent>=0)
		{
			//std::cout << boneInfo[parentList[parentList.size() - 1]].parent << std::endl;
			parentList.push_back(boneInfo[parentList[parentList.size() - 1]].parent);
			parentWeightsList.push_back(boneInfo[parentList[parentList.size() - 1]].strength);
		}
		// Need to fill up the rest of the vector in case no parents exist
		int ps = parentList.size();
		int rest = 4 - ps;
		for (int j = 0; j < rest; j++)
		{
			parentList.push_back(-1);
			parentWeightsList.push_back(1000);
			ps++;
		}
		vertices[i].boneIDs = XMFLOAT4(parentList[ps-4], parentList[ps-3], parentList[ps-2], parentList[ps-1]);
		vertices[i].boneWeights = XMFLOAT4(parentWeightsList[ps-4], parentWeightsList[ps-3], parentWeightsList[ps-2], parentWeightsList[ps-1]);
		parentList.clear();
		parentWeightsList.clear();
	}

}
/**
	Calculates the height value for each branch in the tree
	In order to obtain the height of a branch without defining it beforehand
	Checks all vertices belonging to a branch and saves the one with the biggest distance along the forward-direction
	Saves the result in the boneInfoData
*/
void Foliage::CalculateHeightOfBranches(int vertexCount, Mesh::VertexType * vertices, std::vector<Mesh::BoneInfoData> &boneInfo, Mesh * mesh)
{
	int mainBone;
	XMVECTOR localPosition, branchRotation, heightVec;
	XMFLOAT3 heightFloat3;
	for (int i = 0; i < vertexCount; i++) {

		mainBone = (int)vertices[i].boneIDs.x;
		localPosition = XMVector4Transform(XMLoadFloat4(&vertices[i].position), XMLoadFloat4x4(&boneInfo[mainBone].BoneOffset)); //bring to local space
		branchRotation = XMVectorSet(0,1,0,0);

		XMVECTOR pointPosition = XMLoadFloat4(&vertices[i].position);
		XMVECTOR bonePosition = XMLoadFloat4(&boneInfo[mainBone].nodePosition);
		heightVec = XMVector3Length(pointPosition - bonePosition);

		XMStoreFloat3(&heightFloat3,heightVec);

		if (boneInfo[mainBone].height < abs(heightFloat3.x))
		{
			boneInfo[mainBone].height = abs(heightFloat3.x);
		}

		if (boneInfo[mainBone].strength < abs(heightFloat3.z))
		{
			boneInfo[mainBone].strength = abs(heightFloat3.z);
		}
	}

	//reasign to vertices
	for (int i = 0; i < vertexCount; i++) {
		if (vertices[i].boneIDs.x >= 0)
			vertices[i].boneWeights.x = boneInfo[vertices[i].boneIDs.x].strength;
		if (vertices[i].boneIDs.y >= 0)
			vertices[i].boneWeights.y = boneInfo[vertices[i].boneIDs.y].strength;
		if (vertices[i].boneIDs.z >= 0)
			vertices[i].boneWeights.z = boneInfo[vertices[i].boneIDs.z].strength;
		if (vertices[i].boneIDs.w >= 0)
			vertices[i].boneWeights.w = boneInfo[vertices[i].boneIDs.w].strength;
	}
}

/*

Calculates the forces that are being applied on the branches

*/
void Foliage::CalculateForces(std::vector<Mesh::BoneInfoData> &boneInfo, Wind * pWind)
{
	if (m_treeInfo.staticBranches) return;
	float damping = 0.98f;
	
	for (unsigned int i = 0; i < boneInfo.size(); i++) {
		Mesh::BoneInfoData bone = boneInfo[i];
		if (bone.isEnd == 1)
		{
			XMFLOAT4 sampledWind = pWind->SampleWindForPosition(XMFLOAT3(bone.nodePosition.x, bone.nodePosition.y, bone.nodePosition.z));
			XMFLOAT4 windVec = sampledWind;
			XMVECTOR boneForce = XMLoadFloat4(&boneInfo[i].force);
			XMVECTOR wind = XMLoadFloat4(&windVec);
			wind = wind * 0.012f * sampledWind.w;
			boneForce = boneForce*damping;
			boneForce = boneForce + wind;
			XMFLOAT4 force;
			XMStoreFloat4(&force, boneForce);
			boneInfo[i].force = force;

			//add force to parent
			XMFLOAT4 fChildForce = force;
			XMVECTOR childForce = XMLoadFloat4(&fChildForce);
			XMVECTOR parentForce = XMLoadFloat4(new XMFLOAT4());
			if (boneInfo[i].parent >= 0)
				parentForce = XMLoadFloat4(&boneInfo[boneInfo[i].parent].force);
			childForce = childForce * 0.002f;
			parentForce = parentForce + childForce + (wind*0.1f);
			XMStoreFloat4(&force, parentForce);
			if (boneInfo[i].parent >= 0)
				boneInfo[boneInfo[i].parent].force = force;
		}
		else
		{
			XMFLOAT4 windVec = XMFLOAT4(0, 0, 0, 0);
			XMVECTOR boneForce = XMLoadFloat4(&boneInfo[i].force);
			XMVECTOR wind = XMLoadFloat4(&windVec);
			boneForce = boneForce*damping;
			boneForce = boneForce+wind;
			XMFLOAT4 force;
			XMStoreFloat4(&force, boneForce);
			boneInfo[i].force = force;
			if (boneInfo[i].parent >= 0)
			{
				XMFLOAT4 fChildForce = force;
				XMVECTOR childForce = XMLoadFloat4(&fChildForce);
				XMVECTOR parentForce = XMLoadFloat4(&boneInfo[boneInfo[i].parent].force);
				childForce = childForce * 0.002f;
				parentForce = parentForce + childForce;
				XMStoreFloat4(&force, parentForce);
				boneInfo[boneInfo[i].parent].force = force;
			}
		}
	}
}

void Foliage::WriteBranchLevel(int vertexCount, Mesh::VertexType * vertices, std::vector<Mesh::BoneInfoData> &boneInfo)
{
	for (unsigned int i = 0; i < boneInfo.size(); i++) {
		Mesh::BoneInfoData bone = boneInfo[i];
		if (boneInfo[i].active == 1)
		{
			int level = 0;
			while (bone.parent >= 0)
			{
				level++;
				if (bone.active == 1)
					boneInfo[bone.parent].isEnd = 0;
				bone = boneInfo[bone.parent];
			}
			boneInfo[i].level = level;
			boneInfo[i].strength = boneInfo[i].strength / (level + 1);
		}
	}

	//update strength for vertex data
	for (int i = 0; i < vertexCount; i++) {
		if (vertices[i].boneIDs.x>=0)
		vertices[i].boneWeights.x = boneInfo[vertices[i].boneIDs.x].strength;
		if (vertices[i].boneIDs.y >= 0)
		vertices[i].boneWeights.y = boneInfo[vertices[i].boneIDs.y].strength;
		if (vertices[i].boneIDs.z >= 0)
		vertices[i].boneWeights.z = boneInfo[vertices[i].boneIDs.z].strength;
		if (vertices[i].boneIDs.w >= 0)
		vertices[i].boneWeights.w = boneInfo[vertices[i].boneIDs.w].strength;
	}
}

void Foliage::CreateRenderData()
{

}

void Foliage::CalculateLevelOfDetail()
{

}
