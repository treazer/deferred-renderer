#include "StandardShader.h"
#include <iostream>

StandardShader::StandardShader(ID3D11Device * device, HWND hwnd, WCHAR * shaderFileName, bool usesTessellation) :
	Shader(device, hwnd, shaderFileName, usesTessellation)
{
	m_samplerState = 0;
	m_initialized = InitializeSamplerState(device);
}

StandardShader::~StandardShader()
{
	if (m_samplerState)
	{
		m_samplerState->Release();
		m_samplerState = NULL;
	}

}

void StandardShader::Begin(ID3D11DeviceContext * deviceContext)
{
	if (m_useTessellation)
		deviceContext->DSSetSamplers(0, 1, &m_samplerState);
	deviceContext->PSSetSamplers(0, 1, &m_samplerState);
	deviceContext->VSSetSamplers(0, 1, &m_samplerState);
	Shader::Begin(deviceContext);
}

void StandardShader::BeginShadow(ID3D11DeviceContext* deviceContext)
{
	if (m_useTessellation)
		deviceContext->DSSetSamplers(0, 1, &m_samplerState);
	deviceContext->PSSetSamplers(0, 1, &m_samplerState);
	deviceContext->VSSetSamplers(0, 1, &m_samplerState);
	Shader::BeginShadow(deviceContext);
}

void StandardShader::BeginTranslucency(ID3D11DeviceContext* deviceContext)
{
	if (m_useTessellation)
		deviceContext->DSSetSamplers(0, 1, &m_samplerState);
	deviceContext->PSSetSamplers(0, 1, &m_samplerState);
	deviceContext->VSSetSamplers(0, 1, &m_samplerState);
	Shader::BeginTranslucency(deviceContext);
}

void StandardShader::End(ID3D11DeviceContext * deviceContext)
{
	deviceContext->PSSetSamplers(0, 0, NULL);
	Shader::End(deviceContext);
}

bool StandardShader::SetShaderParameters(ID3D11DeviceContext * deviceContext,
	ID3D11ShaderResourceView * albedo,
	ID3D11ShaderResourceView * normal,
	ID3D11ShaderResourceView * displacement,
	ID3D11ShaderResourceView * roughness,
	ID3D11ShaderResourceView * metal,
	ID3D11ShaderResourceView * translucency,
	ID3D11ShaderResourceView * cutout,
	ID3D11ShaderResourceView * lightmap,
	float emission,
	int materialId)
{
	deviceContext->PSSetShaderResources(0, 1, &albedo);
	deviceContext->PSSetShaderResources(1, 1, &normal);
	if (m_useTessellation)
		deviceContext->DSSetShaderResources(2, 1, &displacement);
	deviceContext->PSSetShaderResources(3, 1, &roughness);
	deviceContext->PSSetShaderResources(4, 1, &metal);
	deviceContext->PSSetShaderResources(5, 1, &translucency);
	deviceContext->PSSetShaderResources(6, 1, &cutout);
	deviceContext->PSSetShaderResources(13, 1, &lightmap);
	return true;
}


bool StandardShader::Initialize(ID3D11Device * device, HWND hwnd, WCHAR * shaderFileName, bool usesTessellation)
{
	if (!Shader::Initialize(device, hwnd, shaderFileName, usesTessellation))
	{
		return false;
	}
	
	return true;
}

bool StandardShader::InitializeSamplerState(ID3D11Device * device)
{
	D3D11_SAMPLER_DESC samplerDesc;
	HRESULT result;

	//crate texture sampler desc
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;// D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = -1.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	//create sampler state
	result = device->CreateSamplerState(&samplerDesc, &m_samplerState);
	if (FAILED(result))
	{
		return false;
	}


	//crate texture sampler desc
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;//D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.MipLODBias = -1.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	//create sampler state
	result = device->CreateSamplerState(&samplerDesc, &m_samplerStateClamp);
	if (FAILED(result))
	{
		return false;
	}

	return true;
}
