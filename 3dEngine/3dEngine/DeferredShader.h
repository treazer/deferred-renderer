#pragma once
#include "Shader.h"
class DeferredShader :
	public Shader
{
public:
	DeferredShader(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName);
	virtual ~DeferredShader(void);

	void Begin(ID3D11DeviceContext* deviceContext);
	void End(ID3D11DeviceContext* deviceContext);

	bool SetShaderParameters(ID3D11DeviceContext* deviceContext, XMFLOAT4 lightDirection, XMFLOAT4 lightPosition, XMFLOAT4 color, float intensity, XMFLOAT4 eye, XMFLOAT4X4 lightView, XMFLOAT4X4 lightOrtho, XMFLOAT4X4 worldMatrix, XMFLOAT4X4 viewMatrix, XMFLOAT4X4 projectionMatrix, XMFLOAT4X4 projectionMatrixScene, XMFLOAT4X4 viewMatrixScene, int currentFrame, int frameSteps);
	using Shader::SetShaderParameters;

protected:
	bool Initialize(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName);
	bool InitializeSamplerState(ID3D11Device* device);

private:
	ID3D11SamplerState* m_samplerState;
	ID3D11SamplerState* m_shadowSamplerState;
	ID3D11Buffer* m_lightBuffer;
};

