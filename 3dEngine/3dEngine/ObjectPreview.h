#pragma once
#include "Component.h"
#include "Input.h"
class Scene;
class ObjectPreview :
	public Component
{
public:
	ObjectPreview();
	~ObjectPreview();

	void Initialize(GameObject* gameObject);
	string GetName();
	GameObject* GetGameObject();
	void Update(Scene * scene);
	void Update(Input* input);
};

