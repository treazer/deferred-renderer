#include "globals.fx"

Texture2D albedoTex : register(t0);
Texture2D normalTex : register(t1);
Texture2D displacementTex : register(t2);
Texture2D roughnessTex : register(t3);
Texture2D metalTex : register(t4);
Texture2D translucencyTex : register(t5);
Texture2D cutoutTex : register(t6);

SamplerState SampleType;

////////////////////////////////////////////////////////////////////////////////
// Domain Shader
////////////////////////////////////////////////////////////////////////////////
[domain("tri")]

PixelInputType ColorDomainShader(ConstantOutputType input, float3 uvwCoord : SV_DomainLocation, const OutputPatch<HullOutputType, 3> patch)
{
    float4 vertexPosition;
    PixelInputType output;
 
    // Determine the position of the new vertex.
    vertexPosition = uvwCoord.x * patch[0].position + uvwCoord.y * patch[1].position + uvwCoord.z * patch[2].position;
    vertexPosition.w = 1;
    output.tex = uvwCoord.x * patch[0].tex + uvwCoord.y * patch[1].tex + uvwCoord.z * patch[2].tex;
    output.normal = float4(patch[0].normal,0);//float4(uvwCoord.x * patch[0].normal + uvwCoord.y * patch[1].normal + uvwCoord.z * patch[2].normal,0);
    output.tangent = float4(patch[0].tangent,0);//uvwCoord.x * patch[0].tangent + uvwCoord.y * patch[1].tangent + uvwCoord.z * patch[2].tangent;
    output.bitangent = float4(patch[0].bitangent,0);//uvwCoord.x * patch[0].bitangent + uvwCoord.y * patch[1].bitangent + uvwCoord.z * patch[2].bitangent;
    //displacement
    vertexPosition=vertexPosition + (displacementTex.SampleLevel(SampleType, output.tex,0).r*output.normal*patch[0].dispStrength); //displacementStrength
    //vertexPosition+=float4(patch[0].normal*0.01f,0);
    //vertexPosition = patch[0].position;
    // Calculate the position of the new vertex against the world, view, and projection matrices.
    
    //vertexPosition.x+=output.normal.x;
    //vertexPosition.y+=output.normal.y;
    //vertexPosition.z+=output.normal.z;
    
    output.position = mul(vertexPosition, worldMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);
    
    //output.position = float4(vertexPosition,1);
    // Send the input color into the pixel shader.
    output.color = patch[0].color;
    output.wPosition = mul(vertexPosition, worldMatrix);
    output.normal.w = 0;
    output.normal = mul(output.normal, worldMatrix);
    output.tangent = mul(float4(output.tangent,0), worldMatrix).xyz;
    output.bitangent = mul(float4(output.bitangent,0), worldMatrix).xyz;
    //output.normal = float3(output.position.x,output.position.y,output.position.z);
    return output;
    
    
}

////////////////////////////////////////////////////////////////////////////////
// Patch Constant Function
////////////////////////////////////////////////////////////////////////////////
ConstantOutputType ColorPatchConstantFunction(InputPatch<HullInputType, 3> inputPatch, uint patchId : SV_PrimitiveID)
{    
    ConstantOutputType output;

    // Set the tessellation factors for the three edges of the triangle.
    output.edges[0] = inputPatch[0].tessellationStrength;
    output.edges[1] = inputPatch[0].tessellationStrength;
    output.edges[2] = inputPatch[0].tessellationStrength;

    // Set the tessellation factor for tessallating inside the triangle.
    output.inside = inputPatch[0].tessellationStrength;

    return output;
}

////////////////////////////////////////////////////////////////////////////////
// Hull Shader
////////////////////////////////////////////////////////////////////////////////
[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("ColorPatchConstantFunction")]

HullOutputType ColorHullShader(InputPatch<HullInputType, 3> patch, uint pointId : SV_OutputControlPointID, uint patchId : SV_PrimitiveID)
{
    HullOutputType output;


    // Set the position for this control point as the output position.
    output.position = patch[pointId].position;

    // Set the input color as the output color.
    output.color = patch[pointId].color;
    output.normal = patch[pointId].normal;
    output.tangent = patch[pointId].tangent;
    output.bitangent = patch[pointId].bitangent;
    output.tex = patch[pointId].tex;
    output.tessellationStrength = patch[pointId].tessellationStrength;
    output.dispStrength = patch[pointId].dispStrength;
    return output;
}




////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
HullInputType VSMain(VertexInputType input)
{
    //PixelInputType output;
    HullInputType output; //switch to this one
    
    // Change the position vector to be 4 units for proper matrix calculations.
    input.position.w = 1.0f;
    //float4 vec;
    // Calculate the position of the vertex against the world, view, and projection matrices.
    //vec = mul(input.position, worldMatrix);
    //vec = mul(output.position, viewMatrix);
    //vec = mul(output.position, projectionMatrix);

    // Store the texture coordinates for the pixel shader.
    //output.tex = input.tex;
    //output.normal = input.normal;
    
    output.position = input.position;
    output.color = float4(1,0,0,1);
    output.normal = input.normal;
    output.tangent = input.tangent;
    output.bitangent = input.bitangent;
    output.tex = input.tex;
    output.tex.y = 1-output.tex.y;
    
    //tessellation
    float4 pos = mul(input.position, worldMatrix);

    float pos2 = abs(length(eyePosition-pos));
    
    float tessellationValue = tessellationAmount-(pos2*0.1f);// / pos.w;

    if (tessellationValue > tessellationAmount) tessellationValue = tessellationAmount;
    if (tessellationValue < 1) tessellationValue = 1;
    output.tessellationStrength = tessellationValue;
    output.dispStrength = lerp (0, displacementStrength, tessellationValue/tessellationAmount);
    return output;
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
PS_OUTPUT PSMain(PixelInputType input) : SV_TARGET
{
	PS_OUTPUT output;
    float4 textureColor = float4(1, 0, 0, 1);

    // Sample the pixel color from the texture using the sampler at this texture coordinate location.
    textureColor = albedoTex.Sample(SampleType, input.tex);
    
    //drop transparent
    clip(textureColor.w-0.3f);
    
    //NORMAL
    
        // Sample the pixel in the bump map.
        float4 bumpMap = normalTex.Sample(SampleType, input.tex);

        // Expand the range of the normal value from (0, +1) to (-1, +1).
        bumpMap = (bumpMap * 2.0f) - 1.0f;

        // Calculate the normal from the data in the bump map.
        float3 bumpNormal = (bumpMap.x * input.tangent.xyz) + (bumpMap.y * input.bitangent.xyz) + (bumpMap.z * input.normal.xyz);
        
        // Normalize the resulting bump normal.
        //bumpNormal = normalize(bumpNormal);

    //NORMAL END
    /*
	output.Diffuse = textureColor;
    output.Diffuse.w = metalTex.Sample(SampleType, input.tex).r;
	output.Normal = float4(bumpNormal,1);
    output.Normal.w = roughnessTex.Sample(SampleType, input.tex).r;
    //Roughness
    input.wPosition.w = roughnessTex.Sample(SampleType, input.tex);
	output.Position = input.wPosition;
    output.Position.w = 0.1f;
    if (isMetal>0.9f)
        output.Position.w = 10.1f;
    else
        output.Position.w = 0.0f;
    */
    float3 finalColor;
    float2 finalNormal;
	finalColor = textureColor;
	float3 decodedNormal = float4(bumpNormal,1).xyz;
    float kPI = 3.1415926536f;
    finalNormal = (float2(atan2(decodedNormal.y,decodedNormal.x)/kPI, decodedNormal.z)+1.0)*0.5;
    float roughness = roughnessTex.Sample(SampleType, input.tex).r;
    float metalness = metalTex.Sample(SampleType, input.tex).r;
    if (metalness > 0.2f) roughness *=-1;
    
    output.GBuffer1 = float4(finalColor,roughness);
    output.GBuffer2 = float2(finalNormal);
    
    return output;
}
#include "shadows.fx"