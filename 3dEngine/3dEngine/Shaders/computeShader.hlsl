
#include "globals.fx"

struct VertexType
{
    float4 position;
    float4 color;
    float4 uv;
    float4 normal;
    float4 tangent;
    float4 bitangent;
    float4 boneWeights;
    float4 boneIDs;

};

struct LeafData
{
    float4 position;
    float4 color;
    float4 normal;
    float4 tangent;
    float4 bitangent;
    float4 boneWeights;
    float4 boneIDs;
};

cbuffer TreeBufferType: register(b0)
{
    float4 worldPosition;
    float4 leafDimension; //size x,y + variation x,y
    float4 orientationFlattening;
    float4 leafGravity;
    float numLeaves;
    float numVertices;
    float minimumDistanceLeaves;
    float distancePadding;
    float leafMovementStrength;
    float leafMovementBranchInfluence;
    float leafMovementStiffness;
    float doubleSided;
    float staticBranches;
    float branchStrength;
    float bTree;
    float cTree;
};

struct BufType
{
    int i;
    float4 f;
};

StructuredBuffer<VertexType> LeafPoints : register(t0);
StructuredBuffer<VertexType> TreePoints : register(t1);
RWStructuredBuffer<LeafData> LeafList : register(u0);
RWStructuredBuffer<VertexType> MeshVertices : register(u1);
//RWStructuredBuffer<int> MeshIndices : register(u2);

[numthreads(32, 1, 1)]
void CSMain( uint3 DTid : SV_DispatchThreadID )
{
    float3 upDirection = float3(0,1,0);
    float3 gravity = leafGravity.xyz;
    upDirection+= gravity;
    int currentLeaf = DTid.x;
	LeafList[currentLeaf].position = LeafPoints[currentLeaf].position;

    //check all vertices
    float closest = 2000;
    int closestId = -1;
    float dist;
    for (int i = 0; i < numVertices; i++)
    {
        //cheap check if position in array is empty because information about array length does not exist
        if (TreePoints[i].position.x != 0 || TreePoints[i].position.y != 0 || TreePoints[i].position.z != 0)  
        {
            dist = distance(TreePoints[i].position, LeafPoints[currentLeaf].position);
            if (dist < closest)
            {
                closestId = i;
                closest = dist;
            }
        }
    }
    
    //valid point found:
    if (closestId >= 0)
    {
        //Write values into leaf
        
        //move closer to mesh
        LeafList[currentLeaf].position -= ((LeafList[currentLeaf].position - TreePoints[closestId].position)*distancePadding);
        
        LeafList[currentLeaf].boneIDs = TreePoints[closestId].boneIDs;
        LeafList[currentLeaf].boneWeights = TreePoints[closestId].boneWeights;

        //new tangent and bitangent
        float3 leafPos = LeafPoints[currentLeaf].position.xyz;
        float3 branchPoint = TreePoints[closestId].position.xyz;

        //modify up direction
        
        upDirection+=float3(poissonDisk[closestId%64].x,0,poissonDisk[(currentLeaf+1)%33].y)*0.105f;//0.6f;
        upDirection.x = upDirection.x*orientationFlattening.x;
        upDirection = normalize(upDirection);
        //upDirection = float3(0,1,0);
        float3 leafDirection = leafPos - branchPoint;
        leafDirection +=leafGravity.xyz;
        leafDirection = normalize(leafDirection);
        //leafDirection = float3(1,0,0);
        //leafDirection+=float3(poissonDisk[closestId%34].x,0,poissonDisk[(currentLeaf+1)%33].y)*0.0045f;
        //leafDirection.y = leafDirection.y*orientationFlattening.y;//0.2f;
        //leafDirection = normalize(leafDirection);
        float3 sideDirection = normalize(cross(leafDirection, upDirection));
        //sideDirection = float3(0,0,1);
        //Write new orientation
        LeafList[currentLeaf].normal = float4(upDirection,0);
        LeafList[currentLeaf].tangent = float4(leafDirection,0);
        LeafList[currentLeaf].bitangent = float4(sideDirection,0);

    }
    DeviceMemoryBarrierWithGroupSync();
    //remove duplicates
    float minimumDistance = minimumDistanceLeaves;//0.0023f; //0.25f for leaf1
    for (int k = 0; k < numLeaves; k++)
    {
        //cheap check if position in array is empty because information about array length does not exist
        if (LeafList[k].position.y!=0)
        {
            dist = distance(LeafList[k].position, LeafList[currentLeaf].position);
            if (dist < minimumDistance)
            {
                if (k != currentLeaf)
                {
                    LeafList[currentLeaf].position = float4(0,0,0,0);
                }
            }
        }
    }
    
    if (LeafList[currentLeaf].position.x != 0 || LeafList[currentLeaf].position.y != 0 || LeafList[currentLeaf].position.z != 0)  
    {
        //create the mesh
        float leafLength = leafDimension.x;//0.053f;//0.65f; //0.38
        float leafWidth = leafDimension.y;//0.053f;//0.20f; //0.15

        /*
        float2 aUV = float2(-1.0, -0.5);
        float2 bUV = float2(-1.0, 0.0);
        float2 cUV = float2(0.0, -0.5);
        float2 dUV = float2(0.0, 0.0);
        */
        float2 aUV = float2(-1.0, -1.0);
        float2 bUV = float2(0.0, -1.0);
        float2 cUV = float2(-1.0, 0.0);
        float2 dUV = float2(0.0, 0.0);
        //float3 gravity = leafGravity.xyz;
        
        leafLength += (random( LeafList[currentLeaf].position.xy ) % 10 + 1)*leafLength*leafDimension.z;
        leafWidth += (random( LeafList[currentLeaf].position.xz ) % 10 + 1)*leafWidth*leafDimension.w;
            
          
        float3 pos;
        float posLength;
        float2 uvCoord;
        int slot;
        
        float3 basePosition = LeafList[currentLeaf].position.xyz;
        float3 tangent = LeafList[currentLeaf].tangent.xyz; 
        float3 bitangent = LeafList[currentLeaf].bitangent.xyz; 
        for (int j = 0; j < 4; j++) {

			if (j == 0) 
			{
				uvCoord = aUV;
				pos = basePosition-(bitangent*leafWidth);
			}
			if (j==1)
			{
				uvCoord = bUV;
				pos = basePosition+(bitangent*leafWidth);
			}
			if (j == 2)
			{
				uvCoord = cUV;
				pos = basePosition+(tangent*leafLength);
				pos = pos-(bitangent*leafWidth);
				pos += gravity;
			}
			if (j == 3)
			{
				uvCoord = dUV;
				pos = basePosition+(tangent*leafLength);
				pos = pos+(bitangent*leafWidth);
				pos += gravity;
			}    
            
            pos = pos-basePosition;
			posLength = length(pos);
			pos = normalize(pos);
            
            slot = (currentLeaf * 4) + j;
            
            MeshVertices[slot].position = LeafList[currentLeaf].position;
			MeshVertices[slot].uv = float4(uvCoord.x, uvCoord.y,0,0);
			MeshVertices[slot].normal = LeafList[currentLeaf].normal;
			MeshVertices[slot].tangent = LeafList[currentLeaf].tangent;
			MeshVertices[slot].bitangent = LeafList[currentLeaf].bitangent;
			MeshVertices[slot].color = float4(pos.x,pos.y,pos.z,posLength);
			MeshVertices[slot].boneIDs = LeafList[currentLeaf].boneIDs;
			MeshVertices[slot].boneWeights = LeafList[currentLeaf].boneWeights;
            
        }
    }
}
























