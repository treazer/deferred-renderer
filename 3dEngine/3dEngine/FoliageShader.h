#pragma once
#include "StandardShader.h"
#include "ComputeShader.h"

class FoliageShader :
	public StandardShader
{
public:
	FoliageShader(ID3D11Device * device, HWND hwnd, WCHAR * shaderFileName, bool usesTessellation);
	~FoliageShader();
};

