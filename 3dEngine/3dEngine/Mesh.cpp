#include "Mesh.h"
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing fla
#include <iostream>
#include "Foliage.h"
#include "Timer.h"


using namespace std;

Mesh::Mesh(void)
{
	m_globalInverseTransform = XMFLOAT4X4();
	m_vertexBuffer = 0;
	m_indexBuffer = 0;
	m_vertexCount = 0;
	m_indexCount = 0;
	m_name = "";
	m_hasBones = false;
	m_numBones = 0;

	m_indices.reserve(2048);
	m_vertices = 0;
	m_normals.reserve(2048);
	m_uvs.reserve(2048);
	m_tangents.reserve(2048);
	m_bitangents.reserve(2048);
	m_color1.reserve(2048);
	m_color2.reserve(2048);
}

Mesh::~Mesh(void)
{
	/*
	if (m_vertices)
	{
		delete[] m_vertices;
		m_vertices = 0;
	}
	if (m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}
	if (m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}
	*/
}

XMFLOAT4X4 Mesh::CreateDirectXMatrix(aiMatrix4x4* matrix)
{
	XMFLOAT4X4 newMatrix = XMFLOAT4X4();
	newMatrix._11 = matrix->a1;
	newMatrix._12 = matrix->a2;
	newMatrix._13 = matrix->a3;
	newMatrix._14 = matrix->a4;
	newMatrix._21 = matrix->b1;
	newMatrix._22 = matrix->b2;
	newMatrix._23 = matrix->b3;
	newMatrix._24 = matrix->b4;
	newMatrix._31 = matrix->c1;
	newMatrix._32 = matrix->c2;
	newMatrix._33 = matrix->c3;
	newMatrix._34 = matrix->c4;
	newMatrix._41 = matrix->d1;
	newMatrix._42 = matrix->d2;
	newMatrix._43 = matrix->d3;
	newMatrix._44 = matrix->d4;

	return newMatrix;
}


bool Mesh::Initialize(ID3D11Device* device, float size, bool writeable)
{
	return true;
}

void Mesh::Render(ID3D11DeviceContext* deviceContext, Shader * shader, bool shadowOnly, bool translucencyOnly)
{
	unsigned int stride;
	unsigned int offset;
	stride = sizeof(VertexType);
	offset = 0;
	if (shadowOnly)
		shader->BeginShadow(deviceContext);
	else if (translucencyOnly)
		shader->BeginTranslucency(deviceContext);
	else
		shader->Begin(deviceContext);
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	if (shader->UsesTessellation())
		deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
	else
		deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	deviceContext->DrawIndexed(m_indexCount, 0, 0);
	shader->End(deviceContext);
}

Mesh* Mesh::Load(ID3D11Device* device, string fileName, bool flippedAxis)
{
	
	Mesh * finalMesh = new Mesh();
	
	//define the name

	finalMesh->m_name = fileName;
	int pos = finalMesh->m_name.find_last_of("/");
	if (pos >= 0)
	{
		finalMesh->m_name = finalMesh->m_name.substr(pos + 1, finalMesh->m_name.length());
	}
	finalMesh->m_name = finalMesh->m_name.substr(0, finalMesh->m_name.find_last_of("."));

	//load the mesh
	std::cout << "Importing Mesh: " << fileName << std::endl;
	const aiScene* scene;
	if (flippedAxis)
		scene = finalMesh->_importer.ReadFile(fileName, aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_FlipWindingOrder);
	else
		scene = finalMesh->_importer.ReadFile(fileName, aiProcess_CalcTangentSpace | aiProcess_Triangulate);
	if (!scene) {//     If the import failed, report it
		std::cout << "!! Error " << finalMesh->_importer.GetErrorString() << std::endl;
		return false;
	}

	finalMesh->meshScene = scene;
	unsigned int meshes = scene->mNumMeshes;

	int numIndices = 0;
	int numVertices = 0;
	//create the data structures beforehand because of fixed size
	if (meshes <= 0) std::cout << "!! Import Error: no meshes found" << std::endl;
	for (unsigned int m = 0; m < meshes; m++) {
		aiMesh * currentMesh = scene->mMeshes[m];
		numVertices += currentMesh->mNumVertices;
		numIndices += currentMesh->mNumFaces * 3;
	}
	if (numVertices <= 0) std::cout << "!! Import Error: no vertices found" << std::endl;
	finalMesh->m_vertexCount = numVertices;
	finalMesh->m_vertices = new VertexType[numVertices];

	int currentVertexIndex = 0;

	//root node
	XMFLOAT4X4 rootMatrix = CreateDirectXMatrix(&scene->mRootNode->mTransformation);

	float a = rootMatrix._21;
	float b = rootMatrix._22;
	float c = rootMatrix._23;
	rootMatrix._21 = rootMatrix._31;
	rootMatrix._22 = rootMatrix._32;
	rootMatrix._23 = rootMatrix._33;
	rootMatrix._31 = a;
	rootMatrix._32 = b;
	rootMatrix._33 = c;
	for (unsigned int m = 0; m < meshes; m++) {
		aiMesh * mesh = scene->mMeshes[m];
		

		for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
			currentVertexIndex = i;
			const aiVector3D* pPos = &(mesh->mVertices[i]);
			const aiVector3D pNormal = mesh->HasNormals() ? (mesh->mNormals[i]) : aiVector3D(1.0f, 1.0f, 1.0f);
			const aiVector3D ptangent = mesh->mTangents!=NULL ? (mesh->mTangents[i]) : aiVector3D(1.0f, 1.0f, 1.0f);
			const aiVector3D pbitangent = mesh->mTangents != NULL ? (mesh->mBitangents[i]) : aiVector3D(1.0f, 1.0f, 1.0f);
			const aiVector3D* pTexCoord = mesh->HasTextureCoords(0) ? &(mesh->mTextureCoords[0][i]) : new aiVector3D(0.0f, 0.0f, 0.0f);
			const aiVector3D* pTexCoord2 = mesh->HasTextureCoords(1) ? &(mesh->mTextureCoords[1][i]) : new aiVector3D(0.0f, 0.0f, 0.0f);
			const aiColor4D* pVertexColor = mesh->HasVertexColors(0) ? &(mesh->mColors[0][i]) : new aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
			const aiColor4D* pVertexColor2 = mesh->HasVertexColors(1) ? &(mesh->mColors[1][i]) : new aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
			finalMesh->m_vertices[currentVertexIndex].position = XMFLOAT4(pPos->x, pPos->y, pPos->z, 1);
			if (flippedAxis)
				XMStoreFloat4(&finalMesh->m_vertices[currentVertexIndex].position,XMVector4Transform(XMLoadFloat4(&XMFLOAT4(pPos->x, pPos->y, pPos->z, 1)), XMLoadFloat4x4(&rootMatrix)));

			finalMesh->m_vertices[currentVertexIndex].uv = XMFLOAT4(pTexCoord->x, pTexCoord->y,pTexCoord2->x, pTexCoord2->y);
			finalMesh->m_vertices[currentVertexIndex].normal = XMFLOAT4(pNormal.x, pNormal.z, pNormal.y,0);
			finalMesh->m_vertices[currentVertexIndex].tangent = XMFLOAT4(ptangent.x, ptangent.z, ptangent.y,0);
			finalMesh->m_vertices[currentVertexIndex].bitangent = XMFLOAT4(pbitangent.x, pbitangent.z, pbitangent.y,0);
			finalMesh->m_vertices[currentVertexIndex].color = XMFLOAT4(pVertexColor->r, pVertexColor->g, pVertexColor->b, pVertexColor->a);
			finalMesh->m_vertices[currentVertexIndex].boneIDs = XMFLOAT4(-1,-1,-1,-1);
			finalMesh->m_vertices[currentVertexIndex].boneWeights = XMFLOAT4(0,0,0,0);
			finalMesh->m_color1.push_back(XMFLOAT4(pVertexColor->r, pVertexColor->g, pVertexColor->b, pVertexColor->a));
			finalMesh->m_color2.push_back(XMFLOAT4(pVertexColor2->r, pVertexColor2->g, pVertexColor2->b, pVertexColor2->a));
			currentVertexIndex++;
		}
		for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
			const aiFace& Face = mesh->mFaces[i];
			//assert(Face.mNumIndices == 3);
			if (Face.mNumIndices == 3)
			{
				for (int j = 0; j < (int)Face.mNumIndices; j++)
				{
					finalMesh->m_indices.push_back(Face.mIndices[j]);
				}
			}
		}
		if (mesh->HasBones())
			finalMesh->m_hasBones = true;

		if (mesh->HasBones()==true)
		{
			
			finalMesh->LoadBones(mesh);
		}
		
	}

	if (finalMesh->m_hasBones)
	{
		//std::cout << "Loaded Bones" << std::endl;
		finalMesh->LoadNodeHierarchy(scene->mRootNode, -1);
		
		finalMesh->DefineBoneDirection();
		finalMesh->ReduceBones();
		//std::cout << "Loaded node hierarchy" << std::endl;
		Foliage::AssignBranchHierarchyToVector(finalMesh->m_vertexCount, finalMesh->m_vertices, finalMesh->m_boneInfo);
		//std::cout << "Assigned Branch Hierarchy" << std::endl;
		Foliage::CalculateHeightOfBranches(finalMesh->m_vertexCount, finalMesh->m_vertices, finalMesh->m_boneInfo, finalMesh);
		//std::cout << "Calculated Height of Branches" << std::endl;
		Foliage::WriteBranchLevel(finalMesh->m_vertexCount, finalMesh->m_vertices, finalMesh->m_boneInfo);
		//std::cout << "Wrote Branch Levels" << std::endl;
	}
	//std::cout << "root name: " << scene->mRootNode->mName.C_Str() << std::endl;

	
	finalMesh->m_indexCount = numIndices;
	finalMesh->CreateBuffers(device, true, numIndices, numVertices);
	//std::cout << "Created mesh with: " << numIndices << " - " << numVertices << std::endl;
	
	return finalMesh;
}

bool Mesh::CreateBuffers(ID3D11Device* device, bool writeable, int numIndices, int numVertices)
{
	HRESULT result;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	
	m_vertexCount = numVertices;
	m_indexCount = numIndices;

	//set up vertex buffer description
	vertexBufferDesc.Usage = (writeable) ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * numVertices;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = (writeable) ? D3D11_CPU_ACCESS_WRITE : 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	vertexData.pSysMem = m_vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if (FAILED(result))
	{
		std::cout << "!! failed to create vertex buffer #1" << std::endl;
		return false;
	}
	else
	{
		unsigned long* ind = &m_indices[0];

		//set up index buffer desc
		indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		indexBufferDesc.ByteWidth = sizeof(unsigned long) * numIndices;
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;
		indexBufferDesc.StructureByteStride = 0;

		//set up index data
		indexData.pSysMem = ind;
		indexData.SysMemPitch = 0;
		indexData.SysMemSlicePitch = 0;

		//create index buffer
		result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
		if (FAILED(result))
		{
			std::cout << "!! failed to create index buffer #2" << std::endl;
			return false;
		}
	}
	return true;
}

bool Mesh::HasBones()
{
	return m_hasBones;
}

Mesh::VertexType* Mesh::GetVertices()
{
	return m_vertices;
}

std::vector<unsigned long> Mesh::GetIndices()
{
	return m_indices;
}

ID3D11Buffer* Mesh::GetVertexBuffer()
{
	return m_vertexBuffer;
}

int Mesh::GetIndexCount()
{
	return m_indexCount;
}

void Mesh::SetIndexCount(int indeces)
{
	m_indexCount = indeces;
}

int Mesh::GetVertexCount()
{
	return m_vertexCount;
}

void Mesh::SetVertexCount(int amountVertices)
{
	m_vertexCount = amountVertices;
}

void Mesh::SetVertices(VertexType * vertices)
{
	m_vertices = vertices;
}

void Mesh::SetIndices(std::vector< unsigned long > indices)
{
	m_indices = indices;
}

string Mesh::GetName()
{
	return m_name;
}

std::map<std::string, unsigned int> Mesh::GetBoneMap()
{
	return m_boneMap;
}

void Mesh::LoadBones(const aiMesh * pMesh)
{
	
	for (unsigned int i = 0; i < pMesh->mNumBones; i++) {
		int BoneIndex = 0;

		std::string BoneName(pMesh->mBones[i]->mName.data);
		std::map< std::string, unsigned int>::iterator found = m_boneMap.find(BoneName);

		if (found == m_boneMap.end()) {
			BoneIndex = m_numBones;
			m_numBones++;
			BoneInfoData bi;
			m_boneInfo.push_back(bi);
		}
		else {
			BoneIndex = m_boneMap[BoneName];
		}

		m_boneMap[BoneName] = BoneIndex;
		XMMATRIX mt = XMLoadFloat4x4(&CreateDirectXMatrix(&pMesh->mBones[i]->mOffsetMatrix));
		mt = XMMatrixTranspose(mt);
		XMFLOAT4X4 mStore;
		XMStoreFloat4x4(&mStore, mt);
		m_boneInfo[BoneIndex].BoneOffset = mStore;

		for (unsigned int j = 0; j < pMesh->mBones[i]->mNumWeights; j++) {

			unsigned int VertexID = pMesh->mBones[i]->mWeights[j].mVertexId;
			float Weight = pMesh->mBones[i]->mWeights[j].mWeight;
				
			if (m_vertices[VertexID].boneWeights.x == 0.0) {
				m_vertices[VertexID].boneIDs.x = (float)BoneIndex;
				m_vertices[VertexID].boneWeights.x = Weight;
			}
			else if (m_vertices[VertexID].boneWeights.y == 0.0) {
				m_vertices[VertexID].boneIDs.y = (float)BoneIndex;
				m_vertices[VertexID].boneWeights.y = Weight;
			}
			else if (m_vertices[VertexID].boneWeights.z == 0.0) {
				m_vertices[VertexID].boneIDs.z = (float)BoneIndex;
				m_vertices[VertexID].boneWeights.z = Weight;
			}
			else if (m_vertices[VertexID].boneWeights.w == 0.0) {
				m_vertices[VertexID].boneIDs.w = (float)BoneIndex;
				m_vertices[VertexID].boneWeights.w = Weight;
			}
		}
	}
	
}


/**
Goes through all children of a node and checks if a bone exists that maps to the node.
Defines the parent with the right id and sets the transformed position.
*/

void Mesh::LoadNodeHierarchy(aiNode* pNode, int parent)
{
	
	unsigned int BoneIndex = -1;
	std::string NodeName = (pNode->mName.C_Str());

	if (m_boneMap.find(NodeName) != m_boneMap.end())
	{
		BoneIndex = m_boneMap[NodeName];

		m_boneInfo[BoneIndex].parent = parent;
		m_boneInfo[BoneIndex].nodePosition = XMFLOAT4(pNode->mTransformation.a4, pNode->mTransformation.b4, pNode->mTransformation.c4, pNode->mTransformation.d4);
		
		//apply parent transform
		aiNode* node = pNode;
		aiMatrix4x4 parentMatrix = aiMatrix4x4();

		while (node->mParent != NULL)
		{
			parentMatrix = node->mTransformation * parentMatrix;
			node = node->mParent;
		}

		aiVector3D nodePosition = aiVector3D();
		aiQuaternion nodeRotation = aiQuaternion();
		aiVector3D nodeScaling = aiVector3D();
		parentMatrix.Decompose(nodeScaling, nodeRotation, nodePosition);
		int active = 1;
		int replacedBy = -1;

		m_boneInfo[BoneIndex].nodePosition = XMFLOAT4(nodePosition.x, nodePosition.y, nodePosition.z, 0);
		m_boneInfo[BoneIndex].nodeRotation = XMFLOAT4(nodeRotation.x, nodeRotation.y, nodeRotation.z, nodeRotation.w);
		m_boneInfo[BoneIndex].strength = 0.0134f;
		m_boneInfo[BoneIndex].BoneTransform = CreateDirectXMatrix(&pNode->mTransformation);
		m_boneInfo[BoneIndex].isEnd = 1;
		m_boneInfo[BoneIndex].force = XMFLOAT4(0, 0, 0, 0);
		m_boneInfo[BoneIndex].active = active;
		m_boneInfo[BoneIndex].replacedBy = replacedBy;
		m_boneInfo[BoneIndex].boneDirection = XMFLOAT4(0,0,1,0);
	}

	for (unsigned int i = 0; i < pNode->mNumChildren; i++)
	{
		LoadNodeHierarchy(pNode->mChildren[i], BoneIndex);
	}
	
}

void Mesh::DefineBoneDirection()
{
	for (unsigned int i = 0; i < m_boneInfo.size(); i++)
	{
		bool represents = false;
		XMVECTOR bonePosition = XMLoadFloat4(&XMFLOAT4(m_boneInfo[i].nodePosition.x, m_boneInfo[i].nodePosition.y, m_boneInfo[i].nodePosition.z, 0));
		float distance = 0;
		XMVECTOR furthestPoint = bonePosition;

		for (int k = 0; k < m_vertexCount; k++)
		{
			represents = false;
			float weight = 0;

			if ((int)m_vertices[k].boneIDs.x == i)
				weight = m_vertices[k].boneWeights.x;
			else if ((int)m_vertices[k].boneIDs.y == i)
				weight = m_vertices[k].boneWeights.y;
			else if ((int)m_vertices[k].boneIDs.z == i)
				weight = m_vertices[k].boneWeights.z;
			else if ((int)m_vertices[k].boneIDs.w == i)
				weight = m_vertices[k].boneWeights.w;

			if (weight >= m_vertices[k].boneWeights.x && weight >= m_vertices[k].boneWeights.y && weight >= m_vertices[k].boneWeights.z && weight >= m_vertices[k].boneWeights.w)
				represents = true;

			if (represents)
			{
				XMVECTOR pointPosition = XMLoadFloat4(&XMFLOAT4(m_vertices[k].position.x, m_vertices[k].position.y, m_vertices[k].position.z, 0));
				XMVECTOR currentDistance = XMVector3Length(pointPosition - bonePosition);


				XMFLOAT4 currentDis;
				XMStoreFloat4(&currentDis, currentDistance);
				
				if (currentDis.x > distance)
				{
					furthestPoint = pointPosition - bonePosition;
					distance = currentDis.x;
				}

			}
		}
		XMFLOAT4 boneDirection;
		XMStoreFloat4(&boneDirection, furthestPoint);
		m_boneInfo[i].boneDirection = boneDirection;
		m_boneInfo[i].branchLength = distance;
	}
}

void Mesh::ReduceBones()
{
	for (unsigned int i = 0; i < m_boneInfo.size(); i++)
	{
		int ownId = i;
		int parentId = m_boneInfo[i].parent;
		XMVECTOR ownPos;
		XMVECTOR parentPos;
		XMVECTOR boneOrientation;
		XMVECTOR parentOrientation;
		XMVECTOR difference;
		if (parentId >= 0)
		{
			parentPos = XMLoadFloat4(&XMFLOAT4(m_boneInfo[parentId].nodePosition.x, m_boneInfo[parentId].nodePosition.y, m_boneInfo[parentId].nodePosition.z, 0));
			parentOrientation = XMLoadFloat4(&m_boneInfo[parentId].boneDirection);
			
			ownPos = XMLoadFloat4(&XMFLOAT4(m_boneInfo[i].nodePosition.x, m_boneInfo[i].nodePosition.y, m_boneInfo[i].nodePosition.z, 0));
			
			boneOrientation = XMLoadFloat4(&m_boneInfo[i].boneDirection);

			boneOrientation = XMVector3Normalize(boneOrientation);
			parentOrientation = XMVector3Normalize(parentOrientation);
			difference = XMVector3Dot(boneOrientation, parentOrientation);

			float differenceResult = 0;
			XMStoreFloat(&differenceResult, difference);

			if (abs(differenceResult) > 0.98f)
			{
				m_boneInfo[i].replacedBy = parentId;
				m_boneInfo[i].active = 0;
				//transform children of removed bone
				for (int childId = 0; childId < m_boneInfo.size(); childId++)
				{
					if (m_boneInfo[childId].parent == ownId)
					{
						XMMATRIX childT = XMLoadFloat4x4(&m_boneInfo[childId].BoneTransform);
						XMMATRIX parentT = XMLoadFloat4x4(&m_boneInfo[ownId].BoneTransform);
						XMStoreFloat4x4(&m_boneInfo[childId].BoneTransform, XMMatrixMultiply(parentT, childT));
					}
				}
			}
		}
		
	}

	//find the real replacement in case the parent is also being replaced
	for (int i = 0; i < m_boneInfo.size(); i++)
	{
		int replacedBy = -1;
		replacedBy = m_boneInfo[i].replacedBy;
		int newReplacement = replacedBy;
		while (replacedBy >= 0)
		{
			replacedBy = m_boneInfo[replacedBy].replacedBy;
			if (replacedBy >= 0)
				newReplacement = replacedBy;
		}
		m_boneInfo[i].replacedBy = newReplacement;
	}

	//modify data for bones that are left
	for (int i = 0; i < m_boneInfo.size(); i++)
	{

		if (m_boneInfo[i].replacedBy >= 0)
		{
			//remove the bone
			for (int k = 0; k < m_vertexCount; k++)
			{
				if (m_vertices[k].boneIDs.x == (float)i)
					m_vertices[k].boneIDs.x = (float)m_boneInfo[i].replacedBy;
				if (m_vertices[k].boneIDs.y == (float)i)
					m_vertices[k].boneIDs.y = (float)m_boneInfo[i].replacedBy;
				if (m_vertices[k].boneIDs.z == (float)i)
					m_vertices[k].boneIDs.z = (float)m_boneInfo[i].replacedBy;
				if (m_vertices[k].boneIDs.w == (float)i)
					m_vertices[k].boneIDs.w = (float)m_boneInfo[i].replacedBy;
			}
			for (int j = 0; j < m_boneInfo.size(); j++)
			{
				if (m_boneInfo[j].parent == i)
				{
					m_boneInfo[j].parent = m_boneInfo[i].replacedBy;
				}
			}

			////DEBUGGING
			/*
			for (int k = 0; k < m_vertexCount; k++)
			{
				std::cout << "> " << m_vertices[k].boneIDs.x << " # " << m_vertices[k].boneIDs.y << " # " << m_vertices[k].boneIDs.z << " # " << m_vertices[k].boneIDs.w << std::endl;
			}
			*/
		}
	}
}

std::vector<Mesh::BoneInfoData>* Mesh::GetBoneInfo()
{
	return &m_boneInfo;
}

void Mesh::CalculateBonePositions()
{
}

void Mesh::VertexBoneData::AddBoneData(unsigned int BoneID, float Weight)
{
	for (unsigned int i = 0; i < 4; i++) {
		if (Weights[i] == 0.0) {
			IDs[i] = BoneID;
			Weights[i] = Weight;
			return;
		}
	}
}
