/////////////
// SHADOWS //
/////////////
struct PS_OUTPUT_SHADOW
{
    float4 Diffuse : SV_Target0;
};


PS_OUTPUT_SHADOW PSMainShadow(PixelInputType input) : SV_TARGET
{
	PS_OUTPUT_SHADOW output;
  
	output.Diffuse = float4(0,0.0f,0,0.95f)*float4(shadowColor.xyz,1);
    
    float cutoutValue = cutoutTex.Sample(SampleType, input.tex).r;
    if (cutoutValue<0.3f)
        output.Diffuse = float4(0,0,0,0.95f);
    return output;
}


PS_OUTPUT_SHADOW PSMainTranslucency(PixelInputType input) : SV_TARGET
{
	PS_OUTPUT_SHADOW output;
    
    float3 finalTranslucency;
    float3 intensity;
    
    float cutoutValue = cutoutTex.Sample(SampleType, input.tex).r;
    if (cutoutValue<0.25f)
    {
        output.Diffuse = float4(1,0,0,0.0f);
        clip(-1);
        return output;
    }
    
    intensity = translucencyTex.Sample(SampleType, input.tex).xyz * translucencyColor.xyz;
    if (length(intensity.xyz) < 0.001f)
    {
        output.Diffuse = float4(0,0,0,1.0f);
        //clip(-1);
        return output;
    }

    float diffuse = dot(input.normal,float3(0.47115f,0.606024f, -0.640899f));//float3(0.75873f,0.582523f,-1.981872f)); 
    //albedoTex.Sample(SampleType, input.tex).xyz*
    float3 lightBleedThrough = float3(1.0f,1.0f,0.7f);
    finalTranslucency=intensity*diffuse;
    //finalTranslucency=diffuse.xxx;
    //finalTranslucency = float3(diffuse,diffuse,diffuse);
    float3 eyeVec = float3(viewMatrix._13, viewMatrix._23, viewMatrix._33);//normalize(input.position.xyz-eye.xyz);
    //finalTranslucency = float3(0.41f,0,0);
    output.Diffuse = float4(finalTranslucency,1);
    
    
    //output.Diffuse = float4(1,0.9,0.86,1);
    //if (dot(input.normal,eyeVec) < 0.0)
    //   output.Diffuse = float4(0,0,0,1);
    //output.Diffuse = float4(0,0,1,1);
    return output;
}
