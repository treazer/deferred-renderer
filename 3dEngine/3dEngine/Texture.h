#pragma once
#include <d3d11_1.h>

#include "WICTextureLoader.h"

#include <string>
using namespace std;

class Texture
{
public:
	Texture(void);
	~Texture(void);

	bool Initialize(ID3D11Device* device, ID3D11DeviceContext* context, WCHAR* fileName, bool sRGB = true);

	ID3D11ShaderResourceView* GetTexture();
	string GetName();

	int GetWidth();
	int GetHeight();

private:
	ID3D11ShaderResourceView* m_texture;
	string m_name;

	int m_width;
	int m_height;
};
