/////////////
// GLOBALS //
/////////////

static const float PI = 3.14159265f;
static const float2 SCREEN_RESOLUTION = float2(1280,720);
static const matrix <float, 4, 4> identityMatrix = {  1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, };
//Source: http://www.geeks3d.com/20100628/3d-programming-ready-to-use-64-sample-poisson-disc/
static float2 poissonDisk[64] =
{
        float2(-0.613392, 0.617481),float2(0.170019, -0.040254),float2(-0.299417, 0.791925),float2(0.645680, 0.493210),float2(-0.651784, 0.717887),float2(0.421003, 0.027070),float2(-0.817194, -0.271096),float2(-0.705374, -0.668203),float2(0.977050, -0.108615),float2(0.063326, 0.142369),float2(0.203528, 0.214331),float2(-0.667531, 0.326090),float2(-0.098422, -0.295755),float2(-0.885922, 0.215369),float2(0.566637, 0.605213),float2(0.039766, -0.396100),float2(0.751946, 0.453352),float2(0.078707, -0.715323),float2(-0.075838, -0.529344),float2(0.724479, -0.580798),float2(0.222999, -0.215125),float2(-0.467574, -0.405438),float2(-0.248268, -0.814753),float2(0.354411, -0.887570),float2(0.175817, 0.382366),float2(0.487472, -0.063082),float2(-0.084078, 0.898312),float2(0.488876, -0.783441),float2(0.470016, 0.217933),float2(-0.696890, -0.549791),float2(-0.149693, 0.605762),float2(0.034211, 0.979980),float2(0.503098, -0.308878),float2(-0.016205, -0.872921),float2(0.385784, -0.393902),float2(-0.146886, -0.859249),float2(0.643361, 0.164098),float2(0.634388, -0.049471),float2(-0.688894, 0.007843),float2(0.464034, -0.188818),float2(-0.440840, 0.137486),float2(0.364483, 0.511704),float2(0.034028, 0.325968),float2(0.099094, -0.308023),float2(0.693960, -0.366253),float2(0.678884, -0.204688),float2(0.001801, 0.780328),float2(0.145177, -0.898984),float2(0.062655, -0.611866),float2(0.315226, -0.604297),float2(-0.780145, 0.486251),float2(-0.371868, 0.882138),float2(0.200476, 0.494430),float2(-0.494552, -0.711051),float2(0.612476, 0.705252),float2(-0.578845, -0.768792),float2(-0.772454, -0.090976),float2(0.504440, 0.372295),float2(0.155736, 0.065157),float2(0.391522, 0.849605),float2(-0.620106, -0.328104),float2(0.789239, -0.419965),float2(-0.545396, 0.538133),float2(-0.178564, -0.596057)
};
//////////////
// CBUFFERS //
//////////////

cbuffer FrameDataBuffer : register(b0)
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projectionMatrix;
    matrix viewMatrixOld;
    matrix projectionMatrixOld;
    float4 eyePosition;
    float4 wind;
    float4 timeT;
    float4 reconstructionValuesBuffer;
};

cbuffer DeferredBufferType : register(b1)
{
    matrix lightViewMatrix;
    matrix lightOrthoMatrix;
    matrix worldMatrixDeferred;
    matrix viewMatrixDeferred;
    matrix projectionMatrixDeferred;
    matrix inverseProjection;
    matrix inverseView;
    float4 sunColor;
    float4 sunDirection;
    float4 sunPosition;
    float4 eye;
    float4 reconstructionValues;
};

cbuffer RendererBuffer: register(b2)
{
    float4 baseColor;
    float4 shadowColor;
    float4 translucencyColor;
    float tessellationAmount;
    float emission;
    float displacementStrength;
    float materialId;
    float normalStrength;
    float uvTilingX;
    float uvTilingY;
    float isMetal;
	float mulRoughness;
	float mulMetal;
	float a;
	float b;
};

cbuffer ObjectBuffer: register(b3)
{
    matrix treeBones[256];
    matrix treeBoneOffsets[256];
    float4 treeBoneHeights[256];
    float4 treeForces[256];
};

cbuffer TreeBuffer: register(b5)
{
    float leafStrength;
    float leafBranchInfluence;
    float leafStiffness;
    float treeA;
};

//////////////
// TYPEDEFS //
//////////////

struct VertexInputType
{
    float4 position : POSITION;
    float4 color : COLOR;
    float4 tex : TEXCOORD0;
    float4 normal : NORMAL;
    float4 tangent : TANGENT;
    float4 bitangent : BITANGENT;
    float4 boneWeights : COLOR1;
    float4 boneIDs : COLOR2;
};

struct PixelInputType
{
    float4 position : SV_Position;
    float4 color : COLOR;
    float2 tex : TEXCOORD0;
    float2 tex2 : TEXCOORD1;
    float4 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    float4 positionOld : POSITION;
    float4 positionNew : POSITION2;
};

struct HullInputType
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
    float2 tex : TEXCOORD0;
    float2 tex2 : TEXCOORD1;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    float tessellationStrength : TESSFACTOR0;
    float dispStrength : TESSFACTOR1;
};

struct HullOutputType
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
    float2 tex : TEXCOORD0;
    float2 tex2 : TEXCOORD1;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    float tessellationStrength : TESSFACTOR0;
    float dispStrength : TESSFACTOR1;
};

struct ConstantOutputType
{
    float edges[3] : SV_TessFactor;
    float inside : SV_InsideTessFactor;
};

struct PS_OUTPUT
{
    float4 GBuffer1 : SV_Target0; //color r g b roughness -metal
    float2 GBuffer2 : SV_Target1; //normal x y
    float2 GBuffer3 : SV_Target2; //velocity x y
};

matrix CreateMatrixFromQuaternion(float4 quat);
float4 CreateRotationQuaternion(float3 vecA, float3 vecB);
float3 RotateVectorByQuaternion(float3 vectorPoint, float4 rotation);


/*
    Creates a rotation matrix based on a quaternion.
    From: http://www.gamasutra.com/view/feature/131686/rotating_objects_using_quaternions.php?page=2
*/
matrix CreateMatrixFromQuaternion(float4 quat){
    matrix m;
    float wx, wy, wz, xx, yy, yz, xy, xz, zz, x2, y2, z2;


    // calculate coefficients
    x2 = quat.x + quat.x; y2 = quat.y + quat.y;
    z2 = quat.z + quat.z;
    xx = quat.x * x2; xy = quat.x * y2; xz = quat.x * z2;
    yy = quat.y * y2; yz = quat.y * z2; zz = quat.z * z2;
    wx = quat.w * x2; wy = quat.w * y2; wz = quat.w * z2;


    m._11 = 1.0 - (yy + zz); m._21 = xy - wz;
    m._31 = xz + wy; m._41 = 0.0;

    m._12 = xy + wz; m._22 = 1.0 - (xx + zz);
    m._32 = yz - wx; m._42 = 0.0;


    m._13 = xz - wy; m._23 = yz + wx;
    m._33 = 1.0 - (xx + yy); m._43 = 0.0;


    m._14 = 0; m._24 = 0;
    m._34 = 0; m._44 = 1;
    
    return m;

}


/*
    Creates a quaternion for the rotation from vector A to vector B.
*/
float4 CreateRotationQuaternion(float3 vecA, float3 vecB)
{

    float norm_u_norm_v = sqrt(dot(vecA, vecA) * dot(vecB, vecB));
    float real_part = norm_u_norm_v + dot(vecA, vecB);
    float3 w;

    if (real_part < 1.e-6f * norm_u_norm_v)
    {
        /* If u and v are exactly opposite, rotate 180 degrees
         * around an arbitrary orthogonal axis. Axis normalisation
         * can happen later, when we normalise the quaternion. */
        real_part = 0.0f;
        w = abs(vecA.x) > abs(vecA.z) ? float3(-vecA.y, vecA.x, 0.f)
                                : float3(0.f, -vecA.z, vecA.y);
    }
    else
    {
        /* Otherwise, build quaternion the standard way. */
        w = cross(vecA, vecB);
    }

    return normalize(float4(w.x, w.y, w.z,real_part));
}


/*
    Rotates a vector based on a given quaternion. Returns new vector position.
*/
float3 RotateVectorByQuaternion(float3 vectorPoint, float4 rotation)
{
    float3 vq = rotation.xyz;
    float3 vqcrossv = cross(vq, vectorPoint);
    float3 wvplusvqcrossv = rotation.w * vectorPoint + vqcrossv;
    float3 vqcrosswvplusvqcrossv = cross(vq, wvplusvqcrossv);
    float3 res = rotation.w * wvplusvqcrossv + vqcrosswvplusvqcrossv;
    res = dot(vq, vectorPoint) * vq + res;
    return res;
}

float random( float2 p )
{
  // We need irrationals for pseudo randomness.
  // Most (all?) known transcendental numbers will (generally) work.
  const float2 r = float2(
    23.1406926327792690,  // e^pi (Gelfond's constant)
     2.6651441426902251); // 2^sqrt(2) (Gelfond–Schneider constant)
  return frac( cos( fmod( 123456789., 1e-7 + 256. * dot(p,r) ) ) );  
}

float LambertianDiffuse(float3 light, float3 pNormal)
{
    return dot(pNormal, light);
}
