#pragma once

#include <d3d11_1.h>
#include <directxmath.h>
#include "Component.h"

using namespace std;
using namespace DirectX;

class Scene;
class GameObject;
class Transform : public Component
{
public:
	Transform();
	~Transform();

	void Initialize(GameObject* gameObject);
	void SetPosition(float x, float y, float z);
	void SetMatrix(XMFLOAT4X4 matrix);

	void RotateX(float degrees);
	void RotateY(float degrees);
	void RotateZ(float degrees);
	void Translate(float x, float y, float z);

	XMFLOAT3 GetPosition();
	XMFLOAT3 GetDirection();

	Transform* GetParent();
	void SetParent(Transform* parent);

	string GetName();
	GameObject* GetGameObject();
	XMFLOAT4X4 GetMatrix();
	void Update(Scene * scene);
	void Render(Scene * scene);
private:

	Transform* m_parent;
	XMFLOAT4X4 m_transformMatrix;
};
