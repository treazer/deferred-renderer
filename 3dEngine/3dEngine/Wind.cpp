#include "Wind.h"
#include <iostream>
#include <vector>
#include "Timer.h"

using namespace std;

Wind::Wind()
{
}


Wind::~Wind()
{
}

XMFLOAT4 Wind::SampleWindForPosition(XMFLOAT3 position)
{
	XMFLOAT4 finalWind;

	float pi = 3.1415926f;
	float time = Timer::GetTimeSinceStart()*0.01f;
	
	float termA = cos(time*pi)*cos(time*pi * 3)*cos(time*pi * 5)*cos(time * 7 * pi) + sin(time * 25 * pi)*1.0f;
	float termB = cos(time*pi)*cos(time*pi * 5)*cos(time*pi * 5)*cos(time * 12 * pi) + sin(time * 35 * pi)*0.9f;
	finalWind = XMFLOAT4(1.0f+termA, 0, termB, 2.4f);

	return finalWind;
}
