#include "Transform.h"
#include "Scene.h"
#include <iostream>

using namespace std;

Transform::Transform()
{
	m_parent = 0;
	m_transformMatrix = XMFLOAT4X4();
}


Transform::~Transform()
{
}

void Transform::Initialize(GameObject* gameObject)
{
	Component::m_gameObject = gameObject;
	XMStoreFloat4x4(&m_transformMatrix, XMMatrixIdentity());
}

void Transform::SetPosition(float x, float y, float z)
{
	m_transformMatrix._41 = x;
	m_transformMatrix._42 = y;
	m_transformMatrix._43 = z;
}

void Transform::SetMatrix(XMFLOAT4X4 matrix)
{
	m_transformMatrix = matrix;
}

void Transform::RotateX(float degrees)
{
	XMMATRIX matrix = XMMatrixRotationX(degrees);
	XMStoreFloat4x4(&m_transformMatrix, XMMatrixMultiply(matrix, XMLoadFloat4x4(&m_transformMatrix)));
}

void Transform::RotateY(float degrees)
{
	XMMATRIX matrix = XMMatrixRotationY(degrees);
	XMStoreFloat4x4(&m_transformMatrix, XMMatrixMultiply(matrix, XMLoadFloat4x4(&m_transformMatrix)));
}

void Transform::RotateZ(float degrees)
{
	XMMATRIX matrix = XMMatrixRotationZ(degrees);
	XMStoreFloat4x4(&m_transformMatrix, XMMatrixMultiply(matrix, XMLoadFloat4x4(&m_transformMatrix)));
}

void Transform::Translate(float x, float y, float z)
{
	m_transformMatrix._41 += x;
	m_transformMatrix._42 += y;
	m_transformMatrix._43 += z;
}

XMFLOAT3 Transform::GetPosition()
{
	return XMFLOAT3(GetMatrix()._41, GetMatrix()._42, GetMatrix()._43);
}

XMFLOAT3 Transform::GetDirection()
{
	return XMFLOAT3(GetMatrix()._31, GetMatrix()._32, GetMatrix()._33);
}

Transform * Transform::GetParent()
{
	return m_parent;
}

void Transform::SetParent(Transform * parent)
{
	m_parent = parent;
}

string Transform::GetName()
{
	return Component::m_name;
}

GameObject* Transform::GetGameObject()
{
	return m_gameObject;
}

XMFLOAT4X4 Transform::GetMatrix()
{
	return m_transformMatrix;
}

void Transform::Update(Scene * scene)
{

}

void Transform::Render(Scene * scene)
{
}
