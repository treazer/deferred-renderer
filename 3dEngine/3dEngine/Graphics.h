#pragma once

#include "DXManager.h"
#include "SystemDefs.h"

class Graphics
{
public:
	Graphics(void);
	~Graphics(void);

	bool InitializeDX(HWND hwnd);
	void Initialize();
	void BeginScene(float r, float g, float b, float a);
	void EndScene();

	void EnableAlphaBlending(bool enable);
	void EnableZBuffer(bool enable);
	void SetBackgroundZBuffer();
	void SetRasterStateWireframe(bool enable);
	void SetRasterStateBackside(bool enable);
	void SetRasterStateSkybox(bool enable);
	void TranslucencyState(bool enable);
	void SetViewport(int screenWidth, int screenHeight);
	DXManager* GetDXManager();
	HWND GetHwnd();
	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetDeviceContext();
	ID3D11DepthStencilView* GetDepthStencilView();

	void SetHwnd(HWND hwnd);

private:
	DXManager* m_dxManager;
	HWND m_hwnd;
};
