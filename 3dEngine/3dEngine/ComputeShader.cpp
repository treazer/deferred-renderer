
#include "ComputeShader.h"
#include <string>
#include <iostream>
#include <d3dcompiler.h>

ComputeShader::ComputeShader()
{
	m_samplerState = nullptr;
	m_computeShader = nullptr;
	m_initialized = false;
	m_name = 0;
}


ComputeShader::~ComputeShader()
{
	if (m_samplerState)
	{
		m_samplerState->Release();
		m_samplerState = NULL;
	}
}

bool ComputeShader::Initialize(ID3D11Device * device, HWND hwnd, std::string shaderFileName)
{
	HRESULT result;

	ID3DBlob *pByteCodeBlob = NULL;
	ID3DBlob *pErrorBlob = NULL;
	ID3DBlob *pCompressedByteCodeBlob = NULL;
	ID3D11ComputeShader *pCompiledComputeShader = NULL;

	wchar_t m1[] = L"shaders/computeShader.hlsl";

	result = D3DCompileFromFile(m1, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "CSMain", "cs_5_0", 0, 0, &pByteCodeBlob, &pErrorBlob);

	if (FAILED(result))
	{
		MessageBoxA(NULL, (char *)pErrorBlob->GetBufferPointer(), "Error", MB_OK | MB_ICONERROR);
		return false;
	}
	
	result = device->CreateComputeShader(pByteCodeBlob->GetBufferPointer(), pByteCodeBlob->GetBufferSize(), NULL, &pCompiledComputeShader);

	if (FAILED(result))
	{
		MessageBoxA(NULL, "CreateComputerShader() failed", "Error", MB_OK | MB_ICONERROR);
		return false;
	}

	m_computeShader = pCompiledComputeShader;

	D3D11_BUFFER_DESC treeBufferDesc;

	//setup the matrix buffer description
	treeBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	treeBufferDesc.ByteWidth = sizeof(Shader::TreeBufferType);
	treeBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	treeBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	treeBufferDesc.MiscFlags = 0;
	treeBufferDesc.StructureByteStride = 0;

	result = device->CreateBuffer(&treeBufferDesc, NULL, &m_treeBuffer);
	if (FAILED(result))
	{
		std::cout << "!! failed to create tree buffer desc" << std::endl;
		return false;
	}

	return true;
}

bool ComputeShader::InitializeSamplerState(ID3D11Device * device)
{
	return true;
}

char * ComputeShader::GetName()
{
	return m_name;
}

bool ComputeShader::IsInitialized()
{
	return false;
}

ID3D11ComputeShader * ComputeShader::GetComputeShader()
{
	return m_computeShader;
}


void ComputeShader::Begin(ID3D11DeviceContext * deviceContext, int indexCount)
{

}

void ComputeShader::End(ID3D11DeviceContext * deviceContext)
{

}


ID3D11Buffer* ComputeShader::CreateAndCopyToDebugBuf(ID3D11Device* pDevice, ID3D11DeviceContext* pd3dImmediateContext, ID3D11Buffer* pBuffer)
{
	ID3D11Buffer* debugbuf = nullptr;

	D3D11_BUFFER_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	pBuffer->GetDesc(&desc);
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	desc.Usage = D3D11_USAGE_STAGING;
	desc.BindFlags = 0;
	desc.MiscFlags = 0;
	if (SUCCEEDED(pDevice->CreateBuffer(&desc, nullptr, &debugbuf)))
	{
		pd3dImmediateContext->CopyResource(debugbuf, pBuffer);
	}

	return debugbuf;
}

HRESULT ComputeShader::CreateStructuredBuffer(ID3D11Device* pDevice, UINT uElementSize, UINT uCount, void* pInitData, ID3D11Buffer** ppBufOut)
{
	*ppBufOut = nullptr;

	D3D11_BUFFER_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
	desc.ByteWidth = uElementSize * uCount;
	desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	desc.StructureByteStride = uElementSize;

	if (pInitData)
	{
		D3D11_SUBRESOURCE_DATA InitData;
		InitData.pSysMem = pInitData;
		return pDevice->CreateBuffer(&desc, &InitData, ppBufOut);
	}
	else
		return pDevice->CreateBuffer(&desc, nullptr, ppBufOut);
}

HRESULT ComputeShader::CreateBufferUAV(ID3D11Device* pDevice, ID3D11Buffer* pBuffer, ID3D11UnorderedAccessView** ppUAVOut)
{
	D3D11_BUFFER_DESC descBuf;
	ZeroMemory(&descBuf, sizeof(descBuf));
	pBuffer->GetDesc(&descBuf);

	D3D11_UNORDERED_ACCESS_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	desc.Buffer.FirstElement = 0;

	desc.Format = DXGI_FORMAT_UNKNOWN;      // Format must be must be DXGI_FORMAT_UNKNOWN, when creating a View of a Structured Buffer
	desc.Buffer.NumElements = descBuf.ByteWidth / descBuf.StructureByteStride;

	return pDevice->CreateUnorderedAccessView(pBuffer, &desc, ppUAVOut);
}


HRESULT ComputeShader::CreateBufferSRV(ID3D11Device* pDevice, ID3D11Buffer* pBuffer, ID3D11ShaderResourceView** ppSRVOut)
{
	D3D11_BUFFER_DESC descBuf;
	ZeroMemory(&descBuf, sizeof(descBuf));
	pBuffer->GetDesc(&descBuf);

	D3D11_SHADER_RESOURCE_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
	desc.BufferEx.FirstElement = 0;

	desc.Format = DXGI_FORMAT_UNKNOWN;
	desc.BufferEx.NumElements = descBuf.ByteWidth / descBuf.StructureByteStride;

	return pDevice->CreateShaderResourceView(pBuffer, &desc, ppSRVOut);
}

bool ComputeShader::SetShaderParameters(ID3D11DeviceContext* deviceContext, Shader::TreeBufferType treeBuffer)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	Shader::TreeBufferType* dataPtr;
	unsigned int bufferNumber;

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_treeBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	if (FAILED(result))
	{
		return false;
	}

	// Get a pointer to the data in the constant buffer.
	dataPtr = (Shader::TreeBufferType*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	dataPtr->worldPosition = treeBuffer.worldPosition;
	dataPtr->leafDimension = treeBuffer.leafDimension;
	dataPtr->orientationFlattening = treeBuffer.orientationFlattening;
	dataPtr->leafGravity = treeBuffer.leafGravity;
	dataPtr->numLeaves = treeBuffer.numLeaves;
	dataPtr->numVertices = treeBuffer.numVertices;
	dataPtr->minimumDistanceLeaves = treeBuffer.minimumDistanceLeaves;
	dataPtr->distancePadding = treeBuffer.distancePadding;
	dataPtr->leafStrength = treeBuffer.leafStrength;
	dataPtr->leafBranchInfluence = treeBuffer.leafBranchInfluence;
	dataPtr->leafStiffness = treeBuffer.leafStiffness;
	dataPtr->doubleSided = treeBuffer.doubleSided;
	dataPtr->staticBranches = treeBuffer.staticBranches;

	// Unlock the constant buffer.
	deviceContext->Unmap(m_treeBuffer, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;

	// Set the constant buffer in the compute shader with the updated values.
	deviceContext->CSSetConstantBuffers(bufferNumber, 1, &m_treeBuffer);

	return true;
}