TextureCube environmentTexture : register(t0);
Texture2D noiseTexture : register(t1);
SamplerState SampleType;

static const float PI = 3.14159265f;
static const float2 SCREEN_RESOLUTION = float2(1280,720);

//////////////
// CBUFFERS //
//////////////

cbuffer FrameDataBuffer : register(b0)
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projectionMatrix;
    matrix viewMatrixOld;
    matrix projectionMatrixOld;
    float4 eyePosition;
    float4 wind;
    float4 timeT;
    float4 reconstructionValuesBuffer;
};

cbuffer DeferredBufferType : register(b1)
{
    matrix lightViewMatrix;
    matrix lightOrthoMatrix;
    matrix worldMatrixDeferred;
    matrix viewMatrixDeferred;
    matrix projectionMatrixDeferred;
    float4 sunColor;
    float4 sunDirection;
    float4 sunPosition;
    float4 eye;
    float4 reconstructionValues;
};

cbuffer RendererBuffer: register(b2)
{
    float4 baseColor;
    float4 shadowColor;
    float4 translucencyColor;
    float tessellationAmount;
    float emission;
    float displacementStrength;
    float materialId;
    float normalStrength;
    float uvTilingX;
    float uvTilingY;
    float isMetal;
	float mulRoughness;
	float mulMetal;
	float a;
	float b;
};

cbuffer ObjectBuffer: register(b3)
{
    matrix treeBones[256];
    matrix treeBoneOffsets[256];
    float4 treeBoneHeights[256];
};


//////////////
// TYPEDEFS //
//////////////
struct VertexInputType
{
    float4 position : POSITION;
    float4 color : COLOR;
    float4 tex : TEXCOORD0;
    float4 normal : NORMAL;
    float4 tangent : TANGENT;
    float4 bitangent : BITANGENT;
    float4 boneWeights : COLOR1;
    float4 boneIDs : COLOR2;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
    float2 tex : TEXCOORD0;
    float4 normal : TEXCOORD1;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    float4 positionOld : POSITION;
    float4 positionNew : POSITION2;
    float4 wPosition : POSITION3;
};

struct HullInputType
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
    float2 tex : TEXCOORD0;
    float3 normal : TEXCOORD1;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
};

struct HullOutputType
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
    float2 tex : TEXCOORD0;
    float3 normal : TEXCOORD1;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
};

struct ConstantOutputType
{
    float edges[3] : SV_TessFactor;
    float inside : SV_InsideTessFactor;
};

struct PS_OUTPUT
{
    float4 GBuffer1 : SV_Target0; //color r g b roughness -metal
    float2 GBuffer2 : SV_Target1; //normal x y
    float2 GBuffer3 : SV_Target2; //velocity x y
};
float random( float2 p )
{
  // We need irrationals for pseudo randomness.
  // Most (all?) known transcendental numbers will (generally) work.
  const float2 r = float2(
    23.1406926327792690,  // e^pi (Gelfond's constant)
     2.6651441426902251); // 2^sqrt(2) (Gelfond–Schneider constant)
  return frac( cos( fmod( 123456789., 1e-7 + 256. * dot(p,r) ) ) );  
}

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelInputType VSMain(VertexInputType input)
{
    //PixelInputType output;
    PixelInputType output; //switch to this one
    
    // Change the position vector to be 4 units for proper matrix calculations.
    input.position *=50;
    input.position.w = 1.0f;
    float4 vec;
    
    // Calculate the position of the vertex against the world, view, and projection matrices.
    vec = mul(input.position, worldMatrix);
    vec = mul(vec, viewMatrix); 
    vec = mul(vec, projectionMatrix);
    
    output.position = vec;
    
    // Velocity calculation
    if (reconstructionValuesBuffer.x==0)
        output.position -=float4(0,(1/SCREEN_RESOLUTION.y)*2.0f*output.position.w,0,0);
    output.positionNew = output.position;
    
    output.positionOld = mul(input.position, worldMatrix);
    output.positionOld = mul(output.positionOld, viewMatrixOld);
    output.positionOld = mul(output.positionOld, projectionMatrixOld);
    if (reconstructionValuesBuffer.x==0)
        output.positionOld -=float4(0,(1/SCREEN_RESOLUTION.y)*2.0f*output.position.w,0,0);
    
    output.wPosition = mul(input.position, worldMatrix);
    
    output.color = float4(1,0,0,1);
    output.normal = float4(input.normal.xyz,0);
    output.tangent = input.tangent;
    output.bitangent = input.bitangent;
    output.tex = input.tex;
    output.tex.y = 1-output.tex.y;
    return output;

}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
PS_OUTPUT PSMain(PixelInputType input) : SV_TARGET
{
	PS_OUTPUT output;
    //textureColor = environmentTexture.Sample(SampleType, input.wPosition.xyz); //old implementation
    float4 textureColor = float4((53.0f/256.0f), (147.0f/256.0f), (231.0f/256.0f), 1);
    float3 sunOrientation = float3(0.75873f,0.582523f,-1.981872f);// float3(0.3f, 0.579426f, 0.877583f);
    float4 cenitColor = float4(1,1,1,1);
    float4 sunColor = float4(1.0,0.7,0.5,1);
    float4 sunColor2 = float4(1.0,0.55,0.37,1);
    float4 horizonColor = float4(0.792f,0.835f,0.8627f,1);
    
    float3 orientation = normalize(input.wPosition.xyz); //-eyePosition.xyz
    
    float sunStrength = saturate(dot(normalize(sunOrientation),orientation));
    float sunStrength2 = saturate(dot(normalize(sunOrientation),orientation));
    //sunStrength = pow(sunStrength,722)*60.7f;
    sunStrength = sunStrength;//-32000*pow(1*sunStrength,2)+1;
    if (sunStrength<0.9995) sunStrength = 0;
    sunStrength *=128;
    sunStrength2 =0;
    sunStrength2 = pow(sunStrength2,2)*0.5f;
    //sunStrength += 0.4+pow(sunStrength, 142)*1.7f;
    
    float horizon = saturate(1-dot(float3(0,1,0),orientation)-0.03f); //added +0.3f

	float3 col = lerp(float3(0.83922,0.78039,0.75294), float3(0.09235,0.3411,0.596), orientation.y * 0.5 + 0.5); // float3(0.28235,0.39608,0.5215)

	//col += 1.1*float3(1.0,0.7,0.5)*pow(sun, 500);
    //col += 1.2*float3(1.0,0.7,0.5)*pow(sun, 12.0);
    
    //output.Diffuse.xyz = clouds(orientation,col);//+(sunColor*sunStrength).xyz;
    //output.Diffuse.xyz = col;
    //output.Diffuse.x = 1.0f;
    //output.Diffuse.y = 1.0f;
    //output.Diffuse = float4(input.wPosition.xyz,1);//+(sunColor*sunStrength);
    
    //col = lerp(col, col+(sunColor*sunStrength), sunStrength);
    col = lerp(col, col+(sunColor*sunStrength), sunStrength);
    float3 finalColor = lerp(col, col+(sunColor2*sunStrength2), sunStrength2);
    
    float2 finalNormal = float2(0,0);
    float roughness = 3.0f;
    float metalness = 0;
    
    output.GBuffer1 = float4(finalColor,roughness);
    output.GBuffer2 = float2(finalNormal);
    output.GBuffer3 = float2(0,0);
    
    return output;
}

PS_OUTPUT PSMainShadow(PixelInputType input) : SV_TARGET
{
    PS_OUTPUT output;
    output.GBuffer1 = float4(1,1,1,1);
    return output;
}

PS_OUTPUT PSMainTranslucency(PixelInputType input) : SV_TARGET
{
    PS_OUTPUT output;
    output.GBuffer1 = float4(1,1,1,1);
    return output;
}


