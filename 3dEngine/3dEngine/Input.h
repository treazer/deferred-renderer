#pragma once
//Pre-Processing Directive
#define DIRECTINPUT_VERSION 0x0800//removes annoying message defaulting to version 8

#include <dinput.h>

#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")

class Input
{
public:
	Input(void);
	~Input(void);

	bool Initialize(HINSTANCE hInstance, HWND hwnd, int screenWidth, int screenHeight);
	bool Update();

	bool IsKeyDown(unsigned int key);
	bool IsKeyHit(unsigned int key);
	void GetMousePosition(int& x, int& y);
	void GetLastMousePosition(int& x, int& y);
	void GetMouseChange(int& x, int& y);

private:
	const static int s_NumKeys = 256;

	bool ReadKeyboard();
	bool ReadMouse();
	void ProcessInput();

	IDirectInput8 * m_directInput;
	IDirectInputDevice8* m_keyboard;
	IDirectInputDevice8* m_mouse;
	DIMOUSESTATE m_mouseState;

	bool m_keys[s_NumKeys];
	bool m_prevKeys[s_NumKeys];

	int m_screenWidth;
	int m_screenHeight;
	int m_mouseX;
	int m_mouseY;
	int m_lastMouseX;
	int m_lastMouseY;
};
