#pragma once

#include "Shader.h"
//#include <d3d11_1.h>
#include <d3d11.h>
#include <string>
class ComputeShader
{
public:
	ComputeShader();
	~ComputeShader();

	void Begin(ID3D11DeviceContext* deviceContext, int indexCount);
	void End(ID3D11DeviceContext* deviceContext);
	bool Initialize(ID3D11Device* device, HWND hwnd, std::string shaderFileName);
	bool InitializeSamplerState(ID3D11Device * device);
	char* GetName();
	bool IsInitialized();
	ID3D11ComputeShader* GetComputeShader();
	ID3D11Buffer* CreateAndCopyToDebugBuf(ID3D11Device* pDevice, ID3D11DeviceContext* pd3dImmediateContext, ID3D11Buffer* pBuffer);
	HRESULT CreateStructuredBuffer(ID3D11Device* pDevice, UINT uElementSize, UINT uCount, void* pInitData, ID3D11Buffer** ppBufOut);
	HRESULT CreateBufferUAV(ID3D11Device* pDevice, ID3D11Buffer* pBuffer, ID3D11UnorderedAccessView** ppUAVOut);
	HRESULT CreateBufferSRV(ID3D11Device* pDevice, ID3D11Buffer* pBuffer, ID3D11ShaderResourceView** ppSRVOut);
	bool SetShaderParameters(ID3D11DeviceContext* deviceContext, Shader::TreeBufferType treeBuffer);
private:

	ID3D11SamplerState* m_samplerState;
	ID3D11ComputeShader* m_computeShader;
	bool m_initialized;
	char* m_name;

	ID3D11Buffer* m_treeBuffer;
};

