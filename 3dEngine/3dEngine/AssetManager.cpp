#include "AssetManager.h"
#include "StandardShader.h"
#include "Scene.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
AssetManager* AssetManager::m_instance = NULL;

AssetManager::AssetManager(void)
{
	m_textures.reserve(32);
	m_shaders.reserve(32);
	m_meshes.reserve(32);
}

AssetManager::~AssetManager(void)
{
	while (!m_textures.empty())
	{
		delete m_textures[0];

		m_textures.erase(m_textures.begin());
	}

	while (!m_shaders.empty())
	{
		m_shaders.erase(m_shaders.begin());
	}
}

void AssetManager::LoadShader(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName, bool usesTessellation)
{
	Shader * shader = new Shader(device,hwnd,shaderFileName, usesTessellation);
	Shader* shaderCheck = GetShaderByName((char*)shader->GetName().c_str());

	if (shaderCheck==NULL)
		m_shaders.push_back(shader);
	else
	{
		for (int i = 0; i < (int)m_shaders.size(); ++i)
		{
			Shader * shader = m_shaders[i];
			string resourceShaderName = shader->GetName();
			if (!strcmp((char*)shader->GetName().c_str(), resourceShaderName.c_str()))
			{
				m_shaders[i] = shader;
			}
		}
	}
}

void AssetManager::LoadStandardShader(ID3D11Device * device, HWND hwnd, WCHAR * shaderFileName, bool usesTessellation)
{

	StandardShader * shader = new StandardShader(device, hwnd, shaderFileName, usesTessellation);
	Shader* shaderCheck = GetShaderByName((char*)shader->GetName().c_str());

	if (shaderCheck == NULL)
	{
		m_shaders.push_back(shader);
	}
	else
	{
		for (int i = 0; i < (int)m_shaders.size(); ++i)
		{
			Shader * shader2 = m_shaders[i];
			string resourceShaderName = shader2->GetName();
			if (!strcmp((char*)shader->GetName().c_str(), resourceShaderName.c_str()))
			{
				m_shaders[i]->Initialize(device, hwnd, shaderFileName, usesTessellation);
			}
		}
	}
}

void AssetManager::LoadTexture(ID3D11Device* device, ID3D11DeviceContext* context, WCHAR* textureFileName, bool sRGB)
{
	Texture* texture = new Texture();
	
	
	if (!texture->Initialize(device, context, textureFileName, sRGB))
	{
		delete texture;

		wcout << "Unable To Load: " << textureFileName << endl;

		return;
	}
	if (GetTextureByName((char*)texture->GetName().c_str()) == NULL)
	{
		m_textures.push_back(texture);
	}
	else
	{
		for (int i = 0; i < (int)m_textures.size(); ++i)
		{
			Texture * texture2 = m_textures[i];
			string resourceName = texture2->GetName();
			if (!strcmp((char*)texture->GetName().c_str(), resourceName.c_str()))
			{
				m_textures[i]->Initialize(device, context, textureFileName, sRGB);
			}
		}
	}
}

void AssetManager::LoadMesh(ID3D11Device* device, string meshFileName)
{

	Mesh* mesh = new Mesh();

	if (!mesh->Initialize(device,100,false))
	{
		delete mesh;

		cout << "Unable To Load " << meshFileName << endl;

		return;
	}

	m_meshes.push_back(mesh);
}

Shader* AssetManager::GetShaderByName(char* shaderName)
{
	for (int i = 0; i < (int)m_shaders.size(); ++i)
	{
		Shader* shader = m_shaders[i];
		string resourceShaderName = shader->GetName();
		if (!strcmp(shaderName, resourceShaderName.c_str()))
		{
			return m_shaders[i];
		}
	}

	return NULL;
}

Texture* AssetManager::GetTextureByName(char* textureName)
{
	for (int i = 0; i < (int)m_textures.size(); ++i)
	{
		Texture* texture = m_textures[i];
		string textureName2 = texture->GetName();

		if (!strcmp(textureName2.c_str(), textureName))
		{
			return texture;
		}
	}
	return NULL;
}

Mesh * AssetManager::GetMeshByName(char * meshName)
{
	for (int i = 0; i < (int)m_textures.size(); ++i)
	{
		Mesh* mesh = m_meshes[i];
		string meshName2 = mesh->GetName();

		if (!strcmp(meshName2.c_str(), meshName))
		{
			return mesh;
		}
	}
	return NULL;
}

AssetManager* AssetManager::GetInstance()
{
	if (m_instance == NULL)
	{
		m_instance = new AssetManager();
	}

	return m_instance;
}

void AssetManager::LoadScene(ID3D11Device* device, ID3D11DeviceContext* context, Scene* scene, std::string filename)
{
	std::ifstream file(filename.c_str(), std::ios::in);

	if (file.is_open())
	{
		std::cout << "Reading Scenedata from " << filename << std::endl;
		std::string line;
		while (getline(file, line))
		{
			char cmd[32];
			cmd[0] = 0;
			sscanf_s(line.c_str(), "%32s", cmd, sizeof(cmd));

			if (strcmp(cmd, "Texture") == 0)
			{
				char textureName[256];
				textureName[0] = 0;
				int requestSRGB = 0;
				sscanf_s(line.c_str(), "%s %s", &cmd[0], sizeof(cmd), &textureName[0], sizeof(textureName));
					
				WCHAR *pwcsName;
				int nChars = MultiByteToWideChar(CP_ACP, 0, textureName, -1, NULL, 0);
				pwcsName = new WCHAR[nChars];
				MultiByteToWideChar(CP_ACP, 0, textureName, -1, (LPWSTR)pwcsName, nChars);

				bool requestS = true;
				if (requestSRGB == 1)
					requestS = false;
				LoadTexture(device, context, pwcsName, requestS);
					
			}
			else if (strcmp(cmd, "sTexture") == 0)
			{
				char textureName[256];
				textureName[0] = 0;
				int requestSRGB = 0;
				sscanf_s(line.c_str(), "%s %s", &cmd[0], sizeof(cmd), &textureName[0], sizeof(textureName));

				WCHAR *pwcsName;
				int nChars = MultiByteToWideChar(CP_ACP, 0, textureName, -1, NULL, 0);
				pwcsName = new WCHAR[nChars];
				MultiByteToWideChar(CP_ACP, 0, textureName, -1, (LPWSTR)pwcsName, nChars);
				bool requestS = false;
				LoadTexture(device, context, pwcsName, requestS);

			}
			else if (strcmp(cmd, "GameObject") == 0)
			{
				char objectName[256];
				char materialName[256];
				objectName[0] = 0;
				materialName[0] = 0;
				sscanf_s(line.c_str(), "%s %s %s", &cmd[0], sizeof(cmd), &objectName[0], sizeof(objectName), &materialName[0], sizeof(materialName));

				WCHAR *pwcsName;
				int nChars = MultiByteToWideChar(CP_ACP, 0, objectName, -1, NULL, 0);
				pwcsName = new WCHAR[nChars];
				MultiByteToWideChar(CP_ACP, 0, objectName, -1, (LPWSTR)pwcsName, nChars);

				WCHAR *pwcsName2;
				int nChars2 = MultiByteToWideChar(CP_ACP, 0, materialName, -1, NULL, 0);
				pwcsName2 = new WCHAR[nChars2];
				MultiByteToWideChar(CP_ACP, 0, materialName, -1, (LPWSTR)pwcsName2, nChars2);

				scene->CreateGameObject(objectName,materialName,"");
			}
			else if (strcmp(cmd, "GameObjectT") == 0)
			{
				char objectName[256];
				char materialName[256];
				objectName[0] = 0;
				materialName[0] = 0;
				sscanf_s(line.c_str(), "%s %s %s", &cmd[0], sizeof(cmd), &objectName[0], sizeof(objectName), &materialName[0], sizeof(materialName));

				WCHAR *pwcsName;
				int nChars = MultiByteToWideChar(CP_ACP, 0, objectName, -1, NULL, 0);
				pwcsName = new WCHAR[nChars];
				MultiByteToWideChar(CP_ACP, 0, objectName, -1, (LPWSTR)pwcsName, nChars);

				WCHAR *pwcsName2;
				int nChars2 = MultiByteToWideChar(CP_ACP, 0, materialName, -1, NULL, 0);
				pwcsName2 = new WCHAR[nChars2];
				MultiByteToWideChar(CP_ACP, 0, materialName, -1, (LPWSTR)pwcsName2, nChars2);

				scene->CreateGameObjectTransparent(objectName, materialName, "");
			}
			else if (strcmp(cmd, "GameObjectL") == 0)
			{
				char objectName[256];
				char materialName[256];
				objectName[0] = 0;
				materialName[0] = 0;
				sscanf_s(line.c_str(), "%s %s %s", &cmd[0], sizeof(cmd), &objectName[0], sizeof(objectName), &materialName[0], sizeof(materialName));

				WCHAR *pwcsName;
				int nChars = MultiByteToWideChar(CP_ACP, 0, objectName, -1, NULL, 0);
				pwcsName = new WCHAR[nChars];
				MultiByteToWideChar(CP_ACP, 0, objectName, -1, (LPWSTR)pwcsName, nChars);

				WCHAR *pwcsName2;
				int nChars2 = MultiByteToWideChar(CP_ACP, 0, materialName, -1, NULL, 0);
				pwcsName2 = new WCHAR[nChars2];
				MultiByteToWideChar(CP_ACP, 0, materialName, -1, (LPWSTR)pwcsName2, nChars2);

				scene->CreateGameObject(objectName, materialName, "", false, false, true);
			}
			else if (strcmp(cmd, "Tree") == 0)
			{
				char objectName[256];
				char materialName[256];
				char leafMaterialName[256];
				objectName[0] = 0;
				materialName[0] = 0;
				leafMaterialName[0] = 0;
				sscanf_s(line.c_str(), "%s %s %s %s", &cmd[0], sizeof(cmd), &objectName[0], sizeof(objectName), &materialName[0], sizeof(materialName), &leafMaterialName[0], sizeof(leafMaterialName));

				WCHAR *pwcsName;
				int nChars = MultiByteToWideChar(CP_ACP, 0, objectName, -1, NULL, 0);
				pwcsName = new WCHAR[nChars];
				MultiByteToWideChar(CP_ACP, 0, objectName, -1, (LPWSTR)pwcsName, nChars);

				WCHAR *pwcsName2;
				int nChars2 = MultiByteToWideChar(CP_ACP, 0, materialName, -1, NULL, 0);
				pwcsName2 = new WCHAR[nChars2];
				MultiByteToWideChar(CP_ACP, 0, materialName, -1, (LPWSTR)pwcsName2, nChars2);

				WCHAR *pwcsName3;
				int nChars3 = MultiByteToWideChar(CP_ACP, 0, leafMaterialName, -1, NULL, 0);
				pwcsName3 = new WCHAR[nChars3];
				MultiByteToWideChar(CP_ACP, 0, leafMaterialName, -1, (LPWSTR)pwcsName3, nChars3);

				scene->CreateGameObject(objectName, materialName, leafMaterialName, true);
			}
			else if (strcmp(cmd, "Foliage") == 0)
			{
				char objectName[256];
				char materialName[256];
				char leafMaterialName[256];
				objectName[0] = 0;
				materialName[0] = 0;
				leafMaterialName[0] = 0;
				sscanf_s(line.c_str(), "%s %s %s %s", &cmd[0], sizeof(cmd), &objectName[0], sizeof(objectName), &materialName[0], sizeof(materialName), &leafMaterialName[0], sizeof(leafMaterialName));

				WCHAR *pwcsName;
				int nChars = MultiByteToWideChar(CP_ACP, 0, objectName, -1, NULL, 0);
				pwcsName = new WCHAR[nChars];
				MultiByteToWideChar(CP_ACP, 0, objectName, -1, (LPWSTR)pwcsName, nChars);

				WCHAR *pwcsName2;
				int nChars2 = MultiByteToWideChar(CP_ACP, 0, materialName, -1, NULL, 0);
				pwcsName2 = new WCHAR[nChars2];
				MultiByteToWideChar(CP_ACP, 0, materialName, -1, (LPWSTR)pwcsName2, nChars2);

				WCHAR *pwcsName3;
				int nChars3 = MultiByteToWideChar(CP_ACP, 0, leafMaterialName, -1, NULL, 0);
				pwcsName3 = new WCHAR[nChars3];
				MultiByteToWideChar(CP_ACP, 0, leafMaterialName, -1, (LPWSTR)pwcsName3, nChars3);

				scene->CreateGameObject(objectName, materialName, leafMaterialName, true, true);
			}
		}
	}
	else
	{
		std::cout << "!! File not found - " << filename << std::endl;
		return;
	}
}