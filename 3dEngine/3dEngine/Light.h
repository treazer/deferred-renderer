#pragma once

#include <d3d11_1.h>
#include <directxmath.h>
#include "Component.h"

using namespace std;
using namespace DirectX; 
class Scene;
class Light : public Component
{
public:
	Light();
	~Light();

	void InitializeLight(GameObject* gameObject, float intensity, float falloff, float r, float g, float b);
	void Initialize(GameObject* gameObject);
	float GetIntensity();
	float GetFalloff();

	string GetName();
	GameObject* GetGameObject();
	XMFLOAT4 GetColor();
	XMFLOAT4 GetDirection();
	void Update(Scene * scene);
	void Render(Scene * scene);
private:
	float m_intensity;
	float m_falloff;
	XMFLOAT4 m_color;
};

