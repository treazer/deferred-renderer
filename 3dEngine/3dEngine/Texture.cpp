#include "Texture.h"
#include <iostream>
#include "DDSTextureLoader.h"
using namespace DirectX;

Texture::Texture(void)
{
	m_texture = 0;
	m_width = 0;
	m_height = 0;
	m_name = "";
}

Texture::~Texture(void)
{
	if (m_texture)
	{
		m_texture->Release();
		m_texture = NULL;
	}

	m_name.clear();
}

bool Texture::Initialize(ID3D11Device* device, ID3D11DeviceContext* context, WCHAR* fileName, bool sRGB)
{
	HRESULT result;

	wstring tempName(fileName);

	m_name = string(tempName.begin(), tempName.end());

	int pos = m_name.find_last_of("/");
	if (pos >= 0)
	{
		m_name = m_name.substr(pos + 1, m_name.length());
	}
	m_name = m_name.substr(0, m_name.find_last_of("."));

	// Load the texture
	result = CreateWICTextureFromFileEx(device, context, fileName, 0, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, D3D11_RESOURCE_MISC_GENERATE_MIPS, sRGB, NULL, &m_texture);
	if (FAILED(result))
	{
		return false;
	}

	// Get width and height
	ID3D11Resource* resource = 0;
	m_texture->GetResource(&resource);
	context->GenerateMips(m_texture);
	ID3D11Texture2D* texture2D = 0;
	result = resource->QueryInterface(&texture2D);

	if (SUCCEEDED(result))
	{
		D3D11_TEXTURE2D_DESC desc;
		texture2D->GetDesc(&desc);

		m_width = desc.Width;
		m_height = desc.Height;
	}
	
	texture2D->Release();
	resource->Release();

	return true;
}

ID3D11ShaderResourceView* Texture::GetTexture()
{
	return m_texture;
}

string Texture::GetName()
{
	return m_name;
}

int Texture::GetWidth()
{
	return m_width;
}

int Texture::GetHeight()
{
	return m_height;
}
