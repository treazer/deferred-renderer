#pragma once

#include <d3d11_1.h>
#include <directxmath.h>

class DepthTexture
{
public:
	DepthTexture();
	~DepthTexture();

	bool Initialize(ID3D11Device* device, int textureWidth, int textureHeight);
	void SetRenderTarget(ID3D11DeviceContext*);
	void ClearRenderTarget(ID3D11DeviceContext*, ID3D11DepthStencilView*, float, float, float, float);
	ID3D11ShaderResourceView* GetShaderResourceView();
	ID3D11DepthStencilView * GetDepthStencilView();

private:
	ID3D11Texture2D * m_depthStencilTexture;
	ID3D11ShaderResourceView* m_shaderResourceView;
	ID3D11DepthStencilView* m_depthStencilView;
};

