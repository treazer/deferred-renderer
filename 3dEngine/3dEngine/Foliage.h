#pragma once
#include "Component.h"
#include "ComputeShader.h"
#include "Foliage.h"
#include <d3d11_1.h>
#include <directxmath.h>
#include <vector>
#include "FoliageShader.h"
#include "Leaf.h"
#include "Mesh.h"
#include "Renderer.h"

#define MIN_DISTANCE_LEAVES 0.15f //testing 0.95 // normal 0.05
class Scene;
class GameObject;
class Wind;
class Foliage :
	public Component
{
public:
	struct LeafData
	{
		XMFLOAT4 position;
		XMFLOAT4 color;
		XMFLOAT4 normal;
		XMFLOAT4 tangent;
		XMFLOAT4 bitangent;
		XMFLOAT4 boneWeights;
		XMFLOAT4 boneIDs;
	};

	struct BufType
	{
		int i;
		XMFLOAT4 f;
	};

	Foliage();
	~Foliage();
	void Initialize(GameObject* gameObject, Graphics * graphics, HWND hwnd);
	void Initialize(GameObject* gameObject);
	string GetName();
	GameObject* GetGameObject();
	void Update(Scene * scene);
	void Render(Scene * scene);
	void Render(ID3D11DeviceContext * deviceContext);
	void LoadLeafData(ID3D11Device* device, Mesh* mesh, HWND hwnd);
	void SetFoliageShader(FoliageShader* shader);
	void AddRendererToObject(ID3D11Device* device, HWND hwnd);
	float VectorDistance(const XMFLOAT3& v1, const XMFLOAT3& v2);
	float VectorDistance(const XMFLOAT4& v1, const XMFLOAT4& v2);
	void CreateLeafMesh(ID3D11Device* device);
	static void AssignBranchHierarchyToVector(int vertexCount, Mesh::VertexType * vertices, const std::vector<Mesh::BoneInfoData> boneInfo);
	static void CalculateHeightOfBranches(int vertexCount, Mesh::VertexType * vertices, std::vector<Mesh::BoneInfoData> &boneInfo, Mesh * mesh);
	static void WriteBranchLevel(int vertexCount, Mesh::VertexType * vertices, std::vector<Mesh::BoneInfoData> &boneInfo);
	void CalculateForces(std::vector<Mesh::BoneInfoData> &boneInfo, Wind * pWind);
	void ExecuteComputeShader(Mesh * mesh, int leaves, Mesh::VertexType * vertices, int numVertices);
	Renderer * GetRenderer();
	Shader::TreeBufferType GetTreeInfo();
	void SetTreeInfo(Shader::TreeBufferType info);
	Mesh* m_leaves;

private:
	void CreateRenderData();
	void CalculateLevelOfDetail();

	FoliageShader* m_foliageShader;
	ComputeShader* m_computeShader;
	Renderer * m_leafRenderer;
	int m_rendererId;
	LeafData* m_leafData;
	int m_numLeaves;

	Mesh::VertexType * m_leafVertices;
	int m_vertexCount;
	int m_indexCount;
	Texture * m_branchMovementTexture;
	Graphics * m_graphics;

	//ID3D11Buffer* g_pBuf0;
	//ID3D11Buffer* g_pBufResult;

	//ID3D11ShaderResourceView*   g_pBuf0SRV;
	//ID3D11ShaderResourceView*   g_pBuf1SRV;
	//ID3D11UnorderedAccessView*  g_pBufResultUAV;

	Mesh::VertexType * leafDataBuffer;

	Mesh::VertexType g_vBuf0[128000];
	Mesh::VertexType g_vBuf1[128000];

	ID3D11Buffer* g_pBuf0;
	ID3D11Buffer* g_pBuf1;
	ID3D11Buffer* g_pBufResult;
	ID3D11Buffer* g_pBufResult2;
	ID3D11Buffer* g_pBufResult3;

	ID3D11ShaderResourceView*   g_pBuf0SRV;
	ID3D11ShaderResourceView*   g_pBuf1SRV;
	ID3D11UnorderedAccessView*  g_pBufResultUAV;
	ID3D11UnorderedAccessView*  g_pBufResultUAV2;
	ID3D11UnorderedAccessView*  g_pBufResultUAV3;

	float m_baseStrength;
	float m_branchStrength_1;
	float m_branchStrength_2;
	float m_branchStrength_3;
	float m_leafStrength;

	Shader::TreeBufferType m_treeInfo;
};

