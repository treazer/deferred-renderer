////////////////////////////////////////////////////////////////////////////////
// Tree Calculations
////////////////////////////////////////////////////////////////////////////////

float4 CalculateBranchMovement(float4 originalPosition, float4 boneIDs, float4 boneWeights);
matrix GetDisplacementMatrix(float4 bones, float4 boneWeights);
matrix CalculateMatrixDisplacement(matrix baseMatrix, float boneID, float strength, float fullLength, float parentID);
float4 ApplyVectorDisplacement(matrix baseMatrix, float4 originalPosition, float strength, float fullLength, int boneID);
float4 RetrieveWind(int bone, float time);
HullInputType CalculateLeafMovement(HullInputType input, float4 offset, float4 boneIDs);
    
/*
    Calculates the position of a leaf corner.
    Returns new vector position.
*/
HullInputType CalculateLeafMovement(HullInputType input, float4 boneIDs)
{
    float4 originalPosition = input.position;
    float4 offset = input.color;
    float3 direction = offset.xyz;
    
    input.normal = input.normal;
    input.tangent = input.tangent;
    input.bitangent = input.bitangent;
    
    float3 windDirection =  RetrieveWind(boneIDs.x,0)*0.47f;
    
    //add turbulance
    float leafSpeed = leafStrength;
    float leafStrength2 = 0.02f;
    leafStrength2 = pow(leafStrength,12) * length(windDirection)+0.1f;
    
    float2 samplePosition = (originalPosition.xz*2.21f)+(10*timeT.x*leafSpeed*leafStrength2);
    float3 textureSample = ((branchMovementTex.SampleLevel(SampleType, samplePosition, 0)).xyz)*2.0f;
    
    //make direction face the wind direction
    
    float3 rotVec = lerp(direction,windDirection,leafBranchInfluence)+textureSample;
    rotVec = normalize(rotVec);
    rotVec = lerp(rotVec,input.tangent,leafStiffness); //0.65f

    float4 rot1 = CreateRotationQuaternion(rotVec,input.tangent);

    
    float3 result1 = RotateVectorByQuaternion(input.tangent, rot1);
    float3 result2 = RotateVectorByQuaternion(input.normal, rot1);
    float3 result3 = RotateVectorByQuaternion(input.bitangent, rot1);
    float3 result = RotateVectorByQuaternion(offset.xyz, rot1);
    
    input.tangent = normalize(result1);
    input.normal = normalize(result2);
    input.bitangent = normalize(result3);
     
    direction = normalize(result);
    input.position = originalPosition + float4(direction.xyz*offset.w,0);
    
    return input;
}

/*
    Calculates the position of a vector according to the trees bending.
    Returns new vector position.
*/
float4 RetrieveWind(int bone, float time)
{
    float4 windVec;
    windVec = treeForces[bone]*treeForces[bone].w*0.01f;

    return windVec;
}

float4 CalculateBranchMovement(float4 originalPosition, float4 boneIDs, float4 boneWeights)
{
    matrix baseMatrix = GetDisplacementMatrix(boneIDs, boneWeights);  //move to compute shader
    //localize position to bone space for simplification
    originalPosition = mul(treeBoneOffsets[boneIDs.x],originalPosition);
    //windVec = mul(transpose(worldMatrix),windVec);    
    float4 finalPosition = ApplyVectorDisplacement(baseMatrix, originalPosition, boneWeights.x, treeBoneHeights[boneIDs.x].x, boneIDs.x);
    
    return finalPosition;
}   
                                             
                                                                                          
/*
    Returns a matrix based on all parent matrices and the trees displacement.
*/
matrix GetDisplacementMatrix(float4 bones, float4 boneWeights)
{
    matrix finalMatrix = identityMatrix;
    finalMatrix = CalculateMatrixDisplacement(finalMatrix, bones.w, boneWeights.w, 0,-1);
    finalMatrix = CalculateMatrixDisplacement(finalMatrix, bones.z, boneWeights.w, treeBoneHeights[bones.w].x, bones.w);
    finalMatrix = CalculateMatrixDisplacement(finalMatrix, bones.y, boneWeights.z, treeBoneHeights[bones.z].x, bones.z);
    finalMatrix = CalculateMatrixDisplacement(finalMatrix, bones.x, boneWeights.y, treeBoneHeights[bones.y].x, bones.y);
    return finalMatrix;
}
     
                                             
/*
    Transforms a matrix according to the trees bending.
*/
matrix CalculateMatrixDisplacement(matrix baseMatrix, float boneID, float strength, float fullLength, float parentID)
{   
    if (int(boneID)==-1)
        return identityMatrix;
    float3 windV;
    if (int(parentID)>=0)
        windV = RetrieveWind(parentID,timeT.x).xyz;
    else
        windV = float3(0,0,0);
    float3 direction = -normalize(float3(baseMatrix._12, baseMatrix._22, baseMatrix._32));
    float3 branchStartPosition = float3(transpose(treeBones[boneID])._14,transpose(treeBones[boneID])._24,transpose(treeBones[boneID])._34);
    float height = dot(float3(0,1,0), branchStartPosition);
    float3 strengthTerm = (windV * fullLength) / strength;

    float delta = abs(height) / fullLength;
    float oscillation = (0.5f * delta * delta * delta * delta) + (0.45f * delta);
    float3 displacement = strengthTerm * oscillation;
    float3 heightPos = height * direction;
    float3 result = displacement + heightPos;
    float4 rotation = CreateRotationQuaternion(direction,normalize(result));
    matrix finalMatrix = CreateMatrixFromQuaternion(rotation);
    finalMatrix = mul(baseMatrix,finalMatrix);
    if (delta<0.0001f) finalMatrix = baseMatrix;
    finalMatrix = mul(finalMatrix,transpose(treeBones[int(boneID)]));

    return finalMatrix;
}
                                             
                                             
/*
    Calculates the position of a vector according to its branch matrix.
*/                                          
float4 ApplyVectorDisplacement(matrix baseMatrix, float4 originalPosition, float strength, float fullLength, int boneID)
{
    float3 windV = RetrieveWind(boneID, timeT.x).xyz;
    float4 finalPosition = originalPosition;
    float3 direction;
    direction = normalize(float3(baseMatrix._12, baseMatrix._22, baseMatrix._32));
    float3 localPoint = originalPosition.xyz;
    float height = dot(direction, originalPosition.xyz);
    float3 strengthTerm = (windV * fullLength) / strength;
    float delta = abs(height) / fullLength;
    float oscillation = (0.5f * delta * delta * delta * delta) + (0.45f * delta);
    float3 displacement = strengthTerm * oscillation;
    float3 heightPos = height * direction;
    float3 result = displacement + heightPos;
    float4 rotation = CreateRotationQuaternion(direction,normalize(result));
    //put point back into object space
    
    matrix finalMatrix = CreateMatrixFromQuaternion(rotation);
    finalMatrix = mul(baseMatrix,transpose(finalMatrix));
    if (height<0.005f) finalMatrix = baseMatrix;
    finalPosition.w = 1;
    finalPosition = mul(finalMatrix,finalPosition);
    
    
    return finalPosition;
}
