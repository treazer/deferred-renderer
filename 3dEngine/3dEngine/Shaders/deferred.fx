static const float PI = 3.14159265f;
static const float2 SCREEN_RESOLUTION = float2(1280,720);
static float2 poissonDisk[64] =
{
        float2(-0.613392, 0.617481),float2(0.170019, -0.040254),float2(-0.299417, 0.791925),float2(0.645680, 0.493210),float2(-0.651784, 0.717887),float2(0.421003, 0.027070),float2(-0.817194, -0.271096),float2(-0.705374, -0.668203),float2(0.977050, -0.108615),float2(0.063326, 0.142369),float2(0.203528, 0.214331),float2(-0.667531, 0.326090),float2(-0.098422, -0.295755),float2(-0.885922, 0.215369),float2(0.566637, 0.605213),float2(0.039766, -0.396100),float2(0.751946, 0.453352),float2(0.078707, -0.715323),float2(-0.075838, -0.529344),float2(0.724479, -0.580798),float2(0.222999, -0.215125),float2(-0.467574, -0.405438),float2(-0.248268, -0.814753),float2(0.354411, -0.887570),float2(0.175817, 0.382366),float2(0.487472, -0.063082),float2(-0.084078, 0.898312),float2(0.488876, -0.783441),float2(0.470016, 0.217933),float2(-0.696890, -0.549791),float2(-0.149693, 0.605762),float2(0.034211, 0.979980),float2(0.503098, -0.308878),float2(-0.016205, -0.872921),float2(0.385784, -0.393902),float2(-0.146886, -0.859249),float2(0.643361, 0.164098),float2(0.634388, -0.049471),float2(-0.688894, 0.007843),float2(0.464034, -0.188818),float2(-0.440840, 0.137486),float2(0.364483, 0.511704),float2(0.034028, 0.325968),float2(0.099094, -0.308023),float2(0.693960, -0.366253),float2(0.678884, -0.204688),float2(0.001801, 0.780328),float2(0.145177, -0.898984),float2(0.062655, -0.611866),float2(0.315226, -0.604297),float2(-0.780145, 0.486251),float2(-0.371868, 0.882138),float2(0.200476, 0.494430),float2(-0.494552, -0.711051),float2(0.612476, 0.705252),float2(-0.578845, -0.768792),float2(-0.772454, -0.090976),float2(0.504440, 0.372295),float2(0.155736, 0.065157),float2(0.391522, 0.849605),float2(-0.620106, -0.328104),float2(0.789239, -0.419965),float2(-0.545396, 0.538133),float2(-0.178564, -0.596057)
};

static float3 sampleSphere[16] = {float3( 0.2024537f, 0.841204f, -0.9060141f), float3(-0.2200423f, 0.6282339f, -0.8275437f), float3( 0.3677659f, 0.1086345f, -0.4466777f), float3( 0.8775856f, 0.4617546f, -0.6427765f), float3( 0.7867433f, -0.141479f, -0.1567597f), float3( 0.4839356f, -0.8253108f, -0.1563844f), float3( 0.4401554f, -0.4228428f, -0.3300118f), float3( 0.0019193f, -0.8048455f, 0.0726584f), float3(-0.7578573f, -0.5583301f, 0.2347527f), float3(-0.4540417f, -0.252365f, 0.0694318f), float3(-0.0483353f, -0.2527294f, 0.5924745f), float3(-0.4192392f, 0.2084218f, -0.3672943f), float3(-0.8433938f, 0.1451271f, 0.2202872f), float3(-0.4037157f, -0.8263387f, 0.4698132f), float3(-0.6657394f, 0.6298575f, 0.6342437f), float3(-0.0001783f, 0.2834622f, 0.8343929f), }; 

/////////////
// GLOBALS //
/////////////
Texture2D GBuffer1 : register(t0); //color r g b roughness -metal
Texture2D GBuffer2 : register(t1); //normal x y
Texture2D depthBuffer : register(t3);
TextureCube environmentTexture : register(t4);
Texture2D shadowMap : register(t5);
Texture2D shadowColor : register(t6);
Texture2D translucencyBuffer : register(t7);
TextureCube irradianceTexture : register(t8);
Texture2D GBuffer1_2 : register(t9); //color r g b roughness -metal
Texture2D GBuffer2_2 : register(t10); //normal x y
Texture2D depthBuffer_2 : register(t11);
Texture2D GBuffer3 : register(t12);
Texture2D GBuffer3_2 : register(t13);
SamplerState SampleType : register(s0);
SamplerComparisonState SamplerShadow : register(s1);

//////////////
// CBUFFERS //
//////////////

cbuffer FrameDataBuffer : register(b0)
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projectionMatrix;
    matrix viewMatrixOld;
    matrix projectionMatrixOld;
    float4 eyePosition;
    float4 wind;
    float4 time;
};

cbuffer DeferredBufferType : register(b1)
{
    matrix lightViewMatrix;
    matrix lightOrthoMatrix;
    matrix worldMatrixDeferred;
    matrix viewMatrixDeferred;
    matrix projectionMatrixDeferred;
    matrix inverseProjection;
    matrix inverseView;
    float4 sunColor;
    float4 sunDirection;
    float4 sunPosition;
    float4 eye;
    float4 reconstructionValues;
};

cbuffer RendererBuffer: register(b2)
{
    float tessellationAmount;
    float emission;
    float displacementStrength;
    float materialId;
    float normalStrength;
    float uvTilingX;
    float uvTilingY;
    float a;
};

cbuffer ObjectBuffer: register(b3)
{
    matrix treeBones[256];
    matrix treeBoneOffsets[256];
    float4 treeBoneHeights[256];
};


//////////////
// TYPEDEFS //
//////////////
struct VertexInputType
{
    float4 position : POSITION;
    float4 color : COLOR;
    float4 tex : TEXCOORD0;
    float4 normal : NORMAL;
    float4 tangent : TANGENT;
    float4 bitangent : BITANGENT;
    float4 boneWeights : COLOR1;
    float4 boneIDs : COLOR2;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
    float2 tex : TEXCOORD0;
    float3 normal : TEXCOORD1;
};

struct HullInputType
{
    float3 position : POSITION;
    float4 color : COLOR;
};

struct HullOutputType
{
    float3 position : POSITION;
    float4 color : COLOR;
};

struct ConstantOutputType
{
    float edges[3] : SV_TessFactor;
    float inside : SV_InsideTessFactor;
};

struct SURFACE_DATA
{
    float3 Position;
    float3 Normal;
    float Roughness;
    float Metalness;
    float3 Color;
    float Depth;
};

struct PS_OUTPUT
{
unorm float4 Diffuse : SV_Target0;
unorm float2 Luminance : SV_Target1;
unorm float4 Shadowing : SV_Target2;
};


//FUNCTION DEFINITIONS
float3 ComputeSpecFactor( in SURFACE_DATA SurfaceData, in float3 LightDirection, in float NdotL );
float random( float2 p );
float3 randomNormal(float2 vec);
float4 AddFog(float4 color, float3 eyePosition, float3 pixelPosition);
float Vignetting(float2 vec);
float3 positionFromDepth(float2 sp);
////////////////////////////////////////////////////////////////////////////////
// Ambient Occlusion
////////////////////////////////////////////////////////////////////////////////

float AmbientOcclusion(float3 pointPos, float3 pointDir, float2 pointUV, float3 surfaceNormal)
{
    int numSamples = 14;
    float sampleRadius = 0.034f;
    float min = 0.0122013f; //0.4
    float max =2.009f; //1.0
    
    float3 randNor = randomNormal(pointUV);
    float4 wDepth = float4(positionFromDepth(pointUV),1);
    wDepth = mul(wDepth, viewMatrix);
    
    float depth = wDepth.z;
    float3 normal = normalize(mul(float4(pointDir,0), viewMatrix).xyz);
    float radius = sampleRadius / depth;
    float3 samplePos = float3(pointUV, depth);
    float occ = 0.0f;
    float3 ray;
    float4 wOccDepth;
    float occDepth, depthDifference, normalOcc, depthOcc;
    for (int i = 0; i < numSamples; ++i)
    { 
        float3 offset = reflect(float3(poissonDisk[i].xy,poissonDisk[i+1].y), randNor);
        offset = sign(dot(offset, normal))*offset;
        offset.y = -offset.y;
        
        ray = samplePos + radius*offset;
        
        if ((saturate(ray.x) != ray.x) || (saturate(ray.y) != ray.y))
            continue;
        
        wOccDepth = float4(positionFromDepth(ray.xy),1);
        wOccDepth = mul(wOccDepth, viewMatrix);
        occDepth = wOccDepth.z;
        float3 occNormal = normalize(mul(surfaceNormal, viewMatrix).xyz);
        depthDifference = (samplePos.z - occDepth); 
        normalOcc = (1.0f - saturate(dot(occNormal, normal)));
        depthOcc = step(min, depthDifference) * (1.0f - depthDifference) * (1.0f - smoothstep(min, max, depthDifference));
        occ += saturate(depthOcc*normalOcc*1.0f); //depthOcc*normalOcc
    }
    occ /= numSamples;
    //return 
    float result = saturate(pow(1.1f - occ, 2.5f));
    return result;
}

////////////////////////////////////////////////////////////////////////////////
// Shadow Strength
////////////////////////////////////////////////////////////////////////////////
float3 CalculateShadow(float2 screenUV)
{
    int numSamples = 16;
    float sampleRadius = 0.0004f;
    float occluded = 0.0f;
    float shadowSum = 0;
    float shadow = 0;
    float4 shadowPosition = float4(positionFromDepth(screenUV),0.0f);
    shadowPosition.w = 1;
    shadowPosition = mul(shadowPosition, transpose(lightViewMatrix));
    shadowPosition = mul(shadowPosition, transpose(lightOrthoMatrix));
    
    float2 shadowUV  = shadowPosition.xy / shadowPosition.w;
    float shadowDepth = shadowPosition.z / shadowPosition.w;
    shadowUV = shadowUV * float2(0.5f, -0.5f) + float2(0.5f, 0.5f);
    int diskIndex = 0;
    for (int i = 0; i < numSamples; i++)
    {
        diskIndex = (24*random(shadowUV+float2(i,i)));
        shadowSum += shadowMap.SampleCmp(SamplerShadow, shadowUV + (poissonDisk[diskIndex] * sampleRadius),shadowDepth-0.0014f,0.0f);
    }
    shadowSum/=numSamples;
    occluded = 1-shadowSum;
    
    float3 occludedColor = lerp((occluded*shadowColor.Sample(SampleType, shadowUV).xyz),float3(1,1,1),1-shadowColor.Sample(SampleType, shadowUV).w);
    occludedColor = float3(1.0f-occluded.xxx); //something wrong with the shadow color
    occludedColor = shadowSum.xxx+(occludedColor*occluded);
    if (shadowUV.x > 1.0 || shadowUV.y > 1.0 || shadowUV.x < 0 || shadowUV.y < 0)
        return float3(1,1,1);

    return occludedColor;
}
    
////////////////////////////////////////////////////////////////////////////////
// Domain Shader
////////////////////////////////////////////////////////////////////////////////
[domain("tri")]

PixelInputType ColorDomainShader(ConstantOutputType input, float3 uvwCoord : SV_DomainLocation, const OutputPatch<HullOutputType, 3> patch)
{
    float3 vertexPosition;
    PixelInputType output;
 

    // Determine the position of the new vertex.
    vertexPosition = uvwCoord.x * patch[0].position + uvwCoord.y * patch[1].position + uvwCoord.z * patch[2].position;
    //vertexPosition = patch[0].position;
    // Calculate the position of the new vertex against the world, view, and projection matrices.
    output.position = mul(float4(vertexPosition, 1.0f), worldMatrixDeferred);
    output.position = mul(output.position, viewMatrixDeferred);
    output.position = mul(output.position, projectionMatrixDeferred);

    // Send the input color into the pixel shader.
    output.color = patch[0].color;
    
    return output;
}

////////////////////////////////////////////////////////////////////////////////
// Patch Constant Function
////////////////////////////////////////////////////////////////////////////////
ConstantOutputType ColorPatchConstantFunction(InputPatch<HullInputType, 3> inputPatch, uint patchId : SV_PrimitiveID)
{    
    ConstantOutputType output;

    // Set the tessellation factors for the three edges of the triangle.
    output.edges[0] = 2;
    output.edges[1] = 2;
    output.edges[2] = 2;

    // Set the tessellation factor for tessallating inside the triangle.
    output.inside = tessellationAmount;

    return output;
}

////////////////////////////////////////////////////////////////////////////////
// Hull Shader
////////////////////////////////////////////////////////////////////////////////
[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("ColorPatchConstantFunction")]

HullOutputType ColorHullShader(InputPatch<HullInputType, 3> patch, uint pointId : SV_OutputControlPointID, uint patchId : SV_PrimitiveID)
{
    HullOutputType output;

    // Set the position for this control point as the output position.
    output.position = patch[pointId].position;

    // Set the input color as the output color.
    output.color = patch[pointId].color;

    return output;
}




////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelInputType VSMain(VertexInputType input)
{
    PixelInputType output;
    //HullInputType output; //switch to this one
    
    // Change the position vector to be 4 units for proper matrix calculations.
    input.position.w = 1.0f;
    
    // Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(input.position, worldMatrixDeferred);
    output.position = mul(output.position, viewMatrixDeferred);
    output.position = mul(output.position, projectionMatrixDeferred);

    // Store the texture coordinates for the pixel shader.
    output.tex = input.tex;
    output.normal = input.normal;
    
    return output;
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float3 Diffuse(float3 pAlbedo)
{
    return pAlbedo/PI;
}
float NormalDistribution_GGX(float a, float NdH)
{
    
    // Isotropic GGX
    
    float a2 = a*a;
    float NdH2 = NdH * NdH;

    float denominator = NdH2 * (a2 - 1.0f) + 1.0f;
    denominator *= denominator;
    denominator *= PI;

    return a2 / denominator;
}
float3 Fresnel_Schlick(float3 specularColor, float3 h, float3 v)
{
    return (specularColor + (1.0f - specularColor) * pow((1.0f - saturate(dot(v, h))), 5));
}
float Specular_D(float a, float NdH)
{
    return NormalDistribution_GGX(a, NdH);
}
float3 Specular_F(float3 specularColor, float3 h, float3 v)
{
    return Fresnel_Schlick(specularColor, h, v);

}
float Geometric_Smith_Schlick_GGX(float a, float NdV, float NdL)
{
    
    // Smith schlick-GGX
    
    float k = a * 0.5f;
    float GV = NdV / (NdV * (1 - k) + k);
    float GL = NdL / (NdL * (1 - k) + k);

    return GV * GL;
}
float Specular_G(float a, float NdV, float NdL, float NdH, float VdH, float LdV)
{
    return Geometric_Smith_Schlick_GGX(a, NdV, NdL);
}
float3 Specular(float3 specularColor, float3 h, float3 v, float3 l, float a, float NdL, float NdV, float NdH, float VdH, float LdV)
{
    return ((Specular_D(a, NdH) * Specular_G(a, NdV, NdL, NdH, VdH, LdV)) * Specular_F(specularColor, v, h) ) / (4.0f * NdL * NdV + 0.0001f);
}
float3 ComputeLight(float3 albedoColor,float3 specularColor, float3 normal, float roughness, float3 lightPosition, float3 lightColor, float3 lightDir, float3 viewDir)
{
    float NdL = saturate(dot(normal, lightDir));
    float NdV = saturate(dot(normal, viewDir));
    float3 h = normalize(lightDir + viewDir);
    float NdH = saturate(dot(normal, h));
    float VdH = saturate(dot(viewDir, h));
    float LdV = saturate(dot(lightDir, viewDir));
    float a = max(0.001f, roughness * roughness);

    float3 cDiff = Diffuse(albedoColor);
    float3 cSpec = Specular(specularColor, h, viewDir, lightDir, a, NdL, NdV, NdH, VdH, LdV);

    return lightColor * NdL * (cDiff * (1.0f - cSpec) + cSpec);
}
float3 Specular_F_Roughness(float3 specularColor, float a, float3 h, float3 v)
{
    return (specularColor + (max(1.0f-a, specularColor) - specularColor) * pow((1 - saturate(dot(v, h))), 5));
}

float3 BP_PShader(float3 Value, float illumination)
{
	Value *= illumination;
	Value -= 0.5f;
	Value  = max(Value,0.0f);
	Value /= (10.0f + Value);

	return Value;
}

float2 sampleVelocity(float2 ScreenPos)
{
    float2 ScreenPosP = ScreenPos * SCREEN_RESOLUTION;

	float oddRow = int(ScreenPosP.x) % 2 == 1 ? 1.0f : 0.0f;
	float oddColumn = int(ScreenPosP.y) % 2 == 1 ? 1.0f : 0.0f;
    
    if (reconstructionValues.x==0)
    {
        if (oddColumn == 0)
            return float2(0,0) - float2(0,(1/SCREEN_RESOLUTION.y)*1.0f);
        return GBuffer3.Sample(SampleType, ScreenPos).xy*0.5;
    }
    else
    {
        if (oddColumn == 0)
            return GBuffer3_2.Sample(SampleType, ScreenPos - float2(0,(1/SCREEN_RESOLUTION.y))).xy*0.5 - float2(0,(1/SCREEN_RESOLUTION.y)*1.0f);
        return float2(0,0);
    }
}

float sampleRoughness(float2 ScreenPos)
{
    float2 ScreenPosP = ScreenPos * SCREEN_RESOLUTION;

	float oddRow = int(ScreenPosP.x) % 2 == 1 ? 1.0f : 0.0f;
	float oddColumn = int(ScreenPosP.y) % 2 == 1 ? 1.0f : 0.0f;

    if (oddColumn > 0)
        return GBuffer1_2.Sample(SampleType, ScreenPos - sampleVelocity(ScreenPos)).w;
    return GBuffer1.Sample(SampleType, ScreenPos - sampleVelocity(ScreenPos)).w;
}

float3 sampleColor(float2 ScreenPos)
{
    float2 ScreenPosP = ScreenPos * SCREEN_RESOLUTION;

	float oddRow = int(ScreenPosP.x) % 2 == 1 ? 1.0f : 0.0f;
	float oddColumn = int(ScreenPosP.y) % 2 == 1 ? 1.0f : 0.0f;

    if (oddColumn > 0)
        return GBuffer1_2.Sample(SampleType, ScreenPos - sampleVelocity(ScreenPos)).xyz;// - float3(0,1,0);;
    return GBuffer1.Sample(SampleType, ScreenPos - sampleVelocity(ScreenPos)).xyz;// - float3(1,0,1);
}


float3 positionFromDepth(float2 sp)
{
    float2 ScreenPosP = sp * SCREEN_RESOLUTION;

	float oddRow = int(ScreenPosP.x) % 2 == 1 ? 1.0f : 0.0f;
	float oddColumn = int(ScreenPosP.y) % 2 == 1 ? 1.0f : 0.0f;

    float depth = depthBuffer.Sample(SampleType, sp - sampleVelocity(sp)).x;
    if (oddColumn > 0)
        depth = depthBuffer_2.Sample(SampleType, sp - sampleVelocity(sp)).x;
    
    float z  = depth;//(depth * 2.0f) - 1.0f;
    float x = sp.x * 2.0f - 1.0f;
    float y = (1-sp.y) * 2.0f - 1.0f;
    
    float4 clipSpacePosition = float4(x, y, z, 1.0f);
    float4 viewSpacePosition = mul(clipSpacePosition, inverseProjection);
    viewSpacePosition /= viewSpacePosition.w;
    float4 worldSpacePosition = mul(viewSpacePosition,inverseView);

    return worldSpacePosition.xyz;
}

void ComputeSurfaceDataInterlaced(out SURFACE_DATA surface, float2 screenPos)
{
    float2 ScreenPosPixel = screenPos * SCREEN_RESOLUTION;
	float oddColumn = int(ScreenPosPixel.y) % 2 == 1 ? 1.0f : 0.0f;
    
    // Define which buffer to sample for this column
    
    Texture2D bufferA, bufferB, bufferC;
    
    surface.Position = float3(0,0,0);
    surface.Normal = float3(0,0,0);
    surface.Color = float3(0,0,0);
    surface.Depth = 0;
    surface.Metalness = 0;
    surface.Roughness = 0;
}

void ComputeSurfaceDataContinuous(out SURFACE_DATA surface, float2 screenPos)
{
    
}

PS_OUTPUT PSMain(PixelInputType input) : SV_TARGET
{
    
    ////////////////////////////////////////////////////////////
    //
    // Populate Surface with correct data
    // Calculate light
    // Post Process Steps
    //
    //
    //
    //
    //
    ////////////////////////////////////////////////////////////
    
	PS_OUTPUT output;
    SURFACE_DATA surface;

    ComputeSurfaceDataInterlaced(surface, input.tex);
    
    float2 ScreenPosP = input.tex * SCREEN_RESOLUTION;
	float oddRow = int(ScreenPosP.x) % 2 == 1 ? 1.0f : 0.0f;
	float oddColumn = int(ScreenPosP.y) % 2 == 1 ? 1.0f : 0.0f;
    
    // Decode normal
    
    float2 encodedNormal = GBuffer2.Sample(SampleType, input.tex - sampleVelocity(input.tex)).xy;
    
    if (oddColumn > 0)
        encodedNormal = GBuffer2_2.Sample(SampleType, input.tex - sampleVelocity(input.tex)).xy;
    
    float2 ang = encodedNormal.xy*2-1;
    float2 scth;
    sincos(ang.x * PI, scth.x, scth.y);
    float2 scphi = float2(sqrt(1.0 - ang.y*ang.y), ang.y);
    surface.Normal = float3(scth.y*scphi.x, scth.x*scphi.x, scphi.y);
    surface.Normal = normalize(surface.Normal);
    
    surface.Position = positionFromDepth(input.tex.xy);
    surface.Roughness = sampleRoughness(input.tex);
    surface.Color = sampleColor(input.tex);
    surface.Metalness = 0.01f;
    if (surface.Roughness<0) 
    {
        surface.Roughness*=-1;
        surface.Metalness = 0.95f;
    }

    float3 eyeVec = normalize(eye.xyz-surface.Position);
    float3 incident = -eyeVec;
    float3 reflectionVector = reflect(incident, surface.Normal);                                        
    surface.Color = pow(surface.Color, 2.2f);

    float roughness = surface.Roughness;
    float g_AmbientLightIntensity = 0.80f;
    float g_ReflectionIntensity = 1.04f;
    float metallic = 0.0f;
    float3 realSpecularColor = 0.03f;
    float3 realAlbedo = surface.Color;
    metallic = surface.Metalness;
    float3 light1;
    float3 spec = 0.0f.xxx;
    float3 diffuse = 0.0f.xxx;
    float3 sunDir = float3(0.47115f,0.606024f, -0.640899f);

    realAlbedo = surface.Color - surface.Color * metallic;
    realSpecularColor = lerp(0.03f, surface.Color, metallic);
    g_ReflectionIntensity = lerp(0.08f, 1.0f, metallic);
    light1 = ComputeLight( realAlbedo, realSpecularColor,  normalize(surface.Normal),  roughness,  float3(100,100,100), sunColor, normalize(sunDir.xyz), normalize(eyeVec));
   
    float mipIndex =  roughness * roughness * 8.0f;
    
    float3 coloredEnv = float3(0.85f,0.85f,1.0f);
    float3 envColor = environmentTexture.SampleLevel(SampleType, reflectionVector, mipIndex) * coloredEnv;
    float3 irradiance = irradianceTexture.Sample(SampleType, surface.Normal*float3(1,-1,1)) * coloredEnv;
    envColor = pow(envColor.rgb, 2.2f);

    float3 envFresnel = Specular_F_Roughness(realSpecularColor, roughness * roughness, normalize(surface.Normal), normalize(eyeVec));
    float3 shadowOcclusion = CalculateShadow(input.tex);
    float3 ambientOcc = AmbientOcclusion(surface.Position, surface.Normal, input.tex, surface.Normal).xxx;
    light1 = lerp(0.0f.xxx, light1, shadowOcclusion); //Debug 1+
    float3 translucency = saturate((shadowOcclusion)*translucencyBuffer.Sample(SampleType,input.tex)*2.0f);
    output.Diffuse = float4(envFresnel * envColor * g_ReflectionIntensity + realAlbedo * irradiance * g_AmbientLightIntensity + translucency, 1.0f);
    
    // Luminance
    
    float  AvgLum = 0.2f;//length(deferredBuffer.SampleLevel(SampleType, float2(0.5f,0.5f),12).xyz);
    //AvgLum = length(deferredBuffer.SampleLevel(SampleType, float2(0.5f,0.5f),12).xyz); //uses the screen to approximate the white value
    float gMiddleGray = 0.18f;
    float illumination = gMiddleGray/(AvgLum);

    output.Luminance = float2(length(BP_PShader(output.Diffuse, illumination).xyz),0.5f);

    output.Shadowing = float4(light1.xyz,ambientOcc.x*shadowOcclusion.x);
    
     //SKYBOX
    if (surface.Roughness>2.9f && surface.Roughness<3.1f)
    {
        output.Diffuse = float4(surface.Color,1);
    }
    else
    {
        output.Diffuse = AddFog(output.Diffuse, eye.xyz, surface.Position);

    }
    
    //output.Diffuse.xyz = surface.Color;
    output.Diffuse *= Vignetting(input.tex);

    output.Diffuse.w = 1.0f;

    return output;
}


////////////////////////////////////////////////////////////////////////////////
// Assisting functions
////////////////////////////////////////////////////////////////////////////////
float4 AddFog(float4 color, float3 eyePosition, float3 pixelPosition)
{
    float4 fogColor = float4(0.792f,0.835f,0.8627f,1);
    float strength = distance(eyePosition.xyz, pixelPosition.xyz) * 0.006f;
    strength *= strength;
    float4 color2 = color - saturate(color);
    return saturate(lerp(color, saturate(fogColor),saturate(strength)));
}

float random( float2 p )
{
  // We need irrationals for pseudo randomness.
  // Most (all?) known transcendental numbers will (generally) work.
  const float2 r = float2(
    23.1406926327792690,  // e^pi (Gelfond's constant)
     2.6651441426902251); // 2^sqrt(2) (Gelfond–Schneider constant)
  return frac( cos( fmod( 123456789., 1e-7 + 256. * dot(p,r) ) ) );  
}

//Based on: https://gist.github.com/keijiro/ee7bc388272548396870
float3 randomNormal(float2 vec)
{
     float x = frac(sin(dot(vec, float2(12.9898, 78.233))) * 41758.5453);
     float y = frac(sin(dot(vec, float2(13.9898, 76.233))) * 53758.5453);
     float z = frac(sin(dot(vec, float2(14.9898, 73.233))) * 43258.5453);
     return normalize(float3(x,y,z));
}

float Vignetting(float2 vec)
{
    vec+=float2(-0.5f,-0.5f);
    float c = 1;
    float lensRadius = 1.3; //1.05
    vec /= lensRadius;
    float sin2 = vec.x*vec.x+vec.y*vec.y;
    float cos2 = 1.0-min(sin2*sin2,1.0);
    float cos4 = cos2*cos2;
    c *= cos4;
    return c;
}

PS_OUTPUT PSMainShadow(PixelInputType input) : SV_TARGET
{
    PS_OUTPUT output;
    output.Diffuse = float4(1,1,1,1);
    return output;
}

PS_OUTPUT PSMainTranslucency(PixelInputType input) : SV_TARGET
{
    PS_OUTPUT output;
    output.Diffuse = float4(1,0.5f,1,1);
    return output;
}