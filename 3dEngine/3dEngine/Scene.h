#pragma once

#include <vector>
#include "GameObject.h"
#include "Graphics.h"
#include "Input.h"
#include "Wind.h"

#define RESERVED_OBJECTS 1024

typedef std::vector<GameObject*> obj_list;

class Camera;
class Scene
{
public:
	Scene();
	~Scene();

	void Initialize(char* sceneName, Graphics* graphics);
	void Add(GameObject * object);
	void Remove(GameObject * object);
	void Update(Input* input);
	void Render(Graphics* graphics, int currentFrame, int frameSteps);
	void RenderTranslucency(Graphics* graphics);
	void RenderShadows(Graphics* graphics, XMFLOAT4X4 viewMatrix, XMFLOAT4X4 projMatrix);
	void RenderSky(Graphics* graphics, int currentFrame, int frameSteps);
	void SetActiveCamera(Camera* camera);
	void CreateTestSphere(XMFLOAT4 position);
	void CreateGameObject(std::string name, std::string materialName, std::string leafMaterialName, bool isTree = false, bool foliageOnly = false, bool hasLightmap = false);
	void CreateGameObjectTransparent(std::string name, std::string materialName, std::string leafMaterialName);
	Camera* GetActiveCamera();
	GameObject* GetSkybox();
	ID3D11ShaderResourceView* GetEnvironmentMap();
	ID3D11ShaderResourceView* GetIrradianceMap();
	Input * GetInput();
	obj_list GetObjects();
	char* GetSceneName();
	GameObject* GetSun();
	Graphics * GetGraphics();
	Wind * GetWind();

	Graphics * m_graphics;

private:
	obj_list m_objects;
	char* m_SceneName;
	Camera* m_activeCamera;
	GameObject* m_sun;
	GameObject* m_skybox;
	ID3D11ShaderResourceView* m_environmentMap;
	ID3D11ShaderResourceView* m_irradianceMap;
	Input * m_input;
	Wind * m_wind;
};