#include "DepthTexture.h"
#include <iostream>
DepthTexture::DepthTexture()
{
	m_shaderResourceView = 0;
}


DepthTexture::~DepthTexture()
{
	if (m_shaderResourceView)
	{
		m_shaderResourceView->Release();
		m_shaderResourceView = 0;
	}

}

bool DepthTexture::Initialize(ID3D11Device* device, int textureWidth, int textureHeight)
{
	HRESULT result;

	D3D11_TEXTURE2D_DESC descDepth;
	descDepth.Width = textureWidth;
	descDepth.Height = textureHeight;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format =  DXGI_FORMAT_R32_TYPELESS;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	result = device->CreateTexture2D(&descDepth, NULL, &m_depthStencilTexture);
	if (FAILED(result))
	{
		std::cout << "!! creating depth texture failed (#1)" << std::endl;
		return false;
	}

	//create depth stencil view

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	descDSV.Format = DXGI_FORMAT_D32_FLOAT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;
	descDSV.Flags = 0;
	
	// Create the depth stencil view

	result = device->CreateDepthStencilView(m_depthStencilTexture, // Depth stencil texture
		&descDSV, // Depth stencil desc
		&m_depthStencilView);  // [out] Depth stencil view
	if (FAILED(result))
	{
		std::cout << "!! creating depth texture failed (#4)" << std::endl;
		return false;
	}

	//state

	D3D11_DEPTH_STENCIL_DESC dsDesc;

	// Depth test parameters
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

	// Stencil test parameters
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create depth stencil state
	ID3D11DepthStencilState * pDSState;
	result = device->CreateDepthStencilState(&dsDesc, &pDSState);
	if (FAILED(result))
	{
		std::cout << "!! creating depth texture failed (#2)" << std::endl;
		return false;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;

	// Setup the description of the shader resource view.
	shaderResourceViewDesc.Format = DXGI_FORMAT_R32_FLOAT;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	// Create the shader resource view.
	result = device->CreateShaderResourceView(m_depthStencilTexture, &shaderResourceViewDesc, &m_shaderResourceView);
	if (FAILED(result))
	{
		std::cout << "!! creating depth texture failed (#3)" << std::endl;
		return false;
	}

	return true;
}

void DepthTexture::SetRenderTarget(ID3D11DeviceContext * deviceContext)
{
	// Bind the render target view and depth stencil buffer to the output render pipeline.
	//deviceContext->OMSetDepthStencilState(pDSState, 1);
	return;
}

void DepthTexture::ClearRenderTarget(ID3D11DeviceContext* deviceContext, ID3D11DepthStencilView* depthStencilView, float r, float g, float b, float a)
{


}

ID3D11ShaderResourceView * DepthTexture::GetShaderResourceView()
{
	return m_shaderResourceView;
}

ID3D11DepthStencilView * DepthTexture::GetDepthStencilView()
{
	return m_depthStencilView;
}
