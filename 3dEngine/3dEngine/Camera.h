#pragma once

#include <directxmath.h>
#include <string>
#include "GUtility.h"
#include "Component.h"
#include "Input.h"

using namespace DirectX;
class Scene;
class GameObject;
class Camera : public Component
{
public:
	Camera(void);
	~Camera(void);
	
	void Initialize(GameObject* gameObject);

	string GetName();
	GameObject* GetGameObject();
	void Update(Scene * scene);
	void Update(Input* input);
	void Render(Scene * scene);
	void InitializeOthoMatrix(int screenWidth, int screenHeight, float screenNear, float screenFar);
	void InitializeProjectionMatrix(float fov, float screenAspect, float screenNear, float screenFar);
	void InitializeViewMatrix();
	void Rotate(XMFLOAT3 axis, float degrees);

	XMVECTOR GetPosition();
	XMFLOAT3 GetPositionVector();
	XMVECTOR GetRotation();
	XMFLOAT4X4 GetViewMatrix();
	XMFLOAT4X4 GetProjectionMatrix();
	XMFLOAT4X4 GetOrthoMatrix();
	XMFLOAT4 GetEyeVector();
	XMFLOAT4X4 GetViewMatrixOld();
	XMFLOAT4X4 GetProjectionMatrixOld();

	void SetPosition(float x, float y, float z);
	void SetRotation(float x, float y, float z);

private:
	XMVECTOR m_position;
	XMVECTOR m_rotation;
	XMFLOAT4X4 m_viewMatrix;
	XMFLOAT4X4 m_projectionMatrix;
	XMFLOAT4X4 m_viewMatrixOld;
	XMFLOAT4X4 m_projectionMatrixOld;
	XMFLOAT4X4 m_orthoMatrix;
	XMFLOAT3 m_up;
	XMFLOAT3 mTarget;
	XMVECTOR m_lastMousePosition;

	float rotationX;
	float rotationY;
	float smoothRotationX;
	float smoothRotationY;

	const float movementSpeed = 0.018f;
	const float movementSpeedSlow = 0.16f;
	const float mouseSpeed = 0.0018f;

public:
	XMFLOAT3 CalcUp();
};
