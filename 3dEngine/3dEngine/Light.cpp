#include "Light.h"
#include "Transform.h"
#include "GameObject.h"
#include "Scene.h"
Light::Light()
{
	m_intensity = 0;
	m_falloff = 0;
	m_color = XMFLOAT4();
}

Light::~Light()
{
}

void Light::InitializeLight(GameObject * gameObject, float intensity, float falloff, float r, float g, float b)
{
	m_intensity = intensity;
	m_falloff = falloff;
	m_color = XMFLOAT4(r, g, b, 1);

	Initialize(gameObject);
}

void Light::Initialize(GameObject * gameObject)
{
	m_gameObject = gameObject;
}

float Light::GetIntensity()
{
	return m_intensity;
}

float Light::GetFalloff()
{
	return m_falloff;
}

string Light::GetName()
{
	return Component::m_name;
}

GameObject * Light::GetGameObject()
{
	return Component::m_gameObject;
}

XMFLOAT4 Light::GetColor()
{
	return m_color;
}

XMFLOAT4 Light::GetDirection()
{
	XMFLOAT3 dir = Component::m_gameObject->GetTransform()->GetDirection();
	return XMFLOAT4(dir.x,dir.y,dir.z,1);
}

void Light::Update(Scene * scene)
{

}

void Light::Render(Scene * scene)
{
}
