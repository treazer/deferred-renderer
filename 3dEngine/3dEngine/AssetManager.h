#pragma once

#include "Shader.h"
#include "Texture.h"
#include "Mesh.h"

#include <iostream>
#include <vector>

using namespace std;
class Scene;
class AssetManager
{
public:
	~AssetManager(void);

	void LoadShader(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName, bool usesTessellation);
	void LoadStandardShader(ID3D11Device* device, HWND hwnd, WCHAR* shaderFileName, bool usesTessellation);
	void LoadTexture(ID3D11Device* device, ID3D11DeviceContext* context, WCHAR* textureFileName, bool sRGB = true);
	void LoadMesh(ID3D11Device* device, string meshFileName);
	void LoadScene(ID3D11Device* device, ID3D11DeviceContext* context, Scene * scene, string filename);
	Shader* GetShaderByName(char* shaderName);
	Texture* GetTextureByName(char* textureName);
	Mesh* GetMeshByName(char* meshName);

	static AssetManager* GetInstance();

private:
	AssetManager(void);

	vector<Texture*> m_textures;
	vector<Shader*> m_shaders;
	vector<Mesh*> m_meshes;

	static AssetManager* m_instance;
};
